import 'package:belfry_ui_generator/generated/workflow.pbgrpc.dart';
import 'package:belfry_quote/app/services.dart';
import 'package:grpc/service_api.dart';

class FlowService extends WorkflowService<WorkflowClient> with WebGrpcService {
  @override
  WorkflowClient createClient(ClientChannel channel) {
    return WorkflowClient(channel);
  }

  Future<WorkflowListResponse> listAvailableWorkflows() async {
    return await client.listAvailableWorkflows(WorkflowListRequest());
  }

  Future<WorkflowDescriptor> init(WorkflowRequest request) {
    return client.initiate(request);
  }
}
