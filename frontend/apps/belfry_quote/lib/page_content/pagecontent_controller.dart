import 'package:belfry_ui_generator/belfry_ui_generator.dart';
import 'package:belfry_quote/page_content/pagecontent_service.dart';
import 'package:get/get.dart';

enum PageCommand { wait, form, table }

class PageContentController extends GetxController {
  final service = Get.put(ContentService());
  late Stream<PageflowResponse> _pageFlowStream;
  late Stream<PageResponse> _pageStream;
  final _command = Rx(PageCommand.wait);
  late Layout formLayout;
  late Schema fieldsSchema;
  late Item fieldsItem;
  late Table table;
  String? _pageId;

  get command => _command.value;
  set command(value) => _command.value = value;

  @override
  void onInit() {
    var workflow = WorkflowDescriptor(correlationId: Get.arguments);

    _pageFlowStream = service.start(PageflowRequest(workflow: workflow));
    _pageFlowStream.listen(onPageChanged, onDone: onPageFlowFinished);

    super.onInit();
  }

  void onPageChanged(PageflowResponse event) {
    if (event.command.hasPageId()) {
      open(OpenRequest(pageId: event.command.pageId));
    }
    if (event.hasCommand()) {
      if (event.command.hasForm()) {
        // print('FORM: ${event.command.form}');

        formLayout = event.command.form.layout;
        fieldsSchema = event.command.form.schema;
        fieldsItem = event.command.form.item;
        command = PageCommand.form;
        _pageId = event.command.pageId;
      } else if (event.command.hasWait()) {
        print('WAIT');

        command = PageCommand.wait;
        3.seconds.delay().then((value) {
          service.close(
              CloseRequest(pageId: event.command.pageId, dismissed: true));
        });
      } else if (event.command.hasTable()) {
        // print('TABLE: ${event.command.table}');

        table = event.command.table.table;
        command = PageCommand.table;
        5.seconds.delay().then((value) {
          service.close(
              CloseRequest(pageId: event.command.pageId, dismissed: true));
        });
      }
    }
  }

  Future closeForm() async {
    await service.close(CloseRequest(pageId: _pageId!, submitted: true));
  }

  void onPageFlowFinished() {
    print('pageflow finished.');
  }

  Future open(OpenRequest request) async {
    _pageStream = service.open(request);
    _pageStream.listen(pageChanged);
  }

  void pageChanged(PageResponse event) {
    print(event.pageId);
  }
}
