import 'package:belfry_quote/app/app_theme.dart';
import 'package:belfry_ui_generator/belfry_ui_generator.dart' as $uigenerator;
import 'package:flutter/material.dart';

class TableContent extends StatelessWidget {
  final $uigenerator.Table table;

  const TableContent(this.table, {Key? key}) : super(key: key);

  TextStyle _getTextStyle(int n) => AppTheme.getTextStyleByNumber[n]!;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(
          top: 50,
          left: 100,
          right: 100,
          bottom: 50,
        ),
        child: Card(
          child: $uigenerator.TableGenerator(
            table,
            getTextStyle: _getTextStyle,
          ),
        ),
      ),
    );
  }
}
