﻿using Autofac;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Belfry.Context
{
    public interface IContext : IDisposable
    {
        ILifetimeScope Resolver { get; }
        ILogger Logger { get; }
        IConfiguration Configuration { get; }
    }
}
