///
//  Generated code. Do not modify.
//  source: tables.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class ColumnSize extends $pb.ProtobufEnum {
  static const ColumnSize s = ColumnSize._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 's');
  static const ColumnSize m = ColumnSize._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'm');
  static const ColumnSize l = ColumnSize._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'l');

  static const $core.List<ColumnSize> values = <ColumnSize> [
    s,
    m,
    l,
  ];

  static final $core.Map<$core.int, ColumnSize> _byValue = $pb.ProtobufEnum.initByValue(values);
  static ColumnSize? valueOf($core.int value) => _byValue[value];

  const ColumnSize._($core.int v, $core.String n) : super(v, n);
}

class TableType extends $pb.ProtobufEnum {
  static const TableType simple = TableType._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'simple');
  static const TableType simple_async = TableType._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'simple_async');
  static const TableType paginated = TableType._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'paginated');
  static const TableType paginated_async = TableType._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'paginated_async');

  static const $core.List<TableType> values = <TableType> [
    simple,
    simple_async,
    paginated,
    paginated_async,
  ];

  static final $core.Map<$core.int, TableType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static TableType? valueOf($core.int value) => _byValue[value];

  const TableType._($core.int v, $core.String n) : super(v, n);
}

