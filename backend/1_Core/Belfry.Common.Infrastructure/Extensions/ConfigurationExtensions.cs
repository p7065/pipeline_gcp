﻿namespace Microsoft.Extensions.Configuration
{
    public static class ConfigurationExtensions
    {
        public static string GetConnectionStringFromEnvironment(this IConfiguration config)
        {
            var host = config["MSSQL_HOST"];
            var port = config["MSSQL_PORT"];
            var db = config["MSSQL_DATABASE"];
            var user = config["MSSQL_USER"];
            var pass = config["MSSQL_PASS"];

            var connectionString = $"Server={host};Database={db};User ID={user};Password={pass};MultipleActiveResultSets=true;";

            return connectionString;
        }

    }
}