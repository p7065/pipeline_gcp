///
//  Generated code. Do not modify.
//  source: fields.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'fields.pbenum.dart';

export 'fields.pbenum.dart';

enum Field_Type {
  logical, 
  text, 
  number, 
  unit, 
  timestamp, 
  duration, 
  reference, 
  enumerated, 
  notSet
}

class Field extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Field_Type> _Field_TypeByTag = {
    3 : Field_Type.logical,
    4 : Field_Type.text,
    5 : Field_Type.number,
    6 : Field_Type.unit,
    7 : Field_Type.timestamp,
    8 : Field_Type.duration,
    9 : Field_Type.reference,
    10 : Field_Type.enumerated,
    0 : Field_Type.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Field', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Fields'), createEmptyInstance: create)
    ..oo(0, [3, 4, 5, 6, 7, 8, 9, 10])
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'fieldId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'schemaId')
    ..aOM<LogicalField>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'logical', subBuilder: LogicalField.create)
    ..aOM<StringField>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'text', subBuilder: StringField.create)
    ..aOM<NumberField>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'number', subBuilder: NumberField.create)
    ..aOM<UnitField>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'unit', subBuilder: UnitField.create)
    ..aOM<TimeStampField>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'timestamp', subBuilder: TimeStampField.create)
    ..aOM<DurationField>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'duration', subBuilder: DurationField.create)
    ..aOM<ReferenceField>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'reference', subBuilder: ReferenceField.create)
    ..aOM<EnumeratedField>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'enumerated', subBuilder: EnumeratedField.create)
    ..hasRequiredFields = false
  ;

  Field._() : super();
  factory Field({
    $core.String? fieldId,
    $core.String? schemaId,
    LogicalField? logical,
    StringField? text,
    NumberField? number,
    UnitField? unit,
    TimeStampField? timestamp,
    DurationField? duration,
    ReferenceField? reference,
    EnumeratedField? enumerated,
  }) {
    final _result = create();
    if (fieldId != null) {
      _result.fieldId = fieldId;
    }
    if (schemaId != null) {
      _result.schemaId = schemaId;
    }
    if (logical != null) {
      _result.logical = logical;
    }
    if (text != null) {
      _result.text = text;
    }
    if (number != null) {
      _result.number = number;
    }
    if (unit != null) {
      _result.unit = unit;
    }
    if (timestamp != null) {
      _result.timestamp = timestamp;
    }
    if (duration != null) {
      _result.duration = duration;
    }
    if (reference != null) {
      _result.reference = reference;
    }
    if (enumerated != null) {
      _result.enumerated = enumerated;
    }
    return _result;
  }
  factory Field.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Field.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Field clone() => Field()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Field copyWith(void Function(Field) updates) => super.copyWith((message) => updates(message as Field)) as Field; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Field create() => Field._();
  Field createEmptyInstance() => create();
  static $pb.PbList<Field> createRepeated() => $pb.PbList<Field>();
  @$core.pragma('dart2js:noInline')
  static Field getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Field>(create);
  static Field? _defaultInstance;

  Field_Type whichType() => _Field_TypeByTag[$_whichOneof(0)]!;
  void clearType() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.String get fieldId => $_getSZ(0);
  @$pb.TagNumber(1)
  set fieldId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFieldId() => $_has(0);
  @$pb.TagNumber(1)
  void clearFieldId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get schemaId => $_getSZ(1);
  @$pb.TagNumber(2)
  set schemaId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSchemaId() => $_has(1);
  @$pb.TagNumber(2)
  void clearSchemaId() => clearField(2);

  @$pb.TagNumber(3)
  LogicalField get logical => $_getN(2);
  @$pb.TagNumber(3)
  set logical(LogicalField v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasLogical() => $_has(2);
  @$pb.TagNumber(3)
  void clearLogical() => clearField(3);
  @$pb.TagNumber(3)
  LogicalField ensureLogical() => $_ensure(2);

  @$pb.TagNumber(4)
  StringField get text => $_getN(3);
  @$pb.TagNumber(4)
  set text(StringField v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasText() => $_has(3);
  @$pb.TagNumber(4)
  void clearText() => clearField(4);
  @$pb.TagNumber(4)
  StringField ensureText() => $_ensure(3);

  @$pb.TagNumber(5)
  NumberField get number => $_getN(4);
  @$pb.TagNumber(5)
  set number(NumberField v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasNumber() => $_has(4);
  @$pb.TagNumber(5)
  void clearNumber() => clearField(5);
  @$pb.TagNumber(5)
  NumberField ensureNumber() => $_ensure(4);

  @$pb.TagNumber(6)
  UnitField get unit => $_getN(5);
  @$pb.TagNumber(6)
  set unit(UnitField v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasUnit() => $_has(5);
  @$pb.TagNumber(6)
  void clearUnit() => clearField(6);
  @$pb.TagNumber(6)
  UnitField ensureUnit() => $_ensure(5);

  @$pb.TagNumber(7)
  TimeStampField get timestamp => $_getN(6);
  @$pb.TagNumber(7)
  set timestamp(TimeStampField v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasTimestamp() => $_has(6);
  @$pb.TagNumber(7)
  void clearTimestamp() => clearField(7);
  @$pb.TagNumber(7)
  TimeStampField ensureTimestamp() => $_ensure(6);

  @$pb.TagNumber(8)
  DurationField get duration => $_getN(7);
  @$pb.TagNumber(8)
  set duration(DurationField v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasDuration() => $_has(7);
  @$pb.TagNumber(8)
  void clearDuration() => clearField(8);
  @$pb.TagNumber(8)
  DurationField ensureDuration() => $_ensure(7);

  @$pb.TagNumber(9)
  ReferenceField get reference => $_getN(8);
  @$pb.TagNumber(9)
  set reference(ReferenceField v) { setField(9, v); }
  @$pb.TagNumber(9)
  $core.bool hasReference() => $_has(8);
  @$pb.TagNumber(9)
  void clearReference() => clearField(9);
  @$pb.TagNumber(9)
  ReferenceField ensureReference() => $_ensure(8);

  @$pb.TagNumber(10)
  EnumeratedField get enumerated => $_getN(9);
  @$pb.TagNumber(10)
  set enumerated(EnumeratedField v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasEnumerated() => $_has(9);
  @$pb.TagNumber(10)
  void clearEnumerated() => clearField(10);
  @$pb.TagNumber(10)
  EnumeratedField ensureEnumerated() => $_ensure(9);
}

class LogicalField extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LogicalField', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Fields'), createEmptyInstance: create)
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'defaultValue')
    ..hasRequiredFields = false
  ;

  LogicalField._() : super();
  factory LogicalField({
    $core.bool? defaultValue,
  }) {
    final _result = create();
    if (defaultValue != null) {
      _result.defaultValue = defaultValue;
    }
    return _result;
  }
  factory LogicalField.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LogicalField.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LogicalField clone() => LogicalField()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LogicalField copyWith(void Function(LogicalField) updates) => super.copyWith((message) => updates(message as LogicalField)) as LogicalField; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LogicalField create() => LogicalField._();
  LogicalField createEmptyInstance() => create();
  static $pb.PbList<LogicalField> createRepeated() => $pb.PbList<LogicalField>();
  @$core.pragma('dart2js:noInline')
  static LogicalField getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LogicalField>(create);
  static LogicalField? _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get defaultValue => $_getBF(0);
  @$pb.TagNumber(1)
  set defaultValue($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasDefaultValue() => $_has(0);
  @$pb.TagNumber(1)
  void clearDefaultValue() => clearField(1);
}

class StringField extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'StringField', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Fields'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  StringField._() : super();
  factory StringField() => create();
  factory StringField.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory StringField.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  StringField clone() => StringField()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  StringField copyWith(void Function(StringField) updates) => super.copyWith((message) => updates(message as StringField)) as StringField; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static StringField create() => StringField._();
  StringField createEmptyInstance() => create();
  static $pb.PbList<StringField> createRepeated() => $pb.PbList<StringField>();
  @$core.pragma('dart2js:noInline')
  static StringField getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<StringField>(create);
  static StringField? _defaultInstance;
}

class NumberField extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NumberField', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Fields'), createEmptyInstance: create)
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'floating')
    ..hasRequiredFields = false
  ;

  NumberField._() : super();
  factory NumberField({
    $core.bool? floating,
  }) {
    final _result = create();
    if (floating != null) {
      _result.floating = floating;
    }
    return _result;
  }
  factory NumberField.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NumberField.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NumberField clone() => NumberField()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NumberField copyWith(void Function(NumberField) updates) => super.copyWith((message) => updates(message as NumberField)) as NumberField; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NumberField create() => NumberField._();
  NumberField createEmptyInstance() => create();
  static $pb.PbList<NumberField> createRepeated() => $pb.PbList<NumberField>();
  @$core.pragma('dart2js:noInline')
  static NumberField getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NumberField>(create);
  static NumberField? _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get floating => $_getBF(0);
  @$pb.TagNumber(1)
  set floating($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFloating() => $_has(0);
  @$pb.TagNumber(1)
  void clearFloating() => clearField(1);
}

class UnitField extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UnitField', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Fields'), createEmptyInstance: create)
    ..e<UnitType>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'unit', $pb.PbFieldType.OE, defaultOrMaker: UnitType.nonunit, valueOf: UnitType.valueOf, enumValues: UnitType.values)
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'defaltValue', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  UnitField._() : super();
  factory UnitField({
    UnitType? unit,
    $core.double? defaltValue,
  }) {
    final _result = create();
    if (unit != null) {
      _result.unit = unit;
    }
    if (defaltValue != null) {
      _result.defaltValue = defaltValue;
    }
    return _result;
  }
  factory UnitField.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UnitField.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UnitField clone() => UnitField()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UnitField copyWith(void Function(UnitField) updates) => super.copyWith((message) => updates(message as UnitField)) as UnitField; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UnitField create() => UnitField._();
  UnitField createEmptyInstance() => create();
  static $pb.PbList<UnitField> createRepeated() => $pb.PbList<UnitField>();
  @$core.pragma('dart2js:noInline')
  static UnitField getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UnitField>(create);
  static UnitField? _defaultInstance;

  @$pb.TagNumber(1)
  UnitType get unit => $_getN(0);
  @$pb.TagNumber(1)
  set unit(UnitType v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasUnit() => $_has(0);
  @$pb.TagNumber(1)
  void clearUnit() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get defaltValue => $_getN(1);
  @$pb.TagNumber(2)
  set defaltValue($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDefaltValue() => $_has(1);
  @$pb.TagNumber(2)
  void clearDefaltValue() => clearField(2);
}

class TimeStampField extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TimeStampField', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Fields'), createEmptyInstance: create)
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'epochSeconds')
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'defaultValue')
    ..hasRequiredFields = false
  ;

  TimeStampField._() : super();
  factory TimeStampField({
    $core.bool? epochSeconds,
    $fixnum.Int64? defaultValue,
  }) {
    final _result = create();
    if (epochSeconds != null) {
      _result.epochSeconds = epochSeconds;
    }
    if (defaultValue != null) {
      _result.defaultValue = defaultValue;
    }
    return _result;
  }
  factory TimeStampField.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TimeStampField.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TimeStampField clone() => TimeStampField()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TimeStampField copyWith(void Function(TimeStampField) updates) => super.copyWith((message) => updates(message as TimeStampField)) as TimeStampField; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TimeStampField create() => TimeStampField._();
  TimeStampField createEmptyInstance() => create();
  static $pb.PbList<TimeStampField> createRepeated() => $pb.PbList<TimeStampField>();
  @$core.pragma('dart2js:noInline')
  static TimeStampField getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TimeStampField>(create);
  static TimeStampField? _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get epochSeconds => $_getBF(0);
  @$pb.TagNumber(1)
  set epochSeconds($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasEpochSeconds() => $_has(0);
  @$pb.TagNumber(1)
  void clearEpochSeconds() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get defaultValue => $_getI64(1);
  @$pb.TagNumber(2)
  set defaultValue($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDefaultValue() => $_has(1);
  @$pb.TagNumber(2)
  void clearDefaultValue() => clearField(2);
}

class DurationField extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DurationField', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Fields'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'defaultSeconds', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  DurationField._() : super();
  factory DurationField({
    $core.int? defaultSeconds,
  }) {
    final _result = create();
    if (defaultSeconds != null) {
      _result.defaultSeconds = defaultSeconds;
    }
    return _result;
  }
  factory DurationField.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DurationField.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DurationField clone() => DurationField()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DurationField copyWith(void Function(DurationField) updates) => super.copyWith((message) => updates(message as DurationField)) as DurationField; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DurationField create() => DurationField._();
  DurationField createEmptyInstance() => create();
  static $pb.PbList<DurationField> createRepeated() => $pb.PbList<DurationField>();
  @$core.pragma('dart2js:noInline')
  static DurationField getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DurationField>(create);
  static DurationField? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get defaultSeconds => $_getIZ(0);
  @$pb.TagNumber(1)
  set defaultSeconds($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasDefaultSeconds() => $_has(0);
  @$pb.TagNumber(1)
  void clearDefaultSeconds() => clearField(1);
}

class ReferenceField extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ReferenceField', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Fields'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'schemaId')
    ..pc<Item>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'options', $pb.PbFieldType.PM, subBuilder: Item.create)
    ..hasRequiredFields = false
  ;

  ReferenceField._() : super();
  factory ReferenceField({
    $core.String? schemaId,
    $core.Iterable<Item>? options,
  }) {
    final _result = create();
    if (schemaId != null) {
      _result.schemaId = schemaId;
    }
    if (options != null) {
      _result.options.addAll(options);
    }
    return _result;
  }
  factory ReferenceField.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ReferenceField.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ReferenceField clone() => ReferenceField()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ReferenceField copyWith(void Function(ReferenceField) updates) => super.copyWith((message) => updates(message as ReferenceField)) as ReferenceField; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ReferenceField create() => ReferenceField._();
  ReferenceField createEmptyInstance() => create();
  static $pb.PbList<ReferenceField> createRepeated() => $pb.PbList<ReferenceField>();
  @$core.pragma('dart2js:noInline')
  static ReferenceField getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ReferenceField>(create);
  static ReferenceField? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get schemaId => $_getSZ(0);
  @$pb.TagNumber(1)
  set schemaId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSchemaId() => $_has(0);
  @$pb.TagNumber(1)
  void clearSchemaId() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<Item> get options => $_getList(1);
}

class EnumeratedField extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'EnumeratedField', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Fields'), createEmptyInstance: create)
    ..pc<KeyValue>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'values', $pb.PbFieldType.PM, subBuilder: KeyValue.create)
    ..hasRequiredFields = false
  ;

  EnumeratedField._() : super();
  factory EnumeratedField({
    $core.Iterable<KeyValue>? values,
  }) {
    final _result = create();
    if (values != null) {
      _result.values.addAll(values);
    }
    return _result;
  }
  factory EnumeratedField.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory EnumeratedField.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  EnumeratedField clone() => EnumeratedField()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  EnumeratedField copyWith(void Function(EnumeratedField) updates) => super.copyWith((message) => updates(message as EnumeratedField)) as EnumeratedField; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static EnumeratedField create() => EnumeratedField._();
  EnumeratedField createEmptyInstance() => create();
  static $pb.PbList<EnumeratedField> createRepeated() => $pb.PbList<EnumeratedField>();
  @$core.pragma('dart2js:noInline')
  static EnumeratedField getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<EnumeratedField>(create);
  static EnumeratedField? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<KeyValue> get values => $_getList(0);
}

enum KeyValue_Value {
  text, 
  number, 
  notSet
}

class KeyValue extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, KeyValue_Value> _KeyValue_ValueByTag = {
    2 : KeyValue_Value.text,
    3 : KeyValue_Value.number,
    0 : KeyValue_Value.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'KeyValue', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Fields'), createEmptyInstance: create)
    ..oo(0, [2, 3])
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'key')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'text')
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'number', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  KeyValue._() : super();
  factory KeyValue({
    $core.String? key,
    $core.String? text,
    $core.int? number,
  }) {
    final _result = create();
    if (key != null) {
      _result.key = key;
    }
    if (text != null) {
      _result.text = text;
    }
    if (number != null) {
      _result.number = number;
    }
    return _result;
  }
  factory KeyValue.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory KeyValue.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  KeyValue clone() => KeyValue()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  KeyValue copyWith(void Function(KeyValue) updates) => super.copyWith((message) => updates(message as KeyValue)) as KeyValue; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static KeyValue create() => KeyValue._();
  KeyValue createEmptyInstance() => create();
  static $pb.PbList<KeyValue> createRepeated() => $pb.PbList<KeyValue>();
  @$core.pragma('dart2js:noInline')
  static KeyValue getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<KeyValue>(create);
  static KeyValue? _defaultInstance;

  KeyValue_Value whichValue() => _KeyValue_ValueByTag[$_whichOneof(0)]!;
  void clearValue() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.String get key => $_getSZ(0);
  @$pb.TagNumber(1)
  set key($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasKey() => $_has(0);
  @$pb.TagNumber(1)
  void clearKey() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get text => $_getSZ(1);
  @$pb.TagNumber(2)
  set text($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasText() => $_has(1);
  @$pb.TagNumber(2)
  void clearText() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get number => $_getIZ(2);
  @$pb.TagNumber(3)
  set number($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasNumber() => $_has(2);
  @$pb.TagNumber(3)
  void clearNumber() => clearField(3);
}

class SchemaVersion extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SchemaVersion', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Fields'), createEmptyInstance: create)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'number', $pb.PbFieldType.O3)
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'epoch')
    ..hasRequiredFields = false
  ;

  SchemaVersion._() : super();
  factory SchemaVersion({
    $core.int? number,
    $fixnum.Int64? epoch,
  }) {
    final _result = create();
    if (number != null) {
      _result.number = number;
    }
    if (epoch != null) {
      _result.epoch = epoch;
    }
    return _result;
  }
  factory SchemaVersion.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SchemaVersion.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SchemaVersion clone() => SchemaVersion()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SchemaVersion copyWith(void Function(SchemaVersion) updates) => super.copyWith((message) => updates(message as SchemaVersion)) as SchemaVersion; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SchemaVersion create() => SchemaVersion._();
  SchemaVersion createEmptyInstance() => create();
  static $pb.PbList<SchemaVersion> createRepeated() => $pb.PbList<SchemaVersion>();
  @$core.pragma('dart2js:noInline')
  static SchemaVersion getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SchemaVersion>(create);
  static SchemaVersion? _defaultInstance;

  @$pb.TagNumber(2)
  $core.int get number => $_getIZ(0);
  @$pb.TagNumber(2)
  set number($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(2)
  $core.bool hasNumber() => $_has(0);
  @$pb.TagNumber(2)
  void clearNumber() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get epoch => $_getI64(1);
  @$pb.TagNumber(3)
  set epoch($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(3)
  $core.bool hasEpoch() => $_has(1);
  @$pb.TagNumber(3)
  void clearEpoch() => clearField(3);
}

class Schema extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Schema', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Fields'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'schemaId')
    ..e<EntityType>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type', $pb.PbFieldType.OE, defaultOrMaker: EntityType.poco, valueOf: EntityType.valueOf, enumValues: EntityType.values)
    ..aOM<SchemaVersion>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'version', subBuilder: SchemaVersion.create)
    ..pc<Field>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'fields', $pb.PbFieldType.PM, subBuilder: Field.create)
    ..hasRequiredFields = false
  ;

  Schema._() : super();
  factory Schema({
    $core.String? schemaId,
    EntityType? type,
    SchemaVersion? version,
    $core.Iterable<Field>? fields,
  }) {
    final _result = create();
    if (schemaId != null) {
      _result.schemaId = schemaId;
    }
    if (type != null) {
      _result.type = type;
    }
    if (version != null) {
      _result.version = version;
    }
    if (fields != null) {
      _result.fields.addAll(fields);
    }
    return _result;
  }
  factory Schema.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Schema.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Schema clone() => Schema()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Schema copyWith(void Function(Schema) updates) => super.copyWith((message) => updates(message as Schema)) as Schema; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Schema create() => Schema._();
  Schema createEmptyInstance() => create();
  static $pb.PbList<Schema> createRepeated() => $pb.PbList<Schema>();
  @$core.pragma('dart2js:noInline')
  static Schema getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Schema>(create);
  static Schema? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get schemaId => $_getSZ(0);
  @$pb.TagNumber(1)
  set schemaId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSchemaId() => $_has(0);
  @$pb.TagNumber(1)
  void clearSchemaId() => clearField(1);

  @$pb.TagNumber(2)
  EntityType get type => $_getN(1);
  @$pb.TagNumber(2)
  set type(EntityType v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasType() => $_has(1);
  @$pb.TagNumber(2)
  void clearType() => clearField(2);

  @$pb.TagNumber(3)
  SchemaVersion get version => $_getN(2);
  @$pb.TagNumber(3)
  set version(SchemaVersion v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasVersion() => $_has(2);
  @$pb.TagNumber(3)
  void clearVersion() => clearField(3);
  @$pb.TagNumber(3)
  SchemaVersion ensureVersion() => $_ensure(2);

  @$pb.TagNumber(4)
  $core.List<Field> get fields => $_getList(3);
}

class EnumeratedValue extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'EnumeratedValue', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Fields'), createEmptyInstance: create)
    ..pPS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'keys')
    ..hasRequiredFields = false
  ;

  EnumeratedValue._() : super();
  factory EnumeratedValue({
    $core.Iterable<$core.String>? keys,
  }) {
    final _result = create();
    if (keys != null) {
      _result.keys.addAll(keys);
    }
    return _result;
  }
  factory EnumeratedValue.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory EnumeratedValue.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  EnumeratedValue clone() => EnumeratedValue()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  EnumeratedValue copyWith(void Function(EnumeratedValue) updates) => super.copyWith((message) => updates(message as EnumeratedValue)) as EnumeratedValue; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static EnumeratedValue create() => EnumeratedValue._();
  EnumeratedValue createEmptyInstance() => create();
  static $pb.PbList<EnumeratedValue> createRepeated() => $pb.PbList<EnumeratedValue>();
  @$core.pragma('dart2js:noInline')
  static EnumeratedValue getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<EnumeratedValue>(create);
  static EnumeratedValue? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.String> get keys => $_getList(0);
}

enum FieldValue_Value {
  logical, 
  text, 
  integer, 
  long, 
  double_6, 
  list, 
  itemId, 
  notSet
}

class FieldValue extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, FieldValue_Value> _FieldValue_ValueByTag = {
    2 : FieldValue_Value.logical,
    3 : FieldValue_Value.text,
    4 : FieldValue_Value.integer,
    5 : FieldValue_Value.long,
    6 : FieldValue_Value.double_6,
    8 : FieldValue_Value.list,
    9 : FieldValue_Value.itemId,
    0 : FieldValue_Value.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'FieldValue', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Fields'), createEmptyInstance: create)
    ..oo(0, [2, 3, 4, 5, 6, 8, 9])
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'fieldId')
    ..aOB(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'logical')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'text')
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'integer', $pb.PbFieldType.O3)
    ..aInt64(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'long')
    ..a<$core.double>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'double', $pb.PbFieldType.OD)
    ..aOM<EnumeratedValue>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'list', subBuilder: EnumeratedValue.create)
    ..aOS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemId')
    ..hasRequiredFields = false
  ;

  FieldValue._() : super();
  factory FieldValue({
    $core.String? fieldId,
    $core.bool? logical,
    $core.String? text,
    $core.int? integer,
    $fixnum.Int64? long,
    $core.double? double_6,
    EnumeratedValue? list,
    $core.String? itemId,
  }) {
    final _result = create();
    if (fieldId != null) {
      _result.fieldId = fieldId;
    }
    if (logical != null) {
      _result.logical = logical;
    }
    if (text != null) {
      _result.text = text;
    }
    if (integer != null) {
      _result.integer = integer;
    }
    if (long != null) {
      _result.long = long;
    }
    if (double_6 != null) {
      _result.double_6 = double_6;
    }
    if (list != null) {
      _result.list = list;
    }
    if (itemId != null) {
      _result.itemId = itemId;
    }
    return _result;
  }
  factory FieldValue.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory FieldValue.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  FieldValue clone() => FieldValue()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  FieldValue copyWith(void Function(FieldValue) updates) => super.copyWith((message) => updates(message as FieldValue)) as FieldValue; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static FieldValue create() => FieldValue._();
  FieldValue createEmptyInstance() => create();
  static $pb.PbList<FieldValue> createRepeated() => $pb.PbList<FieldValue>();
  @$core.pragma('dart2js:noInline')
  static FieldValue getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<FieldValue>(create);
  static FieldValue? _defaultInstance;

  FieldValue_Value whichValue() => _FieldValue_ValueByTag[$_whichOneof(0)]!;
  void clearValue() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.String get fieldId => $_getSZ(0);
  @$pb.TagNumber(1)
  set fieldId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFieldId() => $_has(0);
  @$pb.TagNumber(1)
  void clearFieldId() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get logical => $_getBF(1);
  @$pb.TagNumber(2)
  set logical($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLogical() => $_has(1);
  @$pb.TagNumber(2)
  void clearLogical() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get text => $_getSZ(2);
  @$pb.TagNumber(3)
  set text($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasText() => $_has(2);
  @$pb.TagNumber(3)
  void clearText() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get integer => $_getIZ(3);
  @$pb.TagNumber(4)
  set integer($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasInteger() => $_has(3);
  @$pb.TagNumber(4)
  void clearInteger() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get long => $_getI64(4);
  @$pb.TagNumber(5)
  set long($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasLong() => $_has(4);
  @$pb.TagNumber(5)
  void clearLong() => clearField(5);

  @$pb.TagNumber(6)
  $core.double get double_6 => $_getN(5);
  @$pb.TagNumber(6)
  set double_6($core.double v) { $_setDouble(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasDouble_6() => $_has(5);
  @$pb.TagNumber(6)
  void clearDouble_6() => clearField(6);

  @$pb.TagNumber(8)
  EnumeratedValue get list => $_getN(6);
  @$pb.TagNumber(8)
  set list(EnumeratedValue v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasList() => $_has(6);
  @$pb.TagNumber(8)
  void clearList() => clearField(8);
  @$pb.TagNumber(8)
  EnumeratedValue ensureList() => $_ensure(6);

  @$pb.TagNumber(9)
  $core.String get itemId => $_getSZ(7);
  @$pb.TagNumber(9)
  set itemId($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(9)
  $core.bool hasItemId() => $_has(7);
  @$pb.TagNumber(9)
  void clearItemId() => clearField(9);
}

class Item extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Item', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Fields'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'schemaId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemId')
    ..pc<FieldValue>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'values', $pb.PbFieldType.PM, subBuilder: FieldValue.create)
    ..hasRequiredFields = false
  ;

  Item._() : super();
  factory Item({
    $core.String? schemaId,
    $core.String? itemId,
    $core.Iterable<FieldValue>? values,
  }) {
    final _result = create();
    if (schemaId != null) {
      _result.schemaId = schemaId;
    }
    if (itemId != null) {
      _result.itemId = itemId;
    }
    if (values != null) {
      _result.values.addAll(values);
    }
    return _result;
  }
  factory Item.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Item.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Item clone() => Item()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Item copyWith(void Function(Item) updates) => super.copyWith((message) => updates(message as Item)) as Item; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Item create() => Item._();
  Item createEmptyInstance() => create();
  static $pb.PbList<Item> createRepeated() => $pb.PbList<Item>();
  @$core.pragma('dart2js:noInline')
  static Item getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Item>(create);
  static Item? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get schemaId => $_getSZ(0);
  @$pb.TagNumber(1)
  set schemaId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSchemaId() => $_has(0);
  @$pb.TagNumber(1)
  void clearSchemaId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get itemId => $_getSZ(1);
  @$pb.TagNumber(2)
  set itemId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasItemId() => $_has(1);
  @$pb.TagNumber(2)
  void clearItemId() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<FieldValue> get values => $_getList(2);
}

