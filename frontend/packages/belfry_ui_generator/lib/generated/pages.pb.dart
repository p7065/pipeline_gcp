///
//  Generated code. Do not modify.
//  source: pages.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'workflow.pb.dart' as $0;
import 'fields.pb.dart' as $2;
import 'forms.pb.dart' as $3;
import 'tables.pb.dart' as $4;

import 'pages.pbenum.dart';

export 'pages.pbenum.dart';

class PageflowConfiguration extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PageflowConfiguration', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  PageflowConfiguration._() : super();
  factory PageflowConfiguration() => create();
  factory PageflowConfiguration.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PageflowConfiguration.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PageflowConfiguration clone() => PageflowConfiguration()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PageflowConfiguration copyWith(void Function(PageflowConfiguration) updates) => super.copyWith((message) => updates(message as PageflowConfiguration)) as PageflowConfiguration; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PageflowConfiguration create() => PageflowConfiguration._();
  PageflowConfiguration createEmptyInstance() => create();
  static $pb.PbList<PageflowConfiguration> createRepeated() => $pb.PbList<PageflowConfiguration>();
  @$core.pragma('dart2js:noInline')
  static PageflowConfiguration getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PageflowConfiguration>(create);
  static PageflowConfiguration? _defaultInstance;
}

class PageflowRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PageflowRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..aOM<$0.WorkflowDescriptor>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'workflow', subBuilder: $0.WorkflowDescriptor.create)
    ..aOM<PageflowConfiguration>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'configuration', subBuilder: PageflowConfiguration.create)
    ..hasRequiredFields = false
  ;

  PageflowRequest._() : super();
  factory PageflowRequest({
    $0.WorkflowDescriptor? workflow,
    PageflowConfiguration? configuration,
  }) {
    final _result = create();
    if (workflow != null) {
      _result.workflow = workflow;
    }
    if (configuration != null) {
      _result.configuration = configuration;
    }
    return _result;
  }
  factory PageflowRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PageflowRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PageflowRequest clone() => PageflowRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PageflowRequest copyWith(void Function(PageflowRequest) updates) => super.copyWith((message) => updates(message as PageflowRequest)) as PageflowRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PageflowRequest create() => PageflowRequest._();
  PageflowRequest createEmptyInstance() => create();
  static $pb.PbList<PageflowRequest> createRepeated() => $pb.PbList<PageflowRequest>();
  @$core.pragma('dart2js:noInline')
  static PageflowRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PageflowRequest>(create);
  static PageflowRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $0.WorkflowDescriptor get workflow => $_getN(0);
  @$pb.TagNumber(1)
  set workflow($0.WorkflowDescriptor v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasWorkflow() => $_has(0);
  @$pb.TagNumber(1)
  void clearWorkflow() => clearField(1);
  @$pb.TagNumber(1)
  $0.WorkflowDescriptor ensureWorkflow() => $_ensure(0);

  @$pb.TagNumber(2)
  PageflowConfiguration get configuration => $_getN(1);
  @$pb.TagNumber(2)
  set configuration(PageflowConfiguration v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasConfiguration() => $_has(1);
  @$pb.TagNumber(2)
  void clearConfiguration() => clearField(2);
  @$pb.TagNumber(2)
  PageflowConfiguration ensureConfiguration() => $_ensure(1);
}

class PageflowResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PageflowResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'flowId')
    ..aOM<PageflowCommand>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'command', subBuilder: PageflowCommand.create)
    ..hasRequiredFields = false
  ;

  PageflowResponse._() : super();
  factory PageflowResponse({
    $core.String? flowId,
    PageflowCommand? command,
  }) {
    final _result = create();
    if (flowId != null) {
      _result.flowId = flowId;
    }
    if (command != null) {
      _result.command = command;
    }
    return _result;
  }
  factory PageflowResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PageflowResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PageflowResponse clone() => PageflowResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PageflowResponse copyWith(void Function(PageflowResponse) updates) => super.copyWith((message) => updates(message as PageflowResponse)) as PageflowResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PageflowResponse create() => PageflowResponse._();
  PageflowResponse createEmptyInstance() => create();
  static $pb.PbList<PageflowResponse> createRepeated() => $pb.PbList<PageflowResponse>();
  @$core.pragma('dart2js:noInline')
  static PageflowResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PageflowResponse>(create);
  static PageflowResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get flowId => $_getSZ(0);
  @$pb.TagNumber(1)
  set flowId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFlowId() => $_has(0);
  @$pb.TagNumber(1)
  void clearFlowId() => clearField(1);

  @$pb.TagNumber(2)
  PageflowCommand get command => $_getN(1);
  @$pb.TagNumber(2)
  set command(PageflowCommand v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasCommand() => $_has(1);
  @$pb.TagNumber(2)
  void clearCommand() => clearField(2);
  @$pb.TagNumber(2)
  PageflowCommand ensureCommand() => $_ensure(1);
}

enum PageflowCommand_Command {
  wait, 
  form, 
  table, 
  notSet
}

class PageflowCommand extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, PageflowCommand_Command> _PageflowCommand_CommandByTag = {
    2 : PageflowCommand_Command.wait,
    3 : PageflowCommand_Command.form,
    4 : PageflowCommand_Command.table,
    0 : PageflowCommand_Command.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PageflowCommand', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..oo(0, [2, 3, 4])
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pageId')
    ..aOM<WaitCommand>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'wait', subBuilder: WaitCommand.create)
    ..aOM<FormCommand>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'form', subBuilder: FormCommand.create)
    ..aOM<TableCommand>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'table', subBuilder: TableCommand.create)
    ..hasRequiredFields = false
  ;

  PageflowCommand._() : super();
  factory PageflowCommand({
    $core.String? pageId,
    WaitCommand? wait,
    FormCommand? form,
    TableCommand? table,
  }) {
    final _result = create();
    if (pageId != null) {
      _result.pageId = pageId;
    }
    if (wait != null) {
      _result.wait = wait;
    }
    if (form != null) {
      _result.form = form;
    }
    if (table != null) {
      _result.table = table;
    }
    return _result;
  }
  factory PageflowCommand.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PageflowCommand.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PageflowCommand clone() => PageflowCommand()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PageflowCommand copyWith(void Function(PageflowCommand) updates) => super.copyWith((message) => updates(message as PageflowCommand)) as PageflowCommand; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PageflowCommand create() => PageflowCommand._();
  PageflowCommand createEmptyInstance() => create();
  static $pb.PbList<PageflowCommand> createRepeated() => $pb.PbList<PageflowCommand>();
  @$core.pragma('dart2js:noInline')
  static PageflowCommand getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PageflowCommand>(create);
  static PageflowCommand? _defaultInstance;

  PageflowCommand_Command whichCommand() => _PageflowCommand_CommandByTag[$_whichOneof(0)]!;
  void clearCommand() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.String get pageId => $_getSZ(0);
  @$pb.TagNumber(1)
  set pageId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPageId() => $_has(0);
  @$pb.TagNumber(1)
  void clearPageId() => clearField(1);

  @$pb.TagNumber(2)
  WaitCommand get wait => $_getN(1);
  @$pb.TagNumber(2)
  set wait(WaitCommand v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasWait() => $_has(1);
  @$pb.TagNumber(2)
  void clearWait() => clearField(2);
  @$pb.TagNumber(2)
  WaitCommand ensureWait() => $_ensure(1);

  @$pb.TagNumber(3)
  FormCommand get form => $_getN(2);
  @$pb.TagNumber(3)
  set form(FormCommand v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasForm() => $_has(2);
  @$pb.TagNumber(3)
  void clearForm() => clearField(3);
  @$pb.TagNumber(3)
  FormCommand ensureForm() => $_ensure(2);

  @$pb.TagNumber(4)
  TableCommand get table => $_getN(3);
  @$pb.TagNumber(4)
  set table(TableCommand v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasTable() => $_has(3);
  @$pb.TagNumber(4)
  void clearTable() => clearField(4);
  @$pb.TagNumber(4)
  TableCommand ensureTable() => $_ensure(3);
}

class WaitCommand extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'WaitCommand', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  WaitCommand._() : super();
  factory WaitCommand() => create();
  factory WaitCommand.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory WaitCommand.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  WaitCommand clone() => WaitCommand()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  WaitCommand copyWith(void Function(WaitCommand) updates) => super.copyWith((message) => updates(message as WaitCommand)) as WaitCommand; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static WaitCommand create() => WaitCommand._();
  WaitCommand createEmptyInstance() => create();
  static $pb.PbList<WaitCommand> createRepeated() => $pb.PbList<WaitCommand>();
  @$core.pragma('dart2js:noInline')
  static WaitCommand getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<WaitCommand>(create);
  static WaitCommand? _defaultInstance;
}

class FormCommand extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'FormCommand', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..aOM<$2.Schema>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'schema', subBuilder: $2.Schema.create)
    ..aOM<$2.Item>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'item', subBuilder: $2.Item.create)
    ..aOM<$3.Layout>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'layout', subBuilder: $3.Layout.create)
    ..hasRequiredFields = false
  ;

  FormCommand._() : super();
  factory FormCommand({
    $2.Schema? schema,
    $2.Item? item,
    $3.Layout? layout,
  }) {
    final _result = create();
    if (schema != null) {
      _result.schema = schema;
    }
    if (item != null) {
      _result.item = item;
    }
    if (layout != null) {
      _result.layout = layout;
    }
    return _result;
  }
  factory FormCommand.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory FormCommand.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  FormCommand clone() => FormCommand()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  FormCommand copyWith(void Function(FormCommand) updates) => super.copyWith((message) => updates(message as FormCommand)) as FormCommand; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static FormCommand create() => FormCommand._();
  FormCommand createEmptyInstance() => create();
  static $pb.PbList<FormCommand> createRepeated() => $pb.PbList<FormCommand>();
  @$core.pragma('dart2js:noInline')
  static FormCommand getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<FormCommand>(create);
  static FormCommand? _defaultInstance;

  @$pb.TagNumber(1)
  $2.Schema get schema => $_getN(0);
  @$pb.TagNumber(1)
  set schema($2.Schema v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasSchema() => $_has(0);
  @$pb.TagNumber(1)
  void clearSchema() => clearField(1);
  @$pb.TagNumber(1)
  $2.Schema ensureSchema() => $_ensure(0);

  @$pb.TagNumber(2)
  $2.Item get item => $_getN(1);
  @$pb.TagNumber(2)
  set item($2.Item v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasItem() => $_has(1);
  @$pb.TagNumber(2)
  void clearItem() => clearField(2);
  @$pb.TagNumber(2)
  $2.Item ensureItem() => $_ensure(1);

  @$pb.TagNumber(3)
  $3.Layout get layout => $_getN(2);
  @$pb.TagNumber(3)
  set layout($3.Layout v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasLayout() => $_has(2);
  @$pb.TagNumber(3)
  void clearLayout() => clearField(3);
  @$pb.TagNumber(3)
  $3.Layout ensureLayout() => $_ensure(2);
}

class TableCommand extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TableCommand', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..aOM<$2.Schema>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'schema', subBuilder: $2.Schema.create)
    ..aOM<$4.Table>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'table', subBuilder: $4.Table.create)
    ..hasRequiredFields = false
  ;

  TableCommand._() : super();
  factory TableCommand({
    $2.Schema? schema,
    $4.Table? table,
  }) {
    final _result = create();
    if (schema != null) {
      _result.schema = schema;
    }
    if (table != null) {
      _result.table = table;
    }
    return _result;
  }
  factory TableCommand.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TableCommand.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TableCommand clone() => TableCommand()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TableCommand copyWith(void Function(TableCommand) updates) => super.copyWith((message) => updates(message as TableCommand)) as TableCommand; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TableCommand create() => TableCommand._();
  TableCommand createEmptyInstance() => create();
  static $pb.PbList<TableCommand> createRepeated() => $pb.PbList<TableCommand>();
  @$core.pragma('dart2js:noInline')
  static TableCommand getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TableCommand>(create);
  static TableCommand? _defaultInstance;

  @$pb.TagNumber(1)
  $2.Schema get schema => $_getN(0);
  @$pb.TagNumber(1)
  set schema($2.Schema v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasSchema() => $_has(0);
  @$pb.TagNumber(1)
  void clearSchema() => clearField(1);
  @$pb.TagNumber(1)
  $2.Schema ensureSchema() => $_ensure(0);

  @$pb.TagNumber(2)
  $4.Table get table => $_getN(1);
  @$pb.TagNumber(2)
  set table($4.Table v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasTable() => $_has(1);
  @$pb.TagNumber(2)
  void clearTable() => clearField(2);
  @$pb.TagNumber(2)
  $4.Table ensureTable() => $_ensure(1);
}

class AsyncTaskDescription extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AsyncTaskDescription', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..hasRequiredFields = false
  ;

  AsyncTaskDescription._() : super();
  factory AsyncTaskDescription({
    $core.String? name,
  }) {
    final _result = create();
    if (name != null) {
      _result.name = name;
    }
    return _result;
  }
  factory AsyncTaskDescription.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AsyncTaskDescription.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AsyncTaskDescription clone() => AsyncTaskDescription()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AsyncTaskDescription copyWith(void Function(AsyncTaskDescription) updates) => super.copyWith((message) => updates(message as AsyncTaskDescription)) as AsyncTaskDescription; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AsyncTaskDescription create() => AsyncTaskDescription._();
  AsyncTaskDescription createEmptyInstance() => create();
  static $pb.PbList<AsyncTaskDescription> createRepeated() => $pb.PbList<AsyncTaskDescription>();
  @$core.pragma('dart2js:noInline')
  static AsyncTaskDescription getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AsyncTaskDescription>(create);
  static AsyncTaskDescription? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);
}

class AsyncTaskStatus extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AsyncTaskStatus', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'percent', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  AsyncTaskStatus._() : super();
  factory AsyncTaskStatus({
    $core.double? percent,
  }) {
    final _result = create();
    if (percent != null) {
      _result.percent = percent;
    }
    return _result;
  }
  factory AsyncTaskStatus.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AsyncTaskStatus.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AsyncTaskStatus clone() => AsyncTaskStatus()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AsyncTaskStatus copyWith(void Function(AsyncTaskStatus) updates) => super.copyWith((message) => updates(message as AsyncTaskStatus)) as AsyncTaskStatus; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AsyncTaskStatus create() => AsyncTaskStatus._();
  AsyncTaskStatus createEmptyInstance() => create();
  static $pb.PbList<AsyncTaskStatus> createRepeated() => $pb.PbList<AsyncTaskStatus>();
  @$core.pragma('dart2js:noInline')
  static AsyncTaskStatus getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AsyncTaskStatus>(create);
  static AsyncTaskStatus? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get percent => $_getN(0);
  @$pb.TagNumber(1)
  set percent($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPercent() => $_has(0);
  @$pb.TagNumber(1)
  void clearPercent() => clearField(1);
}

class AsyncTaskResult extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AsyncTaskResult', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'success')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'error')
    ..hasRequiredFields = false
  ;

  AsyncTaskResult._() : super();
  factory AsyncTaskResult({
    $core.bool? success,
    $core.String? error,
  }) {
    final _result = create();
    if (success != null) {
      _result.success = success;
    }
    if (error != null) {
      _result.error = error;
    }
    return _result;
  }
  factory AsyncTaskResult.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AsyncTaskResult.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AsyncTaskResult clone() => AsyncTaskResult()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AsyncTaskResult copyWith(void Function(AsyncTaskResult) updates) => super.copyWith((message) => updates(message as AsyncTaskResult)) as AsyncTaskResult; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AsyncTaskResult create() => AsyncTaskResult._();
  AsyncTaskResult createEmptyInstance() => create();
  static $pb.PbList<AsyncTaskResult> createRepeated() => $pb.PbList<AsyncTaskResult>();
  @$core.pragma('dart2js:noInline')
  static AsyncTaskResult getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AsyncTaskResult>(create);
  static AsyncTaskResult? _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get success => $_getBF(0);
  @$pb.TagNumber(1)
  set success($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSuccess() => $_has(0);
  @$pb.TagNumber(1)
  void clearSuccess() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get error => $_getSZ(1);
  @$pb.TagNumber(2)
  set error($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasError() => $_has(1);
  @$pb.TagNumber(2)
  void clearError() => clearField(2);
}

enum AsyncProgress_Phase {
  description, 
  status, 
  result, 
  notSet
}

class AsyncProgress extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, AsyncProgress_Phase> _AsyncProgress_PhaseByTag = {
    2 : AsyncProgress_Phase.description,
    3 : AsyncProgress_Phase.status,
    4 : AsyncProgress_Phase.result,
    0 : AsyncProgress_Phase.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AsyncProgress', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..oo(0, [2, 3, 4])
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'taskId')
    ..aOM<AsyncTaskDescription>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description', subBuilder: AsyncTaskDescription.create)
    ..aOM<AsyncTaskStatus>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', subBuilder: AsyncTaskStatus.create)
    ..aOM<AsyncTaskResult>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'result', subBuilder: AsyncTaskResult.create)
    ..hasRequiredFields = false
  ;

  AsyncProgress._() : super();
  factory AsyncProgress({
    $core.String? taskId,
    AsyncTaskDescription? description,
    AsyncTaskStatus? status,
    AsyncTaskResult? result,
  }) {
    final _result = create();
    if (taskId != null) {
      _result.taskId = taskId;
    }
    if (description != null) {
      _result.description = description;
    }
    if (status != null) {
      _result.status = status;
    }
    if (result != null) {
      _result.result = result;
    }
    return _result;
  }
  factory AsyncProgress.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AsyncProgress.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AsyncProgress clone() => AsyncProgress()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AsyncProgress copyWith(void Function(AsyncProgress) updates) => super.copyWith((message) => updates(message as AsyncProgress)) as AsyncProgress; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AsyncProgress create() => AsyncProgress._();
  AsyncProgress createEmptyInstance() => create();
  static $pb.PbList<AsyncProgress> createRepeated() => $pb.PbList<AsyncProgress>();
  @$core.pragma('dart2js:noInline')
  static AsyncProgress getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AsyncProgress>(create);
  static AsyncProgress? _defaultInstance;

  AsyncProgress_Phase whichPhase() => _AsyncProgress_PhaseByTag[$_whichOneof(0)]!;
  void clearPhase() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.String get taskId => $_getSZ(0);
  @$pb.TagNumber(1)
  set taskId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTaskId() => $_has(0);
  @$pb.TagNumber(1)
  void clearTaskId() => clearField(1);

  @$pb.TagNumber(2)
  AsyncTaskDescription get description => $_getN(1);
  @$pb.TagNumber(2)
  set description(AsyncTaskDescription v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasDescription() => $_has(1);
  @$pb.TagNumber(2)
  void clearDescription() => clearField(2);
  @$pb.TagNumber(2)
  AsyncTaskDescription ensureDescription() => $_ensure(1);

  @$pb.TagNumber(3)
  AsyncTaskStatus get status => $_getN(2);
  @$pb.TagNumber(3)
  set status(AsyncTaskStatus v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(2);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
  @$pb.TagNumber(3)
  AsyncTaskStatus ensureStatus() => $_ensure(2);

  @$pb.TagNumber(4)
  AsyncTaskResult get result => $_getN(3);
  @$pb.TagNumber(4)
  set result(AsyncTaskResult v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasResult() => $_has(3);
  @$pb.TagNumber(4)
  void clearResult() => clearField(4);
  @$pb.TagNumber(4)
  AsyncTaskResult ensureResult() => $_ensure(3);
}

class SessionKeepAlive extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SessionKeepAlive', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'uptimeSeconds', $pb.PbFieldType.O3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'idleSeconds', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  SessionKeepAlive._() : super();
  factory SessionKeepAlive({
    $core.int? uptimeSeconds,
    $core.int? idleSeconds,
  }) {
    final _result = create();
    if (uptimeSeconds != null) {
      _result.uptimeSeconds = uptimeSeconds;
    }
    if (idleSeconds != null) {
      _result.idleSeconds = idleSeconds;
    }
    return _result;
  }
  factory SessionKeepAlive.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SessionKeepAlive.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SessionKeepAlive clone() => SessionKeepAlive()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SessionKeepAlive copyWith(void Function(SessionKeepAlive) updates) => super.copyWith((message) => updates(message as SessionKeepAlive)) as SessionKeepAlive; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SessionKeepAlive create() => SessionKeepAlive._();
  SessionKeepAlive createEmptyInstance() => create();
  static $pb.PbList<SessionKeepAlive> createRepeated() => $pb.PbList<SessionKeepAlive>();
  @$core.pragma('dart2js:noInline')
  static SessionKeepAlive getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SessionKeepAlive>(create);
  static SessionKeepAlive? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get uptimeSeconds => $_getIZ(0);
  @$pb.TagNumber(1)
  set uptimeSeconds($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUptimeSeconds() => $_has(0);
  @$pb.TagNumber(1)
  void clearUptimeSeconds() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get idleSeconds => $_getIZ(1);
  @$pb.TagNumber(2)
  set idleSeconds($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasIdleSeconds() => $_has(1);
  @$pb.TagNumber(2)
  void clearIdleSeconds() => clearField(2);
}

class SessionClose extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SessionClose', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'remainingSeconds', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  SessionClose._() : super();
  factory SessionClose({
    $core.int? remainingSeconds,
  }) {
    final _result = create();
    if (remainingSeconds != null) {
      _result.remainingSeconds = remainingSeconds;
    }
    return _result;
  }
  factory SessionClose.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SessionClose.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SessionClose clone() => SessionClose()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SessionClose copyWith(void Function(SessionClose) updates) => super.copyWith((message) => updates(message as SessionClose)) as SessionClose; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SessionClose create() => SessionClose._();
  SessionClose createEmptyInstance() => create();
  static $pb.PbList<SessionClose> createRepeated() => $pb.PbList<SessionClose>();
  @$core.pragma('dart2js:noInline')
  static SessionClose getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SessionClose>(create);
  static SessionClose? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get remainingSeconds => $_getIZ(0);
  @$pb.TagNumber(1)
  set remainingSeconds($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasRemainingSeconds() => $_has(0);
  @$pb.TagNumber(1)
  void clearRemainingSeconds() => clearField(1);
}

enum SessionStatus_State {
  keepAlive, 
  close, 
  notSet
}

class SessionStatus extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, SessionStatus_State> _SessionStatus_StateByTag = {
    1 : SessionStatus_State.keepAlive,
    2 : SessionStatus_State.close,
    0 : SessionStatus_State.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SessionStatus', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..oo(0, [1, 2])
    ..aOM<SessionKeepAlive>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'keepAlive', subBuilder: SessionKeepAlive.create)
    ..aOM<SessionClose>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'close', subBuilder: SessionClose.create)
    ..hasRequiredFields = false
  ;

  SessionStatus._() : super();
  factory SessionStatus({
    SessionKeepAlive? keepAlive,
    SessionClose? close,
  }) {
    final _result = create();
    if (keepAlive != null) {
      _result.keepAlive = keepAlive;
    }
    if (close != null) {
      _result.close = close;
    }
    return _result;
  }
  factory SessionStatus.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SessionStatus.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SessionStatus clone() => SessionStatus()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SessionStatus copyWith(void Function(SessionStatus) updates) => super.copyWith((message) => updates(message as SessionStatus)) as SessionStatus; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SessionStatus create() => SessionStatus._();
  SessionStatus createEmptyInstance() => create();
  static $pb.PbList<SessionStatus> createRepeated() => $pb.PbList<SessionStatus>();
  @$core.pragma('dart2js:noInline')
  static SessionStatus getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SessionStatus>(create);
  static SessionStatus? _defaultInstance;

  SessionStatus_State whichState() => _SessionStatus_StateByTag[$_whichOneof(0)]!;
  void clearState() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  SessionKeepAlive get keepAlive => $_getN(0);
  @$pb.TagNumber(1)
  set keepAlive(SessionKeepAlive v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasKeepAlive() => $_has(0);
  @$pb.TagNumber(1)
  void clearKeepAlive() => clearField(1);
  @$pb.TagNumber(1)
  SessionKeepAlive ensureKeepAlive() => $_ensure(0);

  @$pb.TagNumber(2)
  SessionClose get close => $_getN(1);
  @$pb.TagNumber(2)
  set close(SessionClose v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasClose() => $_has(1);
  @$pb.TagNumber(2)
  void clearClose() => clearField(2);
  @$pb.TagNumber(2)
  SessionClose ensureClose() => $_ensure(1);
}

class DataState extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DataState', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..e<FieldState>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'state', $pb.PbFieldType.OE, defaultOrMaker: FieldState.field_state_initialized, valueOf: FieldState.valueOf, enumValues: FieldState.values)
    ..aOM<$2.FieldValue>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value', subBuilder: $2.FieldValue.create)
    ..hasRequiredFields = false
  ;

  DataState._() : super();
  factory DataState({
    $core.String? name,
    FieldState? state,
    $2.FieldValue? value,
  }) {
    final _result = create();
    if (name != null) {
      _result.name = name;
    }
    if (state != null) {
      _result.state = state;
    }
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory DataState.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DataState.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DataState clone() => DataState()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DataState copyWith(void Function(DataState) updates) => super.copyWith((message) => updates(message as DataState)) as DataState; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DataState create() => DataState._();
  DataState createEmptyInstance() => create();
  static $pb.PbList<DataState> createRepeated() => $pb.PbList<DataState>();
  @$core.pragma('dart2js:noInline')
  static DataState getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DataState>(create);
  static DataState? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  FieldState get state => $_getN(1);
  @$pb.TagNumber(2)
  set state(FieldState v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasState() => $_has(1);
  @$pb.TagNumber(2)
  void clearState() => clearField(2);

  @$pb.TagNumber(3)
  $2.FieldValue get value => $_getN(2);
  @$pb.TagNumber(3)
  set value($2.FieldValue v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasValue() => $_has(2);
  @$pb.TagNumber(3)
  void clearValue() => clearField(3);
  @$pb.TagNumber(3)
  $2.FieldValue ensureValue() => $_ensure(2);
}

class OpenRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OpenRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pageId')
    ..hasRequiredFields = false
  ;

  OpenRequest._() : super();
  factory OpenRequest({
    $core.String? pageId,
  }) {
    final _result = create();
    if (pageId != null) {
      _result.pageId = pageId;
    }
    return _result;
  }
  factory OpenRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OpenRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OpenRequest clone() => OpenRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OpenRequest copyWith(void Function(OpenRequest) updates) => super.copyWith((message) => updates(message as OpenRequest)) as OpenRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OpenRequest create() => OpenRequest._();
  OpenRequest createEmptyInstance() => create();
  static $pb.PbList<OpenRequest> createRepeated() => $pb.PbList<OpenRequest>();
  @$core.pragma('dart2js:noInline')
  static OpenRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OpenRequest>(create);
  static OpenRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get pageId => $_getSZ(0);
  @$pb.TagNumber(1)
  set pageId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPageId() => $_has(0);
  @$pb.TagNumber(1)
  void clearPageId() => clearField(1);
}

enum CloseRequest_Reason {
  submitted, 
  dismissed, 
  notSet
}

class CloseRequest extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, CloseRequest_Reason> _CloseRequest_ReasonByTag = {
    2 : CloseRequest_Reason.submitted,
    3 : CloseRequest_Reason.dismissed,
    0 : CloseRequest_Reason.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CloseRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..oo(0, [2, 3])
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pageId')
    ..aOB(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'submitted')
    ..aOB(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'dismissed')
    ..hasRequiredFields = false
  ;

  CloseRequest._() : super();
  factory CloseRequest({
    $core.String? pageId,
    $core.bool? submitted,
    $core.bool? dismissed,
  }) {
    final _result = create();
    if (pageId != null) {
      _result.pageId = pageId;
    }
    if (submitted != null) {
      _result.submitted = submitted;
    }
    if (dismissed != null) {
      _result.dismissed = dismissed;
    }
    return _result;
  }
  factory CloseRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CloseRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CloseRequest clone() => CloseRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CloseRequest copyWith(void Function(CloseRequest) updates) => super.copyWith((message) => updates(message as CloseRequest)) as CloseRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CloseRequest create() => CloseRequest._();
  CloseRequest createEmptyInstance() => create();
  static $pb.PbList<CloseRequest> createRepeated() => $pb.PbList<CloseRequest>();
  @$core.pragma('dart2js:noInline')
  static CloseRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CloseRequest>(create);
  static CloseRequest? _defaultInstance;

  CloseRequest_Reason whichReason() => _CloseRequest_ReasonByTag[$_whichOneof(0)]!;
  void clearReason() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.String get pageId => $_getSZ(0);
  @$pb.TagNumber(1)
  set pageId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPageId() => $_has(0);
  @$pb.TagNumber(1)
  void clearPageId() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get submitted => $_getBF(1);
  @$pb.TagNumber(2)
  set submitted($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSubmitted() => $_has(1);
  @$pb.TagNumber(2)
  void clearSubmitted() => clearField(2);

  @$pb.TagNumber(3)
  $core.bool get dismissed => $_getBF(2);
  @$pb.TagNumber(3)
  set dismissed($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDismissed() => $_has(2);
  @$pb.TagNumber(3)
  void clearDismissed() => clearField(3);
}

enum PageResponse_Response {
  progress, 
  session, 
  notSet
}

class PageResponse extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, PageResponse_Response> _PageResponse_ResponseByTag = {
    2 : PageResponse_Response.progress,
    3 : PageResponse_Response.session,
    0 : PageResponse_Response.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PageResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..oo(0, [2, 3])
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pageId')
    ..aOM<AsyncProgress>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'progress', subBuilder: AsyncProgress.create)
    ..aOM<SessionStatus>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'session', subBuilder: SessionStatus.create)
    ..hasRequiredFields = false
  ;

  PageResponse._() : super();
  factory PageResponse({
    $core.String? pageId,
    AsyncProgress? progress,
    SessionStatus? session,
  }) {
    final _result = create();
    if (pageId != null) {
      _result.pageId = pageId;
    }
    if (progress != null) {
      _result.progress = progress;
    }
    if (session != null) {
      _result.session = session;
    }
    return _result;
  }
  factory PageResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PageResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PageResponse clone() => PageResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PageResponse copyWith(void Function(PageResponse) updates) => super.copyWith((message) => updates(message as PageResponse)) as PageResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PageResponse create() => PageResponse._();
  PageResponse createEmptyInstance() => create();
  static $pb.PbList<PageResponse> createRepeated() => $pb.PbList<PageResponse>();
  @$core.pragma('dart2js:noInline')
  static PageResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PageResponse>(create);
  static PageResponse? _defaultInstance;

  PageResponse_Response whichResponse() => _PageResponse_ResponseByTag[$_whichOneof(0)]!;
  void clearResponse() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.String get pageId => $_getSZ(0);
  @$pb.TagNumber(1)
  set pageId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPageId() => $_has(0);
  @$pb.TagNumber(1)
  void clearPageId() => clearField(1);

  @$pb.TagNumber(2)
  AsyncProgress get progress => $_getN(1);
  @$pb.TagNumber(2)
  set progress(AsyncProgress v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasProgress() => $_has(1);
  @$pb.TagNumber(2)
  void clearProgress() => clearField(2);
  @$pb.TagNumber(2)
  AsyncProgress ensureProgress() => $_ensure(1);

  @$pb.TagNumber(3)
  SessionStatus get session => $_getN(2);
  @$pb.TagNumber(3)
  set session(SessionStatus v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasSession() => $_has(2);
  @$pb.TagNumber(3)
  void clearSession() => clearField(3);
  @$pb.TagNumber(3)
  SessionStatus ensureSession() => $_ensure(2);
}

class DataRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DataRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOM<$2.FieldValue>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value', subBuilder: $2.FieldValue.create)
    ..hasRequiredFields = false
  ;

  DataRequest._() : super();
  factory DataRequest({
    $core.String? name,
    $2.FieldValue? value,
  }) {
    final _result = create();
    if (name != null) {
      _result.name = name;
    }
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory DataRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DataRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DataRequest clone() => DataRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DataRequest copyWith(void Function(DataRequest) updates) => super.copyWith((message) => updates(message as DataRequest)) as DataRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DataRequest create() => DataRequest._();
  DataRequest createEmptyInstance() => create();
  static $pb.PbList<DataRequest> createRepeated() => $pb.PbList<DataRequest>();
  @$core.pragma('dart2js:noInline')
  static DataRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DataRequest>(create);
  static DataRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $2.FieldValue get value => $_getN(1);
  @$pb.TagNumber(2)
  set value($2.FieldValue v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasValue() => $_has(1);
  @$pb.TagNumber(2)
  void clearValue() => clearField(2);
  @$pb.TagNumber(2)
  $2.FieldValue ensureValue() => $_ensure(1);
}

enum ConfirmationResult_State {
  confirmed, 
  failed, 
  notSet
}

class ConfirmationResult extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, ConfirmationResult_State> _ConfirmationResult_StateByTag = {
    1 : ConfirmationResult_State.confirmed,
    2 : ConfirmationResult_State.failed,
    0 : ConfirmationResult_State.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ConfirmationResult', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Pages'), createEmptyInstance: create)
    ..oo(0, [1, 2])
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'confirmed')
    ..aOB(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'failed')
    ..hasRequiredFields = false
  ;

  ConfirmationResult._() : super();
  factory ConfirmationResult({
    $core.bool? confirmed,
    $core.bool? failed,
  }) {
    final _result = create();
    if (confirmed != null) {
      _result.confirmed = confirmed;
    }
    if (failed != null) {
      _result.failed = failed;
    }
    return _result;
  }
  factory ConfirmationResult.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ConfirmationResult.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ConfirmationResult clone() => ConfirmationResult()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ConfirmationResult copyWith(void Function(ConfirmationResult) updates) => super.copyWith((message) => updates(message as ConfirmationResult)) as ConfirmationResult; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ConfirmationResult create() => ConfirmationResult._();
  ConfirmationResult createEmptyInstance() => create();
  static $pb.PbList<ConfirmationResult> createRepeated() => $pb.PbList<ConfirmationResult>();
  @$core.pragma('dart2js:noInline')
  static ConfirmationResult getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ConfirmationResult>(create);
  static ConfirmationResult? _defaultInstance;

  ConfirmationResult_State whichState() => _ConfirmationResult_StateByTag[$_whichOneof(0)]!;
  void clearState() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.bool get confirmed => $_getBF(0);
  @$pb.TagNumber(1)
  set confirmed($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasConfirmed() => $_has(0);
  @$pb.TagNumber(1)
  void clearConfirmed() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get failed => $_getBF(1);
  @$pb.TagNumber(2)
  set failed($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasFailed() => $_has(1);
  @$pb.TagNumber(2)
  void clearFailed() => clearField(2);
}

