import 'package:belfry_ui_generator/src/utils.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';

import '../generated/tables.pb.dart' as $table;

class TableGenerator extends StatelessWidget {
  final $table.Table table;
  final TextStyle Function(int n)? getTextStyle;

  const TableGenerator(this.table, {Key? key, this.getTextStyle})
      : super(key: key);

  _getTextStyle(int n) => getTextStyle != null ? getTextStyle!(n) : null;

  MaterialStateProperty<Color?> get headingRowColor =>
      MaterialStateProperty.all(hexToColor(table.header.color));

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: DataTable2(
        columnSpacing: table.header.columnSpacing,
        horizontalMargin: 12,
        minWidth: 600,
        showCheckboxColumn: table.header.showCheckboxColumn,
        headingTextStyle: table.header.hasTextStyle()
            ? _getTextStyle(table.header.textStyle)
            : null,
        headingRowColor: headingRowColor,
        columns: _getDataColumns().toList(),
        rows: _getDataRows().toList(),
      ),
    );
  }

  Iterable<DataColumn> _getDataColumns() {
    return table.header.items.map((col) {
      return DataColumn2(
        label: Text(
          col.label,
          style: col.hasTextStyle() ? _getTextStyle(col.textStyle) : null,
        ),
        tooltip: col.tooltip,
        size: _getColumnSize(col.size),
        numeric: col.numeric,
        onSort: (int columnIndex, bool ascending) {},
      );
    });
  }

  Iterable<DataRow> _getDataRows() {
    return table.rows.map((row) {
      return DataRow2(
        key: ValueKey<String>(row.key),
        onSelectChanged: (bool? selected) {},
        cells: _getDataCells(row),
      );
    });
  }

  List<DataCell> _getDataCells($table.Row row) {
    var cells = row.cells.values
        .map((cell) => DataCell(
              Text(cell.text),
              onTap: () {},
            ))
        .toList();

// TODO: row acitons
/*     if (row.enableActions) {
      cells.add(DataCell(Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.delete),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.edit),
          ),
        ],
      )));
    } */

    return cells;
  }

  ColumnSize _getColumnSize($table.ColumnSize size) {
    switch (size) {
      case $table.ColumnSize.s:
        return ColumnSize.S;
      case $table.ColumnSize.m:
        return ColumnSize.M;
      case $table.ColumnSize.l:
        return ColumnSize.L;
      default:
        return ColumnSize.M;
    }
  }
}
