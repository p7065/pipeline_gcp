///
//  Generated code. Do not modify.
//  source: forms.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class FileType extends $pb.ProtobufEnum {
  static const FileType any = FileType._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'any');
  static const FileType media = FileType._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'media');
  static const FileType image = FileType._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'image');
  static const FileType video = FileType._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'video');
  static const FileType audio = FileType._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'audio');
  static const FileType custom = FileType._(5, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'custom');

  static const $core.List<FileType> values = <FileType> [
    any,
    media,
    image,
    video,
    audio,
    custom,
  ];

  static final $core.Map<$core.int, FileType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static FileType? valueOf($core.int value) => _byValue[value];

  const FileType._($core.int v, $core.String n) : super(v, n);
}

class Direction extends $pb.ProtobufEnum {
  static const Direction horizontal = Direction._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'horizontal');
  static const Direction vertical = Direction._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'vertical');

  static const $core.List<Direction> values = <Direction> [
    horizontal,
    vertical,
  ];

  static final $core.Map<$core.int, Direction> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Direction? valueOf($core.int value) => _byValue[value];

  const Direction._($core.int v, $core.String n) : super(v, n);
}

class VerticalDirection extends $pb.ProtobufEnum {
  static const VerticalDirection up = VerticalDirection._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'up');
  static const VerticalDirection down = VerticalDirection._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'down');

  static const $core.List<VerticalDirection> values = <VerticalDirection> [
    up,
    down,
  ];

  static final $core.Map<$core.int, VerticalDirection> _byValue = $pb.ProtobufEnum.initByValue(values);
  static VerticalDirection? valueOf($core.int value) => _byValue[value];

  const VerticalDirection._($core.int v, $core.String n) : super(v, n);
}

class Alignment extends $pb.ProtobufEnum {
  static const Alignment start = Alignment._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'start');
  static const Alignment end = Alignment._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'end');
  static const Alignment center = Alignment._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'center');
  static const Alignment space_around = Alignment._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'space_around');
  static const Alignment space_between = Alignment._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'space_between');
  static const Alignment space_evenly = Alignment._(5, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'space_evenly');

  static const $core.List<Alignment> values = <Alignment> [
    start,
    end,
    center,
    space_around,
    space_between,
    space_evenly,
  ];

  static final $core.Map<$core.int, Alignment> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Alignment? valueOf($core.int value) => _byValue[value];

  const Alignment._($core.int v, $core.String n) : super(v, n);
}

