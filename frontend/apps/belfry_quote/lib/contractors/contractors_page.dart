import 'dart:typed_data';

import 'package:belfry_ui_generator/belfry_ui_generator.dart' as $uigenerator;
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../app/app_theme.dart';
import '../app/side_navigation.dart';

class ImportDialog extends StatefulWidget {
  final void Function()? onSaved;

  const ImportDialog({Key? key, this.onSaved}) : super(key: key);

  @override
  State<ImportDialog> createState() => _ImportDialogState();
}

class _ImportDialogState extends State<ImportDialog> {
  late final $uigenerator.FormGenerator _form;
  bool _isButtonDisabled = true;

  @override
  void initState() {
    _createImportForm();
    super.initState();
  }

  void _createImportForm() {
    var filepicker = $uigenerator.Container(
      formField: $uigenerator.FormField(
        fieldId: 'Contractors_RequestFile',
        label: 'Alvállakozó lista improtálás',
        control: $uigenerator.FieldControl(
            filePicker: $uigenerator.FilePicker(
          allowMultiple: false,
          fileTypeIndex: 5,
          withReadStream: true,
        )),
      ),
    );

    var layout = $uigenerator.Layout(containers: [filepicker]);

    var supportedFiles = $uigenerator.EnumeratedField();
    supportedFiles.values.add($uigenerator.KeyValue(
      key: 'xlsx',
      text: 'Excel',
    ));
    supportedFiles.values.add($uigenerator.KeyValue(
      key: 'csv',
      text: 'CSV',
    ));

    var schema = $uigenerator.Schema(
      schemaId: 'ContractorsInput',
      type: $uigenerator.EntityType.poco,
      version: $uigenerator.SchemaVersion(),
      fields: [
        $uigenerator.Field(
          fieldId: 'Contractors_RequestFile',
          enumerated: supportedFiles,
        )
      ],
    );

    var selectedFiles = $uigenerator.EnumeratedValue();
    selectedFiles.keys.add('xlxs');
    selectedFiles.keys.add('csv');

    var item = $uigenerator.Item(
        schemaId: 'ContractorsInput',
        itemId: '41e879aa-859c-4bda-b77b-37b7e07c6674',
        values: [
          $uigenerator.FieldValue(
            fieldId: 'Contractors_RequestFile',
            list: selectedFiles,
          )
        ]);

    _form = $uigenerator.FormGenerator(
      layout: layout,
      schema: schema,
      item: item,
      onChanged: _onChanged,
    );
  }

  void _onChanged(dynamic val) {
    if (!val.isEmpty) {
      setState(() {
        _isButtonDisabled = false;
      });
    } else {
      setState(() {
        _isButtonDisabled = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Container(
          decoration: AppTheme.roundedShadowBoxDecoration,
          width: 400,
          height: 300,
          padding: EdgeInsets.all(40),
          child: Column(
            children: [
              Text(
                'Fájl kiválasztása',
                style: Get.theme.textTheme.headline2,
              ),
              SizedBox(
                height: 30,
              ),
              _form,
              SizedBox(
                height: 30,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: MaterialButton(
                      color: Theme.of(context).colorScheme.secondary,
                      child: const Text(
                        "Importálás",
                      ),
                      onPressed: _isButtonDisabled
                          ? null
                          : () {
                              var result = _form.getFormValue();
                              if (result.isNotEmpty) {
                                Uint8List fileBytes =
                                    result['Contractors_RequestFile']
                                        .first
                                        .bytes;
                                String fileName =
                                    result['Contractors_RequestFile']
                                        .first
                                        .name;

                                // print('import: ${fileName}, ${fileBytes}');
                                //TODO: service update call
                              }
                            },
                    ),
                  ),
                  const SizedBox(width: 20),
                  Expanded(
                    child: MaterialButton(
                      color: Theme.of(context).colorScheme.secondary,
                      child: const Text(
                        "Mégse",
                      ),
                      onPressed: () {
                        _form.resetForm();
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ContractorsPage extends StatelessWidget {
  const ContractorsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Row(
      children: [
        const SideNavigation(),
        Expanded(
          child: Container(
            color: AppTheme.background,
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'Alvállalkozók',
                  style: AppTheme.h1,
                ),
                const SizedBox(
                  height: 40,
                ),
                Expanded(
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          ElevatedButton(
                            onPressed: () => {
                              Get.dialog(
                                ImportDialog(
                                  onSaved: () {},
                                ),
                                // barrierColor: Colors.red,
                                transitionDuration: Duration(milliseconds: 500),
                              )
                            },
                            child: Text('Importálás'),
                          ),
                        ],
                      ),
                      Text('alvállakozó lista ide jön...')
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ));
  }
}
