﻿using Autofac;

namespace Belfry.Infrastructure
{
    public interface ICaminoModule
    {
        void InitializeComponent(ILifetimeScope container);
    }
}
