///
//  Generated code. Do not modify.
//  source: forms.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use fileTypeDescriptor instead')
const FileType$json = const {
  '1': 'FileType',
  '2': const [
    const {'1': 'any', '2': 0},
    const {'1': 'media', '2': 1},
    const {'1': 'image', '2': 2},
    const {'1': 'video', '2': 3},
    const {'1': 'audio', '2': 4},
    const {'1': 'custom', '2': 5},
  ],
};

/// Descriptor for `FileType`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List fileTypeDescriptor = $convert.base64Decode('CghGaWxlVHlwZRIHCgNhbnkQABIJCgVtZWRpYRABEgkKBWltYWdlEAISCQoFdmlkZW8QAxIJCgVhdWRpbxAEEgoKBmN1c3RvbRAF');
@$core.Deprecated('Use directionDescriptor instead')
const Direction$json = const {
  '1': 'Direction',
  '2': const [
    const {'1': 'horizontal', '2': 0},
    const {'1': 'vertical', '2': 1},
  ],
};

/// Descriptor for `Direction`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List directionDescriptor = $convert.base64Decode('CglEaXJlY3Rpb24SDgoKaG9yaXpvbnRhbBAAEgwKCHZlcnRpY2FsEAE=');
@$core.Deprecated('Use verticalDirectionDescriptor instead')
const VerticalDirection$json = const {
  '1': 'VerticalDirection',
  '2': const [
    const {'1': 'up', '2': 0},
    const {'1': 'down', '2': 1},
  ],
};

/// Descriptor for `VerticalDirection`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List verticalDirectionDescriptor = $convert.base64Decode('ChFWZXJ0aWNhbERpcmVjdGlvbhIGCgJ1cBAAEggKBGRvd24QAQ==');
@$core.Deprecated('Use alignmentDescriptor instead')
const Alignment$json = const {
  '1': 'Alignment',
  '2': const [
    const {'1': 'start', '2': 0},
    const {'1': 'end', '2': 1},
    const {'1': 'center', '2': 2},
    const {'1': 'space_around', '2': 3},
    const {'1': 'space_between', '2': 4},
    const {'1': 'space_evenly', '2': 5},
  ],
};

/// Descriptor for `Alignment`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List alignmentDescriptor = $convert.base64Decode('CglBbGlnbm1lbnQSCQoFc3RhcnQQABIHCgNlbmQQARIKCgZjZW50ZXIQAhIQCgxzcGFjZV9hcm91bmQQAxIRCg1zcGFjZV9iZXR3ZWVuEAQSEAoMc3BhY2VfZXZlbmx5EAU=');
@$core.Deprecated('Use requiredDescriptor instead')
const Required$json = const {
  '1': 'Required',
};

/// Descriptor for `Required`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List requiredDescriptor = $convert.base64Decode('CghSZXF1aXJlZA==');
@$core.Deprecated('Use equalDescriptor instead')
const Equal$json = const {
  '1': 'Equal',
  '2': const [
    const {'1': 'value', '3': 1, '4': 1, '5': 11, '6': '.Fields.FieldValue', '10': 'value'},
  ],
};

/// Descriptor for `Equal`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List equalDescriptor = $convert.base64Decode('CgVFcXVhbBIoCgV2YWx1ZRgBIAEoCzISLkZpZWxkcy5GaWVsZFZhbHVlUgV2YWx1ZQ==');
@$core.Deprecated('Use notEqualDescriptor instead')
const NotEqual$json = const {
  '1': 'NotEqual',
  '2': const [
    const {'1': 'value', '3': 1, '4': 1, '5': 11, '6': '.Fields.FieldValue', '10': 'value'},
  ],
};

/// Descriptor for `NotEqual`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List notEqualDescriptor = $convert.base64Decode('CghOb3RFcXVhbBIoCgV2YWx1ZRgBIAEoCzISLkZpZWxkcy5GaWVsZFZhbHVlUgV2YWx1ZQ==');
@$core.Deprecated('Use minDescriptor instead')
const Min$json = const {
  '1': 'Min',
  '2': const [
    const {'1': 'min', '3': 1, '4': 1, '5': 1, '10': 'min'},
    const {'1': 'inclusive', '3': 2, '4': 1, '5': 8, '10': 'inclusive'},
  ],
};

/// Descriptor for `Min`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List minDescriptor = $convert.base64Decode('CgNNaW4SEAoDbWluGAEgASgBUgNtaW4SHAoJaW5jbHVzaXZlGAIgASgIUglpbmNsdXNpdmU=');
@$core.Deprecated('Use maxDescriptor instead')
const Max$json = const {
  '1': 'Max',
  '2': const [
    const {'1': 'max', '3': 1, '4': 1, '5': 1, '10': 'max'},
    const {'1': 'inclusive', '3': 2, '4': 1, '5': 8, '10': 'inclusive'},
  ],
};

/// Descriptor for `Max`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List maxDescriptor = $convert.base64Decode('CgNNYXgSEAoDbWF4GAEgASgBUgNtYXgSHAoJaW5jbHVzaXZlGAIgASgIUglpbmNsdXNpdmU=');
@$core.Deprecated('Use minLengthDescriptor instead')
const MinLength$json = const {
  '1': 'MinLength',
  '2': const [
    const {'1': 'min_length', '3': 1, '4': 1, '5': 5, '10': 'minLength'},
    const {'1': 'allow_empty', '3': 2, '4': 1, '5': 8, '10': 'allowEmpty'},
  ],
};

/// Descriptor for `MinLength`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List minLengthDescriptor = $convert.base64Decode('CglNaW5MZW5ndGgSHQoKbWluX2xlbmd0aBgBIAEoBVIJbWluTGVuZ3RoEh8KC2FsbG93X2VtcHR5GAIgASgIUgphbGxvd0VtcHR5');
@$core.Deprecated('Use maxLengthDescriptor instead')
const MaxLength$json = const {
  '1': 'MaxLength',
  '2': const [
    const {'1': 'max_length', '3': 1, '4': 1, '5': 5, '10': 'maxLength'},
  ],
};

/// Descriptor for `MaxLength`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List maxLengthDescriptor = $convert.base64Decode('CglNYXhMZW5ndGgSHQoKbWF4X2xlbmd0aBgBIAEoBVIJbWF4TGVuZ3Ro');
@$core.Deprecated('Use matchDescriptor instead')
const Match$json = const {
  '1': 'Match',
  '2': const [
    const {'1': 'pattern', '3': 1, '4': 1, '5': 9, '10': 'pattern'},
  ],
};

/// Descriptor for `Match`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List matchDescriptor = $convert.base64Decode('CgVNYXRjaBIYCgdwYXR0ZXJuGAEgASgJUgdwYXR0ZXJu');
@$core.Deprecated('Use validatorDescriptor instead')
const Validator$json = const {
  '1': 'Validator',
  '2': const [
    const {'1': 'error_text', '3': 1, '4': 1, '5': 9, '9': 1, '10': 'errorText', '17': true},
    const {'1': 'required', '3': 2, '4': 1, '5': 11, '6': '.Forms.Required', '9': 0, '10': 'required'},
    const {'1': 'equal', '3': 3, '4': 1, '5': 11, '6': '.Forms.Equal', '9': 0, '10': 'equal'},
    const {'1': 'not_equal', '3': 4, '4': 1, '5': 11, '6': '.Forms.NotEqual', '9': 0, '10': 'notEqual'},
    const {'1': 'min', '3': 5, '4': 1, '5': 11, '6': '.Forms.Min', '9': 0, '10': 'min'},
    const {'1': 'max', '3': 6, '4': 1, '5': 11, '6': '.Forms.Max', '9': 0, '10': 'max'},
    const {'1': 'min_length', '3': 7, '4': 1, '5': 11, '6': '.Forms.MinLength', '9': 0, '10': 'minLength'},
    const {'1': 'max_length', '3': 8, '4': 1, '5': 11, '6': '.Forms.MaxLength', '9': 0, '10': 'maxLength'},
    const {'1': 'match', '3': 9, '4': 1, '5': 11, '6': '.Forms.Match', '9': 0, '10': 'match'},
  ],
  '8': const [
    const {'1': 'type'},
    const {'1': '_error_text'},
  ],
};

/// Descriptor for `Validator`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List validatorDescriptor = $convert.base64Decode('CglWYWxpZGF0b3ISIgoKZXJyb3JfdGV4dBgBIAEoCUgBUgllcnJvclRleHSIAQESLQoIcmVxdWlyZWQYAiABKAsyDy5Gb3Jtcy5SZXF1aXJlZEgAUghyZXF1aXJlZBIkCgVlcXVhbBgDIAEoCzIMLkZvcm1zLkVxdWFsSABSBWVxdWFsEi4KCW5vdF9lcXVhbBgEIAEoCzIPLkZvcm1zLk5vdEVxdWFsSABSCG5vdEVxdWFsEh4KA21pbhgFIAEoCzIKLkZvcm1zLk1pbkgAUgNtaW4SHgoDbWF4GAYgASgLMgouRm9ybXMuTWF4SABSA21heBIxCgptaW5fbGVuZ3RoGAcgASgLMhAuRm9ybXMuTWluTGVuZ3RoSABSCW1pbkxlbmd0aBIxCgptYXhfbGVuZ3RoGAggASgLMhAuRm9ybXMuTWF4TGVuZ3RoSABSCW1heExlbmd0aBIkCgVtYXRjaBgJIAEoCzIMLkZvcm1zLk1hdGNoSABSBW1hdGNoQgYKBHR5cGVCDQoLX2Vycm9yX3RleHQ=');
@$core.Deprecated('Use formRowDescriptor instead')
const FormRow$json = const {
  '1': 'FormRow',
  '2': const [
    const {'1': 'children', '3': 2, '4': 3, '5': 11, '6': '.Forms.Container', '10': 'children'},
  ],
};

/// Descriptor for `FormRow`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List formRowDescriptor = $convert.base64Decode('CgdGb3JtUm93EiwKCGNoaWxkcmVuGAIgAygLMhAuRm9ybXMuQ29udGFpbmVyUghjaGlsZHJlbg==');
@$core.Deprecated('Use formColumnDescriptor instead')
const FormColumn$json = const {
  '1': 'FormColumn',
  '2': const [
    const {'1': 'children', '3': 2, '4': 3, '5': 11, '6': '.Forms.Container', '10': 'children'},
  ],
};

/// Descriptor for `FormColumn`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List formColumnDescriptor = $convert.base64Decode('CgpGb3JtQ29sdW1uEiwKCGNoaWxkcmVuGAIgAygLMhAuRm9ybXMuQ29udGFpbmVyUghjaGlsZHJlbg==');
@$core.Deprecated('Use fieldControlDescriptor instead')
const FieldControl$json = const {
  '1': 'FieldControl',
  '2': const [
    const {'1': 'checkbox', '3': 1, '4': 1, '5': 11, '6': '.Forms.Checkbox', '9': 0, '10': 'checkbox'},
    const {'1': 'checkbox_group', '3': 2, '4': 1, '5': 11, '6': '.Forms.CheckboxGroup', '9': 0, '10': 'checkboxGroup'},
    const {'1': 'choice_chip', '3': 3, '4': 1, '5': 11, '6': '.Forms.ChoiceChip', '9': 0, '10': 'choiceChip'},
    const {'1': 'date_range_picker', '3': 4, '4': 1, '5': 11, '6': '.Forms.DateRangePicker', '9': 0, '10': 'dateRangePicker'},
    const {'1': 'date_time_picker', '3': 5, '4': 1, '5': 11, '6': '.Forms.DateTimePicker', '9': 0, '10': 'dateTimePicker'},
    const {'1': 'dropdown', '3': 6, '4': 1, '5': 11, '6': '.Forms.Dropdown', '9': 0, '10': 'dropdown'},
    const {'1': 'filter_chip', '3': 7, '4': 1, '5': 11, '6': '.Forms.FilterChip', '9': 0, '10': 'filterChip'},
    const {'1': 'radio_group', '3': 8, '4': 1, '5': 11, '6': '.Forms.RadioGroup', '9': 0, '10': 'radioGroup'},
    const {'1': 'range_slider', '3': 9, '4': 1, '5': 11, '6': '.Forms.RangeSlider', '9': 0, '10': 'rangeSlider'},
    const {'1': 'segmented_control', '3': 10, '4': 1, '5': 11, '6': '.Forms.SegmentedControl', '9': 0, '10': 'segmentedControl'},
    const {'1': 'slider', '3': 11, '4': 1, '5': 11, '6': '.Forms.Slider', '9': 0, '10': 'slider'},
    const {'1': 'switch_field', '3': 12, '4': 1, '5': 11, '6': '.Forms.Switch', '9': 0, '10': 'switchField'},
    const {'1': 'text_field', '3': 13, '4': 1, '5': 11, '6': '.Forms.TextField', '9': 0, '10': 'textField'},
    const {'1': 'searchable_dropdown', '3': 14, '4': 1, '5': 11, '6': '.Forms.SearchableDropdown', '9': 0, '10': 'searchableDropdown'},
    const {'1': 'color_picker', '3': 15, '4': 1, '5': 11, '6': '.Forms.ColorPicker', '9': 0, '10': 'colorPicker'},
    const {'1': 'cupertino_date_time_picker', '3': 16, '4': 1, '5': 11, '6': '.Forms.CupertinoDateTimePicker', '9': 0, '10': 'cupertinoDateTimePicker'},
    const {'1': 'type_ahead', '3': 17, '4': 1, '5': 11, '6': '.Forms.TypeAhead', '9': 0, '10': 'typeAhead'},
    const {'1': 'touch_spin', '3': 18, '4': 1, '5': 11, '6': '.Forms.TouchSpin', '9': 0, '10': 'touchSpin'},
    const {'1': 'rating', '3': 19, '4': 1, '5': 11, '6': '.Forms.Rating', '9': 0, '10': 'rating'},
    const {'1': 'signature_pad', '3': 20, '4': 1, '5': 11, '6': '.Forms.SignaturePad', '9': 0, '10': 'signaturePad'},
    const {'1': 'file_picker', '3': 21, '4': 1, '5': 11, '6': '.Forms.FilePicker', '9': 0, '10': 'filePicker'},
    const {'1': 'chips_input', '3': 22, '4': 1, '5': 11, '6': '.Forms.ChipsInput', '9': 0, '10': 'chipsInput'},
  ],
  '8': const [
    const {'1': 'type'},
  ],
};

/// Descriptor for `FieldControl`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List fieldControlDescriptor = $convert.base64Decode('CgxGaWVsZENvbnRyb2wSLQoIY2hlY2tib3gYASABKAsyDy5Gb3Jtcy5DaGVja2JveEgAUghjaGVja2JveBI9Cg5jaGVja2JveF9ncm91cBgCIAEoCzIULkZvcm1zLkNoZWNrYm94R3JvdXBIAFINY2hlY2tib3hHcm91cBI0CgtjaG9pY2VfY2hpcBgDIAEoCzIRLkZvcm1zLkNob2ljZUNoaXBIAFIKY2hvaWNlQ2hpcBJEChFkYXRlX3JhbmdlX3BpY2tlchgEIAEoCzIWLkZvcm1zLkRhdGVSYW5nZVBpY2tlckgAUg9kYXRlUmFuZ2VQaWNrZXISQQoQZGF0ZV90aW1lX3BpY2tlchgFIAEoCzIVLkZvcm1zLkRhdGVUaW1lUGlja2VySABSDmRhdGVUaW1lUGlja2VyEi0KCGRyb3Bkb3duGAYgASgLMg8uRm9ybXMuRHJvcGRvd25IAFIIZHJvcGRvd24SNAoLZmlsdGVyX2NoaXAYByABKAsyES5Gb3Jtcy5GaWx0ZXJDaGlwSABSCmZpbHRlckNoaXASNAoLcmFkaW9fZ3JvdXAYCCABKAsyES5Gb3Jtcy5SYWRpb0dyb3VwSABSCnJhZGlvR3JvdXASNwoMcmFuZ2Vfc2xpZGVyGAkgASgLMhIuRm9ybXMuUmFuZ2VTbGlkZXJIAFILcmFuZ2VTbGlkZXISRgoRc2VnbWVudGVkX2NvbnRyb2wYCiABKAsyFy5Gb3Jtcy5TZWdtZW50ZWRDb250cm9sSABSEHNlZ21lbnRlZENvbnRyb2wSJwoGc2xpZGVyGAsgASgLMg0uRm9ybXMuU2xpZGVySABSBnNsaWRlchIyCgxzd2l0Y2hfZmllbGQYDCABKAsyDS5Gb3Jtcy5Td2l0Y2hIAFILc3dpdGNoRmllbGQSMQoKdGV4dF9maWVsZBgNIAEoCzIQLkZvcm1zLlRleHRGaWVsZEgAUgl0ZXh0RmllbGQSTAoTc2VhcmNoYWJsZV9kcm9wZG93bhgOIAEoCzIZLkZvcm1zLlNlYXJjaGFibGVEcm9wZG93bkgAUhJzZWFyY2hhYmxlRHJvcGRvd24SNwoMY29sb3JfcGlja2VyGA8gASgLMhIuRm9ybXMuQ29sb3JQaWNrZXJIAFILY29sb3JQaWNrZXISXQoaY3VwZXJ0aW5vX2RhdGVfdGltZV9waWNrZXIYECABKAsyHi5Gb3Jtcy5DdXBlcnRpbm9EYXRlVGltZVBpY2tlckgAUhdjdXBlcnRpbm9EYXRlVGltZVBpY2tlchIxCgp0eXBlX2FoZWFkGBEgASgLMhAuRm9ybXMuVHlwZUFoZWFkSABSCXR5cGVBaGVhZBIxCgp0b3VjaF9zcGluGBIgASgLMhAuRm9ybXMuVG91Y2hTcGluSABSCXRvdWNoU3BpbhInCgZyYXRpbmcYEyABKAsyDS5Gb3Jtcy5SYXRpbmdIAFIGcmF0aW5nEjoKDXNpZ25hdHVyZV9wYWQYFCABKAsyEy5Gb3Jtcy5TaWduYXR1cmVQYWRIAFIMc2lnbmF0dXJlUGFkEjQKC2ZpbGVfcGlja2VyGBUgASgLMhEuRm9ybXMuRmlsZVBpY2tlckgAUgpmaWxlUGlja2VyEjQKC2NoaXBzX2lucHV0GBYgASgLMhEuRm9ybXMuQ2hpcHNJbnB1dEgAUgpjaGlwc0lucHV0QgYKBHR5cGU=');
@$core.Deprecated('Use checkboxDescriptor instead')
const Checkbox$json = const {
  '1': 'Checkbox',
  '2': const [
    const {'1': 'peculiar', '3': 1, '4': 1, '5': 11, '6': '.Forms.CheckboxPeculiar', '10': 'peculiar'},
    const {'1': 'autofocus', '3': 2, '4': 1, '5': 8, '10': 'autofocus'},
    const {'1': 'selected', '3': 3, '4': 1, '5': 8, '10': 'selected'},
    const {'1': 'subtitle', '3': 4, '4': 1, '5': 11, '6': '.Forms.Text', '10': 'subtitle'},
  ],
};

/// Descriptor for `Checkbox`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List checkboxDescriptor = $convert.base64Decode('CghDaGVja2JveBIzCghwZWN1bGlhchgBIAEoCzIXLkZvcm1zLkNoZWNrYm94UGVjdWxpYXJSCHBlY3VsaWFyEhwKCWF1dG9mb2N1cxgCIAEoCFIJYXV0b2ZvY3VzEhoKCHNlbGVjdGVkGAMgASgIUghzZWxlY3RlZBInCghzdWJ0aXRsZRgEIAEoCzILLkZvcm1zLlRleHRSCHN1YnRpdGxl');
@$core.Deprecated('Use checkboxGroupDescriptor instead')
const CheckboxGroup$json = const {
  '1': 'CheckboxGroup',
  '2': const [
    const {'1': 'peculiar', '3': 1, '4': 1, '5': 11, '6': '.Forms.CheckboxPeculiar', '10': 'peculiar'},
    const {'1': 'focus_color', '3': 2, '4': 1, '5': 9, '10': 'focusColor'},
    const {'1': 'hover_color', '3': 3, '4': 1, '5': 9, '10': 'hoverColor'},
    const {'1': 'wrap', '3': 4, '4': 1, '5': 11, '6': '.Forms.Wrap', '9': 0, '10': 'wrap'},
    const {'1': 'direction', '3': 5, '4': 1, '5': 14, '6': '.Forms.Direction', '9': 0, '10': 'direction'},
  ],
  '8': const [
    const {'1': 'orientation'},
  ],
};

/// Descriptor for `CheckboxGroup`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List checkboxGroupDescriptor = $convert.base64Decode('Cg1DaGVja2JveEdyb3VwEjMKCHBlY3VsaWFyGAEgASgLMhcuRm9ybXMuQ2hlY2tib3hQZWN1bGlhclIIcGVjdWxpYXISHwoLZm9jdXNfY29sb3IYAiABKAlSCmZvY3VzQ29sb3ISHwoLaG92ZXJfY29sb3IYAyABKAlSCmhvdmVyQ29sb3ISIQoEd3JhcBgEIAEoCzILLkZvcm1zLldyYXBIAFIEd3JhcBIwCglkaXJlY3Rpb24YBSABKA4yEC5Gb3Jtcy5EaXJlY3Rpb25IAFIJZGlyZWN0aW9uQg0KC29yaWVudGF0aW9u');
@$core.Deprecated('Use choiceChipDescriptor instead')
const ChoiceChip$json = const {
  '1': 'ChoiceChip',
};

/// Descriptor for `ChoiceChip`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List choiceChipDescriptor = $convert.base64Decode('CgpDaG9pY2VDaGlw');
@$core.Deprecated('Use dateRangePickerDescriptor instead')
const DateRangePicker$json = const {
  '1': 'DateRangePicker',
};

/// Descriptor for `DateRangePicker`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List dateRangePickerDescriptor = $convert.base64Decode('Cg9EYXRlUmFuZ2VQaWNrZXI=');
@$core.Deprecated('Use dateTimePickerDescriptor instead')
const DateTimePicker$json = const {
  '1': 'DateTimePicker',
};

/// Descriptor for `DateTimePicker`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List dateTimePickerDescriptor = $convert.base64Decode('Cg5EYXRlVGltZVBpY2tlcg==');
@$core.Deprecated('Use dropdownDescriptor instead')
const Dropdown$json = const {
  '1': 'Dropdown',
};

/// Descriptor for `Dropdown`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List dropdownDescriptor = $convert.base64Decode('CghEcm9wZG93bg==');
@$core.Deprecated('Use filterChipDescriptor instead')
const FilterChip$json = const {
  '1': 'FilterChip',
};

/// Descriptor for `FilterChip`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List filterChipDescriptor = $convert.base64Decode('CgpGaWx0ZXJDaGlw');
@$core.Deprecated('Use radioGroupDescriptor instead')
const RadioGroup$json = const {
  '1': 'RadioGroup',
};

/// Descriptor for `RadioGroup`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List radioGroupDescriptor = $convert.base64Decode('CgpSYWRpb0dyb3Vw');
@$core.Deprecated('Use rangeSliderDescriptor instead')
const RangeSlider$json = const {
  '1': 'RangeSlider',
};

/// Descriptor for `RangeSlider`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List rangeSliderDescriptor = $convert.base64Decode('CgtSYW5nZVNsaWRlcg==');
@$core.Deprecated('Use segmentedControlDescriptor instead')
const SegmentedControl$json = const {
  '1': 'SegmentedControl',
};

/// Descriptor for `SegmentedControl`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List segmentedControlDescriptor = $convert.base64Decode('ChBTZWdtZW50ZWRDb250cm9s');
@$core.Deprecated('Use sliderDescriptor instead')
const Slider$json = const {
  '1': 'Slider',
};

/// Descriptor for `Slider`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sliderDescriptor = $convert.base64Decode('CgZTbGlkZXI=');
@$core.Deprecated('Use switchDescriptor instead')
const Switch$json = const {
  '1': 'Switch',
};

/// Descriptor for `Switch`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List switchDescriptor = $convert.base64Decode('CgZTd2l0Y2g=');
@$core.Deprecated('Use textFieldDescriptor instead')
const TextField$json = const {
  '1': 'TextField',
  '2': const [
    const {'1': 'max_lines', '3': 1, '4': 1, '5': 5, '10': 'maxLines'},
    const {'1': 'min_lines', '3': 2, '4': 1, '5': 5, '10': 'minLines'},
    const {'1': 'text_style', '3': 3, '4': 1, '5': 5, '10': 'textStyle'},
    const {'1': 'max_length', '3': 4, '4': 1, '5': 5, '10': 'maxLength'},
  ],
};

/// Descriptor for `TextField`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List textFieldDescriptor = $convert.base64Decode('CglUZXh0RmllbGQSGwoJbWF4X2xpbmVzGAEgASgFUghtYXhMaW5lcxIbCgltaW5fbGluZXMYAiABKAVSCG1pbkxpbmVzEh0KCnRleHRfc3R5bGUYAyABKAVSCXRleHRTdHlsZRIdCgptYXhfbGVuZ3RoGAQgASgFUgltYXhMZW5ndGg=');
@$core.Deprecated('Use searchableDropdownDescriptor instead')
const SearchableDropdown$json = const {
  '1': 'SearchableDropdown',
};

/// Descriptor for `SearchableDropdown`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List searchableDropdownDescriptor = $convert.base64Decode('ChJTZWFyY2hhYmxlRHJvcGRvd24=');
@$core.Deprecated('Use colorPickerDescriptor instead')
const ColorPicker$json = const {
  '1': 'ColorPicker',
};

/// Descriptor for `ColorPicker`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List colorPickerDescriptor = $convert.base64Decode('CgtDb2xvclBpY2tlcg==');
@$core.Deprecated('Use cupertinoDateTimePickerDescriptor instead')
const CupertinoDateTimePicker$json = const {
  '1': 'CupertinoDateTimePicker',
};

/// Descriptor for `CupertinoDateTimePicker`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List cupertinoDateTimePickerDescriptor = $convert.base64Decode('ChdDdXBlcnRpbm9EYXRlVGltZVBpY2tlcg==');
@$core.Deprecated('Use typeAheadDescriptor instead')
const TypeAhead$json = const {
  '1': 'TypeAhead',
};

/// Descriptor for `TypeAhead`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List typeAheadDescriptor = $convert.base64Decode('CglUeXBlQWhlYWQ=');
@$core.Deprecated('Use touchSpinDescriptor instead')
const TouchSpin$json = const {
  '1': 'TouchSpin',
};

/// Descriptor for `TouchSpin`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List touchSpinDescriptor = $convert.base64Decode('CglUb3VjaFNwaW4=');
@$core.Deprecated('Use ratingDescriptor instead')
const Rating$json = const {
  '1': 'Rating',
};

/// Descriptor for `Rating`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List ratingDescriptor = $convert.base64Decode('CgZSYXRpbmc=');
@$core.Deprecated('Use signaturePadDescriptor instead')
const SignaturePad$json = const {
  '1': 'SignaturePad',
};

/// Descriptor for `SignaturePad`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List signaturePadDescriptor = $convert.base64Decode('CgxTaWduYXR1cmVQYWQ=');
@$core.Deprecated('Use filePickerDescriptor instead')
const FilePicker$json = const {
  '1': 'FilePicker',
  '2': const [
    const {'1': 'allow_multiple', '3': 1, '4': 1, '5': 8, '10': 'allowMultiple'},
    const {'1': 'with_data', '3': 2, '4': 1, '5': 8, '10': 'withData'},
    const {'1': 'with_read_stream', '3': 3, '4': 1, '5': 8, '10': 'withReadStream'},
    const {'1': 'max_files', '3': 4, '4': 1, '5': 5, '10': 'maxFiles'},
    const {'1': 'preview_images', '3': 5, '4': 1, '5': 8, '10': 'previewImages'},
    const {'1': 'file_type_index', '3': 6, '4': 1, '5': 5, '10': 'fileTypeIndex'},
  ],
};

/// Descriptor for `FilePicker`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List filePickerDescriptor = $convert.base64Decode('CgpGaWxlUGlja2VyEiUKDmFsbG93X211bHRpcGxlGAEgASgIUg1hbGxvd011bHRpcGxlEhsKCXdpdGhfZGF0YRgCIAEoCFIId2l0aERhdGESKAoQd2l0aF9yZWFkX3N0cmVhbRgDIAEoCFIOd2l0aFJlYWRTdHJlYW0SGwoJbWF4X2ZpbGVzGAQgASgFUghtYXhGaWxlcxIlCg5wcmV2aWV3X2ltYWdlcxgFIAEoCFINcHJldmlld0ltYWdlcxImCg9maWxlX3R5cGVfaW5kZXgYBiABKAVSDWZpbGVUeXBlSW5kZXg=');
@$core.Deprecated('Use chipsInputDescriptor instead')
const ChipsInput$json = const {
  '1': 'ChipsInput',
  '2': const [
    const {'1': 'max_chips', '3': 1, '4': 1, '5': 5, '10': 'maxChips'},
  ],
};

/// Descriptor for `ChipsInput`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List chipsInputDescriptor = $convert.base64Decode('CgpDaGlwc0lucHV0EhsKCW1heF9jaGlwcxgBIAEoBVIIbWF4Q2hpcHM=');
@$core.Deprecated('Use checkboxPeculiarDescriptor instead')
const CheckboxPeculiar$json = const {
  '1': 'CheckboxPeculiar',
  '2': const [
    const {'1': 'active_color', '3': 1, '4': 1, '5': 9, '10': 'activeColor'},
    const {'1': 'check_color', '3': 2, '4': 1, '5': 9, '10': 'checkColor'},
    const {'1': 'tristate', '3': 3, '4': 1, '5': 8, '10': 'tristate'},
  ],
};

/// Descriptor for `CheckboxPeculiar`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List checkboxPeculiarDescriptor = $convert.base64Decode('ChBDaGVja2JveFBlY3VsaWFyEiEKDGFjdGl2ZV9jb2xvchgBIAEoCVILYWN0aXZlQ29sb3ISHwoLY2hlY2tfY29sb3IYAiABKAlSCmNoZWNrQ29sb3ISGgoIdHJpc3RhdGUYAyABKAhSCHRyaXN0YXRl');
@$core.Deprecated('Use wrapDescriptor instead')
const Wrap$json = const {
  '1': 'Wrap',
  '2': const [
    const {'1': 'direction', '3': 1, '4': 1, '5': 14, '6': '.Forms.Direction', '10': 'direction'},
    const {'1': 'alignment', '3': 2, '4': 1, '5': 14, '6': '.Forms.Alignment', '10': 'alignment'},
    const {'1': 'spacing', '3': 3, '4': 1, '5': 1, '10': 'spacing'},
    const {'1': 'run_alignment', '3': 4, '4': 1, '5': 14, '6': '.Forms.Alignment', '10': 'runAlignment'},
    const {'1': 'run_spacing', '3': 5, '4': 1, '5': 1, '10': 'runSpacing'},
    const {'1': 'cross_axis_alignment', '3': 6, '4': 1, '5': 14, '6': '.Forms.Alignment', '10': 'crossAxisAlignment'},
    const {'1': 'vertical_direction', '3': 7, '4': 1, '5': 14, '6': '.Forms.VerticalDirection', '10': 'verticalDirection'},
  ],
};

/// Descriptor for `Wrap`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List wrapDescriptor = $convert.base64Decode('CgRXcmFwEi4KCWRpcmVjdGlvbhgBIAEoDjIQLkZvcm1zLkRpcmVjdGlvblIJZGlyZWN0aW9uEi4KCWFsaWdubWVudBgCIAEoDjIQLkZvcm1zLkFsaWdubWVudFIJYWxpZ25tZW50EhgKB3NwYWNpbmcYAyABKAFSB3NwYWNpbmcSNQoNcnVuX2FsaWdubWVudBgEIAEoDjIQLkZvcm1zLkFsaWdubWVudFIMcnVuQWxpZ25tZW50Eh8KC3J1bl9zcGFjaW5nGAUgASgBUgpydW5TcGFjaW5nEkIKFGNyb3NzX2F4aXNfYWxpZ25tZW50GAYgASgOMhAuRm9ybXMuQWxpZ25tZW50UhJjcm9zc0F4aXNBbGlnbm1lbnQSRwoSdmVydGljYWxfZGlyZWN0aW9uGAcgASgOMhguRm9ybXMuVmVydGljYWxEaXJlY3Rpb25SEXZlcnRpY2FsRGlyZWN0aW9u');
@$core.Deprecated('Use textDescriptor instead')
const Text$json = const {
  '1': 'Text',
  '2': const [
    const {'1': 'text', '3': 1, '4': 1, '5': 9, '9': 0, '10': 'text'},
    const {'1': 'texts', '3': 2, '4': 1, '5': 11, '6': '.Forms.TextChildren', '9': 0, '10': 'texts'},
    const {'1': 'text_style', '3': 3, '4': 1, '5': 5, '10': 'textStyle'},
  ],
  '8': const [
    const {'1': 'type'},
  ],
};

/// Descriptor for `Text`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List textDescriptor = $convert.base64Decode('CgRUZXh0EhQKBHRleHQYASABKAlIAFIEdGV4dBIrCgV0ZXh0cxgCIAEoCzITLkZvcm1zLlRleHRDaGlsZHJlbkgAUgV0ZXh0cxIdCgp0ZXh0X3N0eWxlGAMgASgFUgl0ZXh0U3R5bGVCBgoEdHlwZQ==');
@$core.Deprecated('Use textChildrenDescriptor instead')
const TextChildren$json = const {
  '1': 'TextChildren',
  '2': const [
    const {'1': 'children', '3': 1, '4': 3, '5': 11, '6': '.Forms.Text', '10': 'children'},
  ],
};

/// Descriptor for `TextChildren`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List textChildrenDescriptor = $convert.base64Decode('CgxUZXh0Q2hpbGRyZW4SJwoIY2hpbGRyZW4YASADKAsyCy5Gb3Jtcy5UZXh0UghjaGlsZHJlbg==');
@$core.Deprecated('Use themeDescriptor instead')
const Theme$json = const {
  '1': 'Theme',
  '2': const [
    const {'1': 'text_color', '3': 1, '4': 1, '5': 9, '10': 'textColor'},
    const {'1': 'primary_color', '3': 2, '4': 1, '5': 9, '10': 'primaryColor'},
    const {'1': 'accent_color', '3': 3, '4': 1, '5': 9, '10': 'accentColor'},
    const {'1': 'font_size_1', '3': 4, '4': 1, '5': 5, '10': 'fontSize1'},
    const {'1': 'font_size_2', '3': 5, '4': 1, '5': 5, '10': 'fontSize2'},
  ],
};

/// Descriptor for `Theme`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List themeDescriptor = $convert.base64Decode('CgVUaGVtZRIdCgp0ZXh0X2NvbG9yGAEgASgJUgl0ZXh0Q29sb3ISIwoNcHJpbWFyeV9jb2xvchgCIAEoCVIMcHJpbWFyeUNvbG9yEiEKDGFjY2VudF9jb2xvchgDIAEoCVILYWNjZW50Q29sb3ISHgoLZm9udF9zaXplXzEYBCABKAVSCWZvbnRTaXplMRIeCgtmb250X3NpemVfMhgFIAEoBVIJZm9udFNpemUy');
@$core.Deprecated('Use formFieldDescriptor instead')
const FormField$json = const {
  '1': 'FormField',
  '2': const [
    const {'1': 'field_id', '3': 1, '4': 1, '5': 9, '10': 'fieldId'},
    const {'1': 'label', '3': 2, '4': 1, '5': 9, '10': 'label'},
    const {'1': 'enabled', '3': 3, '4': 1, '5': 8, '10': 'enabled'},
    const {'1': 'style', '3': 4, '4': 1, '5': 11, '6': '.Forms.FieldStyle', '10': 'style'},
    const {'1': 'validators', '3': 5, '4': 3, '5': 11, '6': '.Forms.Validator', '10': 'validators'},
    const {'1': 'control', '3': 6, '4': 1, '5': 11, '6': '.Forms.FieldControl', '10': 'control'},
    const {'1': 'title', '3': 7, '4': 1, '5': 11, '6': '.Forms.Text', '10': 'title'},
    const {'1': 'should_request_focus', '3': 8, '4': 1, '5': 8, '10': 'shouldRequestFocus'},
  ],
};

/// Descriptor for `FormField`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List formFieldDescriptor = $convert.base64Decode('CglGb3JtRmllbGQSGQoIZmllbGRfaWQYASABKAlSB2ZpZWxkSWQSFAoFbGFiZWwYAiABKAlSBWxhYmVsEhgKB2VuYWJsZWQYAyABKAhSB2VuYWJsZWQSJwoFc3R5bGUYBCABKAsyES5Gb3Jtcy5GaWVsZFN0eWxlUgVzdHlsZRIwCgp2YWxpZGF0b3JzGAUgAygLMhAuRm9ybXMuVmFsaWRhdG9yUgp2YWxpZGF0b3JzEi0KB2NvbnRyb2wYBiABKAsyEy5Gb3Jtcy5GaWVsZENvbnRyb2xSB2NvbnRyb2wSIQoFdGl0bGUYByABKAsyCy5Gb3Jtcy5UZXh0UgV0aXRsZRIwChRzaG91bGRfcmVxdWVzdF9mb2N1cxgIIAEoCFISc2hvdWxkUmVxdWVzdEZvY3Vz');
@$core.Deprecated('Use fieldStyleDescriptor instead')
const FieldStyle$json = const {
  '1': 'FieldStyle',
  '2': const [
    const {'1': 'accented_label', '3': 1, '4': 1, '5': 8, '10': 'accentedLabel'},
    const {'1': 'font_size', '3': 2, '4': 1, '5': 5, '10': 'fontSize'},
  ],
};

/// Descriptor for `FieldStyle`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List fieldStyleDescriptor = $convert.base64Decode('CgpGaWVsZFN0eWxlEiUKDmFjY2VudGVkX2xhYmVsGAEgASgIUg1hY2NlbnRlZExhYmVsEhsKCWZvbnRfc2l6ZRgCIAEoBVIIZm9udFNpemU=');
@$core.Deprecated('Use containerDescriptor instead')
const Container$json = const {
  '1': 'Container',
  '2': const [
    const {'1': 'column', '3': 1, '4': 1, '5': 11, '6': '.Forms.FormColumn', '9': 0, '10': 'column'},
    const {'1': 'row', '3': 2, '4': 1, '5': 11, '6': '.Forms.FormRow', '9': 0, '10': 'row'},
    const {'1': 'form_field', '3': 3, '4': 1, '5': 11, '6': '.Forms.FormField', '9': 0, '10': 'formField'},
    const {'1': 'columns', '3': 4, '4': 1, '5': 5, '10': 'columns'},
  ],
  '8': const [
    const {'1': 'type'},
  ],
};

/// Descriptor for `Container`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List containerDescriptor = $convert.base64Decode('CglDb250YWluZXISKwoGY29sdW1uGAEgASgLMhEuRm9ybXMuRm9ybUNvbHVtbkgAUgZjb2x1bW4SIgoDcm93GAIgASgLMg4uRm9ybXMuRm9ybVJvd0gAUgNyb3cSMQoKZm9ybV9maWVsZBgDIAEoCzIQLkZvcm1zLkZvcm1GaWVsZEgAUglmb3JtRmllbGQSGAoHY29sdW1ucxgEIAEoBVIHY29sdW1uc0IGCgR0eXBl');
@$core.Deprecated('Use layoutDescriptor instead')
const Layout$json = const {
  '1': 'Layout',
  '2': const [
    const {'1': 'containers', '3': 1, '4': 3, '5': 11, '6': '.Forms.Container', '10': 'containers'},
    const {'1': 'gap', '3': 2, '4': 1, '5': 1, '10': 'gap'},
    const {'1': 'theme', '3': 3, '4': 1, '5': 11, '6': '.Forms.Theme', '10': 'theme'},
  ],
};

/// Descriptor for `Layout`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List layoutDescriptor = $convert.base64Decode('CgZMYXlvdXQSMAoKY29udGFpbmVycxgBIAMoCzIQLkZvcm1zLkNvbnRhaW5lclIKY29udGFpbmVycxIQCgNnYXAYAiABKAFSA2dhcBIiCgV0aGVtZRgDIAEoCzIMLkZvcm1zLlRoZW1lUgV0aGVtZQ==');
