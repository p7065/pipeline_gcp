﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Belfry.Relational
{
    public interface IRelationalStorage
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        EntityEntry<TEntity> Loader<TEntity>(TEntity entity) where TEntity : class;
        Task SaveChangesAsync();
        Task BeginTransactionAsync();
        void CommitTransaction();
        Task ClearDatabase();
        void ClearChanges();
    }
}
