﻿using Autofac.Core.Resolving.Pipeline;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Belfry.Infrastructure
{
    internal class AutoInjectionMiddleware : IResolveMiddleware
    {
        public PipelinePhase Phase => PipelinePhase.Activation;
        public IAutoInjector Injector { get; }

        public AutoInjectionMiddleware(IAutoInjector injector)
        {
            Injector = injector;
        }

        public void Execute(ResolveRequestContext context, Action<ResolveRequestContext> next)
        {
            next(context);

            if (context.NewInstanceActivated)
            {
                Injector.OnRegistrationActivated(context);
            }
        }
    }
}
