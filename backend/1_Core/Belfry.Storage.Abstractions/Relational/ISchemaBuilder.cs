﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Belfry.Relational
{
    public interface ISchemaBuilder
    {
        void Build(ModelBuilder builder);
    }
}
