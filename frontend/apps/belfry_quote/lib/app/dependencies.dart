import 'package:belfry_quote/page_content/pagecontent_controller.dart';
import 'package:belfry_quote/workflow/workflow_controller.dart';
import 'package:get/get.dart';

class AppDependencies extends Bindings {
  @override
  void dependencies() {
    Get.put(WorkflowController(), permanent: true);
    Get.lazyPut(() => PageContentController());
  }
}
