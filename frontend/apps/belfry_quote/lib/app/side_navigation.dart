import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SideNavigation extends StatelessWidget {
  const SideNavigation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
        child: Column(
          children: [
            InkWell(
              child: Text(
                'Irányítópult',
                style: Get.theme.textTheme.headline5,
              ),
              onTap: () => Get.toNamed('/'),
            ),
            SizedBox(height: 20),
            InkWell(
              child: Text(
                'Alvállalkozók',
                style: Get.theme.textTheme.headline5,
              ),
              onTap: () => Get.toNamed('/contractors'),
            ),
          ],
        ),
      ),
    );
  }
}
