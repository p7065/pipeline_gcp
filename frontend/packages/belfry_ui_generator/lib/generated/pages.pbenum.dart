///
//  Generated code. Do not modify.
//  source: pages.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class FieldState extends $pb.ProtobufEnum {
  static const FieldState field_state_initialized = FieldState._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'field_state_initialized');
  static const FieldState field_state_loaded = FieldState._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'field_state_loaded');
  static const FieldState field_state_dirty = FieldState._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'field_state_dirty');
  static const FieldState field_state_saving = FieldState._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'field_state_saving');
  static const FieldState field_state_saved = FieldState._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'field_state_saved');

  static const $core.List<FieldState> values = <FieldState> [
    field_state_initialized,
    field_state_loaded,
    field_state_dirty,
    field_state_saving,
    field_state_saved,
  ];

  static final $core.Map<$core.int, FieldState> _byValue = $pb.ProtobufEnum.initByValue(values);
  static FieldState? valueOf($core.int value) => _byValue[value];

  const FieldState._($core.int v, $core.String n) : super(v, n);
}

