///
//  Generated code. Do not modify.
//  source: pages.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'pages.pb.dart' as $1;
export 'pages.pb.dart';

class PageflowClient extends $grpc.Client {
  static final _$start =
      $grpc.ClientMethod<$1.PageflowRequest, $1.PageflowResponse>(
          '/Pages.Pageflow/Start',
          ($1.PageflowRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $1.PageflowResponse.fromBuffer(value));
  static final _$open = $grpc.ClientMethod<$1.OpenRequest, $1.PageResponse>(
      '/Pages.Pageflow/Open',
      ($1.OpenRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.PageResponse.fromBuffer(value));
  static final _$save =
      $grpc.ClientMethod<$1.DataRequest, $1.ConfirmationResult>(
          '/Pages.Pageflow/Save',
          ($1.DataRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $1.ConfirmationResult.fromBuffer(value));
  static final _$close =
      $grpc.ClientMethod<$1.CloseRequest, $1.ConfirmationResult>(
          '/Pages.Pageflow/Close',
          ($1.CloseRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $1.ConfirmationResult.fromBuffer(value));

  PageflowClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseStream<$1.PageflowResponse> start($1.PageflowRequest request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$start, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseStream<$1.PageResponse> open($1.OpenRequest request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$open, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseFuture<$1.ConfirmationResult> save($1.DataRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$save, request, options: options);
  }

  $grpc.ResponseFuture<$1.ConfirmationResult> close($1.CloseRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$close, request, options: options);
  }
}

abstract class PageflowServiceBase extends $grpc.Service {
  $core.String get $name => 'Pages.Pageflow';

  PageflowServiceBase() {
    $addMethod($grpc.ServiceMethod<$1.PageflowRequest, $1.PageflowResponse>(
        'Start',
        start_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $1.PageflowRequest.fromBuffer(value),
        ($1.PageflowResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.OpenRequest, $1.PageResponse>(
        'Open',
        open_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $1.OpenRequest.fromBuffer(value),
        ($1.PageResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.DataRequest, $1.ConfirmationResult>(
        'Save',
        save_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.DataRequest.fromBuffer(value),
        ($1.ConfirmationResult value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.CloseRequest, $1.ConfirmationResult>(
        'Close',
        close_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.CloseRequest.fromBuffer(value),
        ($1.ConfirmationResult value) => value.writeToBuffer()));
  }

  $async.Stream<$1.PageflowResponse> start_Pre($grpc.ServiceCall call,
      $async.Future<$1.PageflowRequest> request) async* {
    yield* start(call, await request);
  }

  $async.Stream<$1.PageResponse> open_Pre(
      $grpc.ServiceCall call, $async.Future<$1.OpenRequest> request) async* {
    yield* open(call, await request);
  }

  $async.Future<$1.ConfirmationResult> save_Pre(
      $grpc.ServiceCall call, $async.Future<$1.DataRequest> request) async {
    return save(call, await request);
  }

  $async.Future<$1.ConfirmationResult> close_Pre(
      $grpc.ServiceCall call, $async.Future<$1.CloseRequest> request) async {
    return close(call, await request);
  }

  $async.Stream<$1.PageflowResponse> start(
      $grpc.ServiceCall call, $1.PageflowRequest request);
  $async.Stream<$1.PageResponse> open(
      $grpc.ServiceCall call, $1.OpenRequest request);
  $async.Future<$1.ConfirmationResult> save(
      $grpc.ServiceCall call, $1.DataRequest request);
  $async.Future<$1.ConfirmationResult> close(
      $grpc.ServiceCall call, $1.CloseRequest request);
}
