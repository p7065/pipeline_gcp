import 'package:belfry_quote/app/side_navigation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../app/app_theme.dart';
import 'workflow_controller.dart';

class Dashboard extends GetView<WorkflowController> {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Row(
      children: [
        const SideNavigation(),
        Expanded(
          child: Container(
            color: AppTheme.background,
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'Iránytópult',
                  style: AppTheme.h1,
                ),
                const SizedBox(
                  height: 40,
                ),
                Expanded(
                  child: Center(
                    child: FutureBuilder(
                        future: controller.getWorkflowListResponse(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return _buildWorkflows();
                          } else {
                            return const CircularProgressIndicator();
                          }
                        }),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ));
  }

  _buildWorkflows() {
    var workflowButtons = List<ElevatedButton>.empty(growable: true);
    for (var workflow in controller.workflows) {
      var button = ElevatedButton(
        child: Text('Start ${workflow.type.name}'),
        onPressed: () async => await controller
            .initWorkflow(workflow)
            .then((value) => value != null
                // ? Get.to(WorkflowPage(correlationId: value.correlationId))
                ? Get.toNamed('/workflow', arguments: value.correlationId)
                : null),
      );
      workflowButtons.add(button);
    }
    return Wrap(
      children: workflowButtons,
    );
  }
}
