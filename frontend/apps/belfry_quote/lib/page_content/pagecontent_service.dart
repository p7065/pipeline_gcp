import 'package:belfry_ui_generator/generated/pages.pbgrpc.dart';
import 'package:belfry_quote/app/services.dart';
import 'package:grpc/src/client/channel.dart';

class ContentService extends WorkflowService<PageflowClient>
    with WebGrpcService {
  static Future<ContentService> create() => Future.value(ContentService());

  @override
  createClient(ClientChannel channel) {
    return PageflowClient(channel);
  }

// starts a page flow session that is capable of changing pages according to a workflow template
  Stream<PageflowResponse> start(PageflowRequest request) {
    return client.start(request);
  }

// opens a page in the flow which is a page session that is capable of asynchronous interaction with the user
  Stream<PageResponse> open(OpenRequest request) {
    return client.open(request);
  }

// saves a value (or set of values?) that the user specifies during page session
  Future save(DataRequest request) async {
    await client.save(request);
  }

// closes the page
  Future close(CloseRequest request) async {
    await client.close(request);
  }
}
