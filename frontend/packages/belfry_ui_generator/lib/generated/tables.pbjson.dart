///
//  Generated code. Do not modify.
//  source: tables.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use columnSizeDescriptor instead')
const ColumnSize$json = const {
  '1': 'ColumnSize',
  '2': const [
    const {'1': 's', '2': 0},
    const {'1': 'm', '2': 1},
    const {'1': 'l', '2': 2},
  ],
};

/// Descriptor for `ColumnSize`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List columnSizeDescriptor = $convert.base64Decode('CgpDb2x1bW5TaXplEgUKAXMQABIFCgFtEAESBQoBbBAC');
@$core.Deprecated('Use tableTypeDescriptor instead')
const TableType$json = const {
  '1': 'TableType',
  '2': const [
    const {'1': 'simple', '2': 0},
    const {'1': 'simple_async', '2': 1},
    const {'1': 'paginated', '2': 3},
    const {'1': 'paginated_async', '2': 4},
  ],
};

/// Descriptor for `TableType`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List tableTypeDescriptor = $convert.base64Decode('CglUYWJsZVR5cGUSCgoGc2ltcGxlEAASEAoMc2ltcGxlX2FzeW5jEAESDQoJcGFnaW5hdGVkEAMSEwoPcGFnaW5hdGVkX2FzeW5jEAQ=');
@$core.Deprecated('Use headerItemDescriptor instead')
const HeaderItem$json = const {
  '1': 'HeaderItem',
  '2': const [
    const {'1': 'field_id', '3': 1, '4': 1, '5': 9, '10': 'fieldId'},
    const {'1': 'label', '3': 2, '4': 1, '5': 9, '10': 'label'},
    const {'1': 'tooltip', '3': 3, '4': 1, '5': 9, '10': 'tooltip'},
    const {'1': 'numeric', '3': 4, '4': 1, '5': 8, '10': 'numeric'},
    const {'1': 'size', '3': 5, '4': 1, '5': 14, '6': '.Tables.ColumnSize', '10': 'size'},
    const {'1': 'text_style', '3': 6, '4': 1, '5': 5, '10': 'textStyle'},
  ],
};

/// Descriptor for `HeaderItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List headerItemDescriptor = $convert.base64Decode('CgpIZWFkZXJJdGVtEhkKCGZpZWxkX2lkGAEgASgJUgdmaWVsZElkEhQKBWxhYmVsGAIgASgJUgVsYWJlbBIYCgd0b29sdGlwGAMgASgJUgd0b29sdGlwEhgKB251bWVyaWMYBCABKAhSB251bWVyaWMSJgoEc2l6ZRgFIAEoDjISLlRhYmxlcy5Db2x1bW5TaXplUgRzaXplEh0KCnRleHRfc3R5bGUYBiABKAVSCXRleHRTdHlsZQ==');
@$core.Deprecated('Use headerDescriptor instead')
const Header$json = const {
  '1': 'Header',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.Tables.HeaderItem', '10': 'items'},
    const {'1': 'sort_col_index', '3': 2, '4': 1, '5': 5, '10': 'sortColIndex'},
    const {'1': 'color', '3': 3, '4': 1, '5': 9, '10': 'color'},
    const {'1': 'height', '3': 4, '4': 1, '5': 1, '10': 'height'},
    const {'1': 'text_style', '3': 5, '4': 1, '5': 5, '10': 'textStyle'},
    const {'1': 'show_checkbox_column', '3': 6, '4': 1, '5': 8, '10': 'showCheckboxColumn'},
    const {'1': 'column_spacing', '3': 8, '4': 1, '5': 1, '10': 'columnSpacing'},
  ],
};

/// Descriptor for `Header`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List headerDescriptor = $convert.base64Decode('CgZIZWFkZXISKAoFaXRlbXMYASADKAsyEi5UYWJsZXMuSGVhZGVySXRlbVIFaXRlbXMSJAoOc29ydF9jb2xfaW5kZXgYAiABKAVSDHNvcnRDb2xJbmRleBIUCgVjb2xvchgDIAEoCVIFY29sb3ISFgoGaGVpZ2h0GAQgASgBUgZoZWlnaHQSHQoKdGV4dF9zdHlsZRgFIAEoBVIJdGV4dFN0eWxlEjAKFHNob3dfY2hlY2tib3hfY29sdW1uGAYgASgIUhJzaG93Q2hlY2tib3hDb2x1bW4SJQoOY29sdW1uX3NwYWNpbmcYCCABKAFSDWNvbHVtblNwYWNpbmc=');
@$core.Deprecated('Use rowDescriptor instead')
const Row$json = const {
  '1': 'Row',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'cells', '3': 2, '4': 1, '5': 11, '6': '.Fields.Item', '10': 'cells'},
    const {'1': 'enable_actions', '3': 3, '4': 1, '5': 8, '10': 'enableActions'},
    const {'1': 'selected', '3': 4, '4': 1, '5': 8, '10': 'selected'},
    const {'1': 'color', '3': 5, '4': 1, '5': 9, '10': 'color'},
    const {'1': 'height', '3': 6, '4': 1, '5': 1, '10': 'height'},
  ],
};

/// Descriptor for `Row`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List rowDescriptor = $convert.base64Decode('CgNSb3cSEAoDa2V5GAEgASgJUgNrZXkSIgoFY2VsbHMYAiABKAsyDC5GaWVsZHMuSXRlbVIFY2VsbHMSJQoOZW5hYmxlX2FjdGlvbnMYAyABKAhSDWVuYWJsZUFjdGlvbnMSGgoIc2VsZWN0ZWQYBCABKAhSCHNlbGVjdGVkEhQKBWNvbG9yGAUgASgJUgVjb2xvchIWCgZoZWlnaHQYBiABKAFSBmhlaWdodA==');
@$core.Deprecated('Use tableDescriptor instead')
const Table$json = const {
  '1': 'Table',
  '2': const [
    const {'1': 'table_id', '3': 1, '4': 1, '5': 9, '10': 'tableId'},
    const {'1': 'schema_id', '3': 2, '4': 1, '5': 9, '10': 'schemaId'},
    const {'1': 'title', '3': 3, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'header', '3': 4, '4': 1, '5': 11, '6': '.Tables.Header', '10': 'header'},
    const {'1': 'rows', '3': 5, '4': 3, '5': 11, '6': '.Tables.Row', '10': 'rows'},
    const {'1': 'type', '3': 6, '4': 1, '5': 14, '6': '.Tables.TableType', '10': 'type'},
  ],
};

/// Descriptor for `Table`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List tableDescriptor = $convert.base64Decode('CgVUYWJsZRIZCgh0YWJsZV9pZBgBIAEoCVIHdGFibGVJZBIbCglzY2hlbWFfaWQYAiABKAlSCHNjaGVtYUlkEhQKBXRpdGxlGAMgASgJUgV0aXRsZRImCgZoZWFkZXIYBCABKAsyDi5UYWJsZXMuSGVhZGVyUgZoZWFkZXISHwoEcm93cxgFIAMoCzILLlRhYmxlcy5Sb3dSBHJvd3MSJQoEdHlwZRgGIAEoDjIRLlRhYmxlcy5UYWJsZVR5cGVSBHR5cGU=');
