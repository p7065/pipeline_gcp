///
//  Generated code. Do not modify.
//  source: tables.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'fields.pb.dart' as $0;

import 'tables.pbenum.dart';

export 'tables.pbenum.dart';

class HeaderItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'HeaderItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Tables'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'fieldId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'label')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tooltip')
    ..aOB(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'numeric')
    ..e<ColumnSize>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'size', $pb.PbFieldType.OE, defaultOrMaker: ColumnSize.s, valueOf: ColumnSize.valueOf, enumValues: ColumnSize.values)
    ..a<$core.int>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'textStyle', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  HeaderItem._() : super();
  factory HeaderItem({
    $core.String? fieldId,
    $core.String? label,
    $core.String? tooltip,
    $core.bool? numeric,
    ColumnSize? size,
    $core.int? textStyle,
  }) {
    final _result = create();
    if (fieldId != null) {
      _result.fieldId = fieldId;
    }
    if (label != null) {
      _result.label = label;
    }
    if (tooltip != null) {
      _result.tooltip = tooltip;
    }
    if (numeric != null) {
      _result.numeric = numeric;
    }
    if (size != null) {
      _result.size = size;
    }
    if (textStyle != null) {
      _result.textStyle = textStyle;
    }
    return _result;
  }
  factory HeaderItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory HeaderItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  HeaderItem clone() => HeaderItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  HeaderItem copyWith(void Function(HeaderItem) updates) => super.copyWith((message) => updates(message as HeaderItem)) as HeaderItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static HeaderItem create() => HeaderItem._();
  HeaderItem createEmptyInstance() => create();
  static $pb.PbList<HeaderItem> createRepeated() => $pb.PbList<HeaderItem>();
  @$core.pragma('dart2js:noInline')
  static HeaderItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<HeaderItem>(create);
  static HeaderItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get fieldId => $_getSZ(0);
  @$pb.TagNumber(1)
  set fieldId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFieldId() => $_has(0);
  @$pb.TagNumber(1)
  void clearFieldId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get label => $_getSZ(1);
  @$pb.TagNumber(2)
  set label($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLabel() => $_has(1);
  @$pb.TagNumber(2)
  void clearLabel() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get tooltip => $_getSZ(2);
  @$pb.TagNumber(3)
  set tooltip($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasTooltip() => $_has(2);
  @$pb.TagNumber(3)
  void clearTooltip() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get numeric => $_getBF(3);
  @$pb.TagNumber(4)
  set numeric($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasNumeric() => $_has(3);
  @$pb.TagNumber(4)
  void clearNumeric() => clearField(4);

  @$pb.TagNumber(5)
  ColumnSize get size => $_getN(4);
  @$pb.TagNumber(5)
  set size(ColumnSize v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasSize() => $_has(4);
  @$pb.TagNumber(5)
  void clearSize() => clearField(5);

  @$pb.TagNumber(6)
  $core.int get textStyle => $_getIZ(5);
  @$pb.TagNumber(6)
  set textStyle($core.int v) { $_setSignedInt32(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasTextStyle() => $_has(5);
  @$pb.TagNumber(6)
  void clearTextStyle() => clearField(6);
}

class Header extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Header', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Tables'), createEmptyInstance: create)
    ..pc<HeaderItem>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: HeaderItem.create)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'sortColIndex', $pb.PbFieldType.O3)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'color')
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'height', $pb.PbFieldType.OD)
    ..a<$core.int>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'textStyle', $pb.PbFieldType.O3)
    ..aOB(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'showCheckboxColumn')
    ..a<$core.double>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'columnSpacing', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  Header._() : super();
  factory Header({
    $core.Iterable<HeaderItem>? items,
    $core.int? sortColIndex,
    $core.String? color,
    $core.double? height,
    $core.int? textStyle,
    $core.bool? showCheckboxColumn,
    $core.double? columnSpacing,
  }) {
    final _result = create();
    if (items != null) {
      _result.items.addAll(items);
    }
    if (sortColIndex != null) {
      _result.sortColIndex = sortColIndex;
    }
    if (color != null) {
      _result.color = color;
    }
    if (height != null) {
      _result.height = height;
    }
    if (textStyle != null) {
      _result.textStyle = textStyle;
    }
    if (showCheckboxColumn != null) {
      _result.showCheckboxColumn = showCheckboxColumn;
    }
    if (columnSpacing != null) {
      _result.columnSpacing = columnSpacing;
    }
    return _result;
  }
  factory Header.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Header.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Header clone() => Header()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Header copyWith(void Function(Header) updates) => super.copyWith((message) => updates(message as Header)) as Header; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Header create() => Header._();
  Header createEmptyInstance() => create();
  static $pb.PbList<Header> createRepeated() => $pb.PbList<Header>();
  @$core.pragma('dart2js:noInline')
  static Header getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Header>(create);
  static Header? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<HeaderItem> get items => $_getList(0);

  @$pb.TagNumber(2)
  $core.int get sortColIndex => $_getIZ(1);
  @$pb.TagNumber(2)
  set sortColIndex($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSortColIndex() => $_has(1);
  @$pb.TagNumber(2)
  void clearSortColIndex() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get color => $_getSZ(2);
  @$pb.TagNumber(3)
  set color($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasColor() => $_has(2);
  @$pb.TagNumber(3)
  void clearColor() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get height => $_getN(3);
  @$pb.TagNumber(4)
  set height($core.double v) { $_setDouble(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasHeight() => $_has(3);
  @$pb.TagNumber(4)
  void clearHeight() => clearField(4);

  @$pb.TagNumber(5)
  $core.int get textStyle => $_getIZ(4);
  @$pb.TagNumber(5)
  set textStyle($core.int v) { $_setSignedInt32(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasTextStyle() => $_has(4);
  @$pb.TagNumber(5)
  void clearTextStyle() => clearField(5);

  @$pb.TagNumber(6)
  $core.bool get showCheckboxColumn => $_getBF(5);
  @$pb.TagNumber(6)
  set showCheckboxColumn($core.bool v) { $_setBool(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasShowCheckboxColumn() => $_has(5);
  @$pb.TagNumber(6)
  void clearShowCheckboxColumn() => clearField(6);

  @$pb.TagNumber(8)
  $core.double get columnSpacing => $_getN(6);
  @$pb.TagNumber(8)
  set columnSpacing($core.double v) { $_setDouble(6, v); }
  @$pb.TagNumber(8)
  $core.bool hasColumnSpacing() => $_has(6);
  @$pb.TagNumber(8)
  void clearColumnSpacing() => clearField(8);
}

class Row extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Row', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Tables'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'key')
    ..aOM<$0.Item>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'cells', subBuilder: $0.Item.create)
    ..aOB(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'enableActions')
    ..aOB(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'selected')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'color')
    ..a<$core.double>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'height', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  Row._() : super();
  factory Row({
    $core.String? key,
    $0.Item? cells,
    $core.bool? enableActions,
    $core.bool? selected,
    $core.String? color,
    $core.double? height,
  }) {
    final _result = create();
    if (key != null) {
      _result.key = key;
    }
    if (cells != null) {
      _result.cells = cells;
    }
    if (enableActions != null) {
      _result.enableActions = enableActions;
    }
    if (selected != null) {
      _result.selected = selected;
    }
    if (color != null) {
      _result.color = color;
    }
    if (height != null) {
      _result.height = height;
    }
    return _result;
  }
  factory Row.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Row.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Row clone() => Row()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Row copyWith(void Function(Row) updates) => super.copyWith((message) => updates(message as Row)) as Row; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Row create() => Row._();
  Row createEmptyInstance() => create();
  static $pb.PbList<Row> createRepeated() => $pb.PbList<Row>();
  @$core.pragma('dart2js:noInline')
  static Row getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Row>(create);
  static Row? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get key => $_getSZ(0);
  @$pb.TagNumber(1)
  set key($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasKey() => $_has(0);
  @$pb.TagNumber(1)
  void clearKey() => clearField(1);

  @$pb.TagNumber(2)
  $0.Item get cells => $_getN(1);
  @$pb.TagNumber(2)
  set cells($0.Item v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasCells() => $_has(1);
  @$pb.TagNumber(2)
  void clearCells() => clearField(2);
  @$pb.TagNumber(2)
  $0.Item ensureCells() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.bool get enableActions => $_getBF(2);
  @$pb.TagNumber(3)
  set enableActions($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasEnableActions() => $_has(2);
  @$pb.TagNumber(3)
  void clearEnableActions() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get selected => $_getBF(3);
  @$pb.TagNumber(4)
  set selected($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasSelected() => $_has(3);
  @$pb.TagNumber(4)
  void clearSelected() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get color => $_getSZ(4);
  @$pb.TagNumber(5)
  set color($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasColor() => $_has(4);
  @$pb.TagNumber(5)
  void clearColor() => clearField(5);

  @$pb.TagNumber(6)
  $core.double get height => $_getN(5);
  @$pb.TagNumber(6)
  set height($core.double v) { $_setDouble(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasHeight() => $_has(5);
  @$pb.TagNumber(6)
  void clearHeight() => clearField(6);
}

class Table extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Table', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Tables'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tableId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'schemaId')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'title')
    ..aOM<Header>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'header', subBuilder: Header.create)
    ..pc<Row>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rows', $pb.PbFieldType.PM, subBuilder: Row.create)
    ..e<TableType>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type', $pb.PbFieldType.OE, defaultOrMaker: TableType.simple, valueOf: TableType.valueOf, enumValues: TableType.values)
    ..hasRequiredFields = false
  ;

  Table._() : super();
  factory Table({
    $core.String? tableId,
    $core.String? schemaId,
    $core.String? title,
    Header? header,
    $core.Iterable<Row>? rows,
    TableType? type,
  }) {
    final _result = create();
    if (tableId != null) {
      _result.tableId = tableId;
    }
    if (schemaId != null) {
      _result.schemaId = schemaId;
    }
    if (title != null) {
      _result.title = title;
    }
    if (header != null) {
      _result.header = header;
    }
    if (rows != null) {
      _result.rows.addAll(rows);
    }
    if (type != null) {
      _result.type = type;
    }
    return _result;
  }
  factory Table.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Table.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Table clone() => Table()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Table copyWith(void Function(Table) updates) => super.copyWith((message) => updates(message as Table)) as Table; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Table create() => Table._();
  Table createEmptyInstance() => create();
  static $pb.PbList<Table> createRepeated() => $pb.PbList<Table>();
  @$core.pragma('dart2js:noInline')
  static Table getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Table>(create);
  static Table? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get tableId => $_getSZ(0);
  @$pb.TagNumber(1)
  set tableId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTableId() => $_has(0);
  @$pb.TagNumber(1)
  void clearTableId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get schemaId => $_getSZ(1);
  @$pb.TagNumber(2)
  set schemaId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSchemaId() => $_has(1);
  @$pb.TagNumber(2)
  void clearSchemaId() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get title => $_getSZ(2);
  @$pb.TagNumber(3)
  set title($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasTitle() => $_has(2);
  @$pb.TagNumber(3)
  void clearTitle() => clearField(3);

  @$pb.TagNumber(4)
  Header get header => $_getN(3);
  @$pb.TagNumber(4)
  set header(Header v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasHeader() => $_has(3);
  @$pb.TagNumber(4)
  void clearHeader() => clearField(4);
  @$pb.TagNumber(4)
  Header ensureHeader() => $_ensure(3);

  @$pb.TagNumber(5)
  $core.List<Row> get rows => $_getList(4);

  @$pb.TagNumber(6)
  TableType get type => $_getN(5);
  @$pb.TagNumber(6)
  set type(TableType v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasType() => $_has(5);
  @$pb.TagNumber(6)
  void clearType() => clearField(6);
}

