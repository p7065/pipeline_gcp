import 'package:flutter/material.dart';

class AppTheme {
  static const Color background = Color(0xFFF3F4FA);
  static const Color darkblue = Color(0xFF1D366C);
  static const Color gray = Color(0xFF646883);
  static const Color lightgray = Color(0xFFD3D5E3);
  static const Color yellow = Color(0xFFFFCC02);
  static const Color orange = Color(0xFFFF9500);
  static const Color lightblue = Color(0xFF5AC8FA);
  static const Color blue = Color(0xFF465EFB);

  static const String poppins = 'Poppins';
  static const String montserrat = 'Montserrat';
  static const FontWeight normal = FontWeight.w400;
  static const FontWeight medium = FontWeight.w500;
  static const FontWeight semiBold = FontWeight.w600;

  static const TextTheme textTheme = TextTheme(
    headline1: h1,
    headline2: h2,
    headline3: h3,
    headline4: h4,
    headline5: h5,
    headline6: h6,
    subtitle1: subtitle,
    bodyText1: body1,
    bodyText2: body2,
    button: button,
    // caption: caption,
  );

  static const Map<int, TextStyle> getTextStyleByNumber = {
    1: h1,
    2: h2,
    3: h3,
    4: h4,
    5: h5,
    6: h6,
    7: subtitle,
    8: body1,
    9: body2,
    10: button,
  };

  static const TextStyle h1 = TextStyle(
    fontFamily: poppins,
    fontSize: 22.0,
    color: darkblue,
    fontWeight: semiBold,
    letterSpacing: 0.5,
  );
  static const TextStyle h2 = TextStyle(
    fontFamily: poppins,
    fontSize: 22.0,
    color: blue,
    fontWeight: semiBold,
    letterSpacing: 0.5,
  );
  static const TextStyle h3 = TextStyle(
    fontFamily: poppins,
    fontSize: 22.0,
    color: lightblue,
    fontWeight: semiBold,
    letterSpacing: 0.5,
  );
  static const TextStyle h4 = TextStyle(
    fontFamily: poppins,
    fontSize: 20.0,
    color: gray,
    fontWeight: semiBold,
    letterSpacing: 1,
  );
  static const TextStyle h5 = TextStyle(
    fontFamily: poppins,
    fontSize: 15.0,
    color: gray,
    fontWeight: semiBold,
    letterSpacing: 1,
  );
  static const TextStyle h6 = TextStyle(
    fontFamily: poppins,
    fontSize: 8.0,
    color: gray,
    fontWeight: semiBold,
  );
  static const TextStyle subtitle = TextStyle(
    fontFamily: montserrat,
    fontSize: 15.0,
    color: gray,
  );
  static const TextStyle body1 = TextStyle(
    fontFamily: montserrat,
    fontSize: 13.0,
    color: gray,
    fontWeight: semiBold,
  );
  static const TextStyle body2 = TextStyle(
    fontFamily: montserrat,
    fontSize: 13.0,
    color: gray,
    fontWeight: normal,
    height: 1.4,
  );
  static const TextStyle button = TextStyle(
    fontFamily: poppins,
    fontSize: 13.0,
    color: Colors.white,
    letterSpacing: 1.2,
    fontWeight: medium,
  );

  static BoxDecoration roundedShadowBoxDecoration = BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(
        Radius.circular(5.0),
      ),
      boxShadow: [
        BoxShadow(
            color: Colors.black.withOpacity(0.1),
            spreadRadius: 0,
            blurRadius: 10,
            offset: Offset(0, 3)),
      ]);

  static ThemeData light() {
    return ThemeData.light().copyWith(
      textTheme: textTheme,
      primaryColor: blue,
      colorScheme: ColorScheme.light().copyWith(
        primary: blue,
        secondary: yellow,
        background: background,
        surface: gray,
        error: Colors.red,
      ),
    );
  }

  static ThemeData dark() {
    return ThemeData.dark().copyWith(
      textTheme:
          textTheme.copyWith(bodyText1: body1.copyWith(color: background)),
      primaryColor: blue,
      colorScheme: ColorScheme.dark().copyWith(
        primary: blue,
        secondary: yellow,
        background: background,
        surface: gray,
        error: Colors.red,
      ),
    );
  }
}
