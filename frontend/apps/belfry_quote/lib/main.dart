import 'package:belfry_quote/app/app_theme.dart';
import 'package:belfry_quote/contractors/contractors_page.dart';
import 'package:belfry_quote/page_content/pagecontent_service.dart';
import 'package:belfry_quote/workflow/dashboard.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'app/dependencies.dart';
import 'workflow/workflow_page.dart';

Future<void> main() async {
  await initServices();
  runApp(const BelfryApp());
}

Future initServices() async {
  await Get.putAsync(() => ContentService.create());
}

class BelfryApp extends StatelessWidget {
  const BelfryApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      locale: const Locale('hu'),
      title: 'Belfry Quote Handler',
      theme: AppTheme.light(),
      initialBinding: AppDependencies(),
      getPages: [
        GetPage(name: '/', page: () => const Dashboard()),
        GetPage(name: '/workflow', page: () => WorkflowPage()),
        GetPage(name: '/contractors', page: () => ContractorsPage()),
        // Workflow Page
      ],
      // home: const Dashboard(),
    );
  }
}
