import 'package:belfry_quote/app/app_theme.dart';
import 'package:belfry_quote/app/side_navigation.dart';
import 'package:belfry_quote/page_content/pagecontent_controller.dart';
import 'package:belfry_quote/page_content/table.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../page_content/form.dart';

class WorkflowPage extends GetView<PageContentController> {
  WorkflowPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Row(
      children: [
        const SideNavigation(),
        Expanded(
          child: Container(
            color: AppTheme.background,
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  Get.arguments.toString(),
                  style: AppTheme.h1,
                ),
                const SizedBox(
                  height: 40,
                ),
                Expanded(
                  child: Center(
                    child: Obx(() => _getPageContent(controller.command)),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ));
  }

  _getPageContent(PageCommand command) {
    switch (command) {
      case PageCommand.wait:
        return const CircularProgressIndicator();
      case PageCommand.form:
        return FormContent(
          controller.formLayout,
          controller.fieldsSchema,
          controller.fieldsItem,
          onSaved: () => controller.closeForm(),
        );
      case PageCommand.table:
        return TableContent(controller.table);
    }
  }
}
