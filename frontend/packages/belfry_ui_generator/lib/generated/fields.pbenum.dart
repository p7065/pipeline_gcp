///
//  Generated code. Do not modify.
//  source: fields.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class UnitType extends $pb.ProtobufEnum {
  static const UnitType nonunit = UnitType._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'nonunit');
  static const UnitType count = UnitType._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'count');
  static const UnitType quantity = UnitType._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'quantity');
  static const UnitType distance = UnitType._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'distance');
  static const UnitType weight = UnitType._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'weight');

  static const $core.List<UnitType> values = <UnitType> [
    nonunit,
    count,
    quantity,
    distance,
    weight,
  ];

  static final $core.Map<$core.int, UnitType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static UnitType? valueOf($core.int value) => _byValue[value];

  const UnitType._($core.int v, $core.String n) : super(v, n);
}

class EntityType extends $pb.ProtobufEnum {
  static const EntityType poco = EntityType._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'poco');

  static const $core.List<EntityType> values = <EntityType> [
    poco,
  ];

  static final $core.Map<$core.int, EntityType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static EntityType? valueOf($core.int value) => _byValue[value];

  const EntityType._($core.int v, $core.String n) : super(v, n);
}

