# belfry_frontend

# generate grpc 

### from: belfry folder for: packages\belfry_ui_generator

protoc --dart_out=grpc:belfry_frontend/packages/belfry_ui_generator/lib/generated -I"belfry_backend\Protos" forms.proto; 
protoc --dart_out=grpc:belfry_frontend/packages/belfry_ui_generator/lib/generated -I"belfry_backend\Protos" fields.proto; 
protoc --dart_out=grpc:belfry_frontend/packages/belfry_ui_generator/lib/generated -I"belfry_backend\Protos" pages.proto;
protoc --dart_out=grpc:belfry_frontend/packages/belfry_ui_generator/lib/generated -I"belfry_backend\Protos" tables.proto;
protoc --dart_out=grpc:belfry_frontend/packages/belfry_ui_generator/lib/generated -I"belfry_backend\Protos" workflow.proto;

# build

docker build -f ./Dockerfile -t gcr.io/camino-main/belfry-dashboard:latest .

docker tag gcr.io/camino-main/belfry-dashboard:latest gcr.io/camino-main/belfry-dashboard:0.0.3

docker push gcr.io/camino-main/belfry-dashboard:latest 

docker push gcr.io/camino-main/belfry-dashboard:0.0.3
