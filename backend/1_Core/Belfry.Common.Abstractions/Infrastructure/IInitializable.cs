﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Belfry.Infrastructure
{
    public interface IInitializable
    {
        void Initialize();
    }
}
