///
//  Generated code. Do not modify.
//  source: pages.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use fieldStateDescriptor instead')
const FieldState$json = const {
  '1': 'FieldState',
  '2': const [
    const {'1': 'field_state_initialized', '2': 0},
    const {'1': 'field_state_loaded', '2': 1},
    const {'1': 'field_state_dirty', '2': 2},
    const {'1': 'field_state_saving', '2': 3},
    const {'1': 'field_state_saved', '2': 4},
  ],
};

/// Descriptor for `FieldState`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List fieldStateDescriptor = $convert.base64Decode('CgpGaWVsZFN0YXRlEhsKF2ZpZWxkX3N0YXRlX2luaXRpYWxpemVkEAASFgoSZmllbGRfc3RhdGVfbG9hZGVkEAESFQoRZmllbGRfc3RhdGVfZGlydHkQAhIWChJmaWVsZF9zdGF0ZV9zYXZpbmcQAxIVChFmaWVsZF9zdGF0ZV9zYXZlZBAE');
@$core.Deprecated('Use pageflowConfigurationDescriptor instead')
const PageflowConfiguration$json = const {
  '1': 'PageflowConfiguration',
};

/// Descriptor for `PageflowConfiguration`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pageflowConfigurationDescriptor = $convert.base64Decode('ChVQYWdlZmxvd0NvbmZpZ3VyYXRpb24=');
@$core.Deprecated('Use pageflowRequestDescriptor instead')
const PageflowRequest$json = const {
  '1': 'PageflowRequest',
  '2': const [
    const {'1': 'workflow', '3': 1, '4': 1, '5': 11, '6': '.Workflow.WorkflowDescriptor', '10': 'workflow'},
    const {'1': 'configuration', '3': 2, '4': 1, '5': 11, '6': '.Pages.PageflowConfiguration', '10': 'configuration'},
  ],
};

/// Descriptor for `PageflowRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pageflowRequestDescriptor = $convert.base64Decode('Cg9QYWdlZmxvd1JlcXVlc3QSOAoId29ya2Zsb3cYASABKAsyHC5Xb3JrZmxvdy5Xb3JrZmxvd0Rlc2NyaXB0b3JSCHdvcmtmbG93EkIKDWNvbmZpZ3VyYXRpb24YAiABKAsyHC5QYWdlcy5QYWdlZmxvd0NvbmZpZ3VyYXRpb25SDWNvbmZpZ3VyYXRpb24=');
@$core.Deprecated('Use pageflowResponseDescriptor instead')
const PageflowResponse$json = const {
  '1': 'PageflowResponse',
  '2': const [
    const {'1': 'flow_id', '3': 1, '4': 1, '5': 9, '10': 'flowId'},
    const {'1': 'command', '3': 2, '4': 1, '5': 11, '6': '.Pages.PageflowCommand', '10': 'command'},
  ],
};

/// Descriptor for `PageflowResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pageflowResponseDescriptor = $convert.base64Decode('ChBQYWdlZmxvd1Jlc3BvbnNlEhcKB2Zsb3dfaWQYASABKAlSBmZsb3dJZBIwCgdjb21tYW5kGAIgASgLMhYuUGFnZXMuUGFnZWZsb3dDb21tYW5kUgdjb21tYW5k');
@$core.Deprecated('Use pageflowCommandDescriptor instead')
const PageflowCommand$json = const {
  '1': 'PageflowCommand',
  '2': const [
    const {'1': 'page_id', '3': 1, '4': 1, '5': 9, '10': 'pageId'},
    const {'1': 'wait', '3': 2, '4': 1, '5': 11, '6': '.Pages.WaitCommand', '9': 0, '10': 'wait'},
    const {'1': 'form', '3': 3, '4': 1, '5': 11, '6': '.Pages.FormCommand', '9': 0, '10': 'form'},
    const {'1': 'table', '3': 4, '4': 1, '5': 11, '6': '.Pages.TableCommand', '9': 0, '10': 'table'},
  ],
  '8': const [
    const {'1': 'command'},
  ],
};

/// Descriptor for `PageflowCommand`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pageflowCommandDescriptor = $convert.base64Decode('Cg9QYWdlZmxvd0NvbW1hbmQSFwoHcGFnZV9pZBgBIAEoCVIGcGFnZUlkEigKBHdhaXQYAiABKAsyEi5QYWdlcy5XYWl0Q29tbWFuZEgAUgR3YWl0EigKBGZvcm0YAyABKAsyEi5QYWdlcy5Gb3JtQ29tbWFuZEgAUgRmb3JtEisKBXRhYmxlGAQgASgLMhMuUGFnZXMuVGFibGVDb21tYW5kSABSBXRhYmxlQgkKB2NvbW1hbmQ=');
@$core.Deprecated('Use waitCommandDescriptor instead')
const WaitCommand$json = const {
  '1': 'WaitCommand',
};

/// Descriptor for `WaitCommand`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List waitCommandDescriptor = $convert.base64Decode('CgtXYWl0Q29tbWFuZA==');
@$core.Deprecated('Use formCommandDescriptor instead')
const FormCommand$json = const {
  '1': 'FormCommand',
  '2': const [
    const {'1': 'schema', '3': 1, '4': 1, '5': 11, '6': '.Fields.Schema', '10': 'schema'},
    const {'1': 'item', '3': 2, '4': 1, '5': 11, '6': '.Fields.Item', '10': 'item'},
    const {'1': 'layout', '3': 3, '4': 1, '5': 11, '6': '.Forms.Layout', '10': 'layout'},
  ],
};

/// Descriptor for `FormCommand`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List formCommandDescriptor = $convert.base64Decode('CgtGb3JtQ29tbWFuZBImCgZzY2hlbWEYASABKAsyDi5GaWVsZHMuU2NoZW1hUgZzY2hlbWESIAoEaXRlbRgCIAEoCzIMLkZpZWxkcy5JdGVtUgRpdGVtEiUKBmxheW91dBgDIAEoCzINLkZvcm1zLkxheW91dFIGbGF5b3V0');
@$core.Deprecated('Use tableCommandDescriptor instead')
const TableCommand$json = const {
  '1': 'TableCommand',
  '2': const [
    const {'1': 'schema', '3': 1, '4': 1, '5': 11, '6': '.Fields.Schema', '10': 'schema'},
    const {'1': 'table', '3': 2, '4': 1, '5': 11, '6': '.Tables.Table', '10': 'table'},
  ],
};

/// Descriptor for `TableCommand`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List tableCommandDescriptor = $convert.base64Decode('CgxUYWJsZUNvbW1hbmQSJgoGc2NoZW1hGAEgASgLMg4uRmllbGRzLlNjaGVtYVIGc2NoZW1hEiMKBXRhYmxlGAIgASgLMg0uVGFibGVzLlRhYmxlUgV0YWJsZQ==');
@$core.Deprecated('Use asyncTaskDescriptionDescriptor instead')
const AsyncTaskDescription$json = const {
  '1': 'AsyncTaskDescription',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
  ],
};

/// Descriptor for `AsyncTaskDescription`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List asyncTaskDescriptionDescriptor = $convert.base64Decode('ChRBc3luY1Rhc2tEZXNjcmlwdGlvbhISCgRuYW1lGAEgASgJUgRuYW1l');
@$core.Deprecated('Use asyncTaskStatusDescriptor instead')
const AsyncTaskStatus$json = const {
  '1': 'AsyncTaskStatus',
  '2': const [
    const {'1': 'percent', '3': 1, '4': 1, '5': 1, '10': 'percent'},
  ],
};

/// Descriptor for `AsyncTaskStatus`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List asyncTaskStatusDescriptor = $convert.base64Decode('Cg9Bc3luY1Rhc2tTdGF0dXMSGAoHcGVyY2VudBgBIAEoAVIHcGVyY2VudA==');
@$core.Deprecated('Use asyncTaskResultDescriptor instead')
const AsyncTaskResult$json = const {
  '1': 'AsyncTaskResult',
  '2': const [
    const {'1': 'success', '3': 1, '4': 1, '5': 8, '10': 'success'},
    const {'1': 'error', '3': 2, '4': 1, '5': 9, '10': 'error'},
  ],
};

/// Descriptor for `AsyncTaskResult`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List asyncTaskResultDescriptor = $convert.base64Decode('Cg9Bc3luY1Rhc2tSZXN1bHQSGAoHc3VjY2VzcxgBIAEoCFIHc3VjY2VzcxIUCgVlcnJvchgCIAEoCVIFZXJyb3I=');
@$core.Deprecated('Use asyncProgressDescriptor instead')
const AsyncProgress$json = const {
  '1': 'AsyncProgress',
  '2': const [
    const {'1': 'task_id', '3': 1, '4': 1, '5': 9, '10': 'taskId'},
    const {'1': 'description', '3': 2, '4': 1, '5': 11, '6': '.Pages.AsyncTaskDescription', '9': 0, '10': 'description'},
    const {'1': 'status', '3': 3, '4': 1, '5': 11, '6': '.Pages.AsyncTaskStatus', '9': 0, '10': 'status'},
    const {'1': 'result', '3': 4, '4': 1, '5': 11, '6': '.Pages.AsyncTaskResult', '9': 0, '10': 'result'},
  ],
  '8': const [
    const {'1': 'phase'},
  ],
};

/// Descriptor for `AsyncProgress`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List asyncProgressDescriptor = $convert.base64Decode('Cg1Bc3luY1Byb2dyZXNzEhcKB3Rhc2tfaWQYASABKAlSBnRhc2tJZBI/CgtkZXNjcmlwdGlvbhgCIAEoCzIbLlBhZ2VzLkFzeW5jVGFza0Rlc2NyaXB0aW9uSABSC2Rlc2NyaXB0aW9uEjAKBnN0YXR1cxgDIAEoCzIWLlBhZ2VzLkFzeW5jVGFza1N0YXR1c0gAUgZzdGF0dXMSMAoGcmVzdWx0GAQgASgLMhYuUGFnZXMuQXN5bmNUYXNrUmVzdWx0SABSBnJlc3VsdEIHCgVwaGFzZQ==');
@$core.Deprecated('Use sessionKeepAliveDescriptor instead')
const SessionKeepAlive$json = const {
  '1': 'SessionKeepAlive',
  '2': const [
    const {'1': 'uptime_seconds', '3': 1, '4': 1, '5': 5, '10': 'uptimeSeconds'},
    const {'1': 'idle_seconds', '3': 2, '4': 1, '5': 5, '10': 'idleSeconds'},
  ],
};

/// Descriptor for `SessionKeepAlive`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sessionKeepAliveDescriptor = $convert.base64Decode('ChBTZXNzaW9uS2VlcEFsaXZlEiUKDnVwdGltZV9zZWNvbmRzGAEgASgFUg11cHRpbWVTZWNvbmRzEiEKDGlkbGVfc2Vjb25kcxgCIAEoBVILaWRsZVNlY29uZHM=');
@$core.Deprecated('Use sessionCloseDescriptor instead')
const SessionClose$json = const {
  '1': 'SessionClose',
  '2': const [
    const {'1': 'remaining_seconds', '3': 1, '4': 1, '5': 5, '10': 'remainingSeconds'},
  ],
};

/// Descriptor for `SessionClose`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sessionCloseDescriptor = $convert.base64Decode('CgxTZXNzaW9uQ2xvc2USKwoRcmVtYWluaW5nX3NlY29uZHMYASABKAVSEHJlbWFpbmluZ1NlY29uZHM=');
@$core.Deprecated('Use sessionStatusDescriptor instead')
const SessionStatus$json = const {
  '1': 'SessionStatus',
  '2': const [
    const {'1': 'keep_alive', '3': 1, '4': 1, '5': 11, '6': '.Pages.SessionKeepAlive', '9': 0, '10': 'keepAlive'},
    const {'1': 'close', '3': 2, '4': 1, '5': 11, '6': '.Pages.SessionClose', '9': 0, '10': 'close'},
  ],
  '8': const [
    const {'1': 'state'},
  ],
};

/// Descriptor for `SessionStatus`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sessionStatusDescriptor = $convert.base64Decode('Cg1TZXNzaW9uU3RhdHVzEjgKCmtlZXBfYWxpdmUYASABKAsyFy5QYWdlcy5TZXNzaW9uS2VlcEFsaXZlSABSCWtlZXBBbGl2ZRIrCgVjbG9zZRgCIAEoCzITLlBhZ2VzLlNlc3Npb25DbG9zZUgAUgVjbG9zZUIHCgVzdGF0ZQ==');
@$core.Deprecated('Use dataStateDescriptor instead')
const DataState$json = const {
  '1': 'DataState',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'state', '3': 2, '4': 1, '5': 14, '6': '.Pages.FieldState', '10': 'state'},
    const {'1': 'value', '3': 3, '4': 1, '5': 11, '6': '.Fields.FieldValue', '10': 'value'},
  ],
};

/// Descriptor for `DataState`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List dataStateDescriptor = $convert.base64Decode('CglEYXRhU3RhdGUSEgoEbmFtZRgBIAEoCVIEbmFtZRInCgVzdGF0ZRgCIAEoDjIRLlBhZ2VzLkZpZWxkU3RhdGVSBXN0YXRlEigKBXZhbHVlGAMgASgLMhIuRmllbGRzLkZpZWxkVmFsdWVSBXZhbHVl');
@$core.Deprecated('Use openRequestDescriptor instead')
const OpenRequest$json = const {
  '1': 'OpenRequest',
  '2': const [
    const {'1': 'page_id', '3': 1, '4': 1, '5': 9, '10': 'pageId'},
  ],
};

/// Descriptor for `OpenRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List openRequestDescriptor = $convert.base64Decode('CgtPcGVuUmVxdWVzdBIXCgdwYWdlX2lkGAEgASgJUgZwYWdlSWQ=');
@$core.Deprecated('Use closeRequestDescriptor instead')
const CloseRequest$json = const {
  '1': 'CloseRequest',
  '2': const [
    const {'1': 'page_id', '3': 1, '4': 1, '5': 9, '10': 'pageId'},
    const {'1': 'submitted', '3': 2, '4': 1, '5': 8, '9': 0, '10': 'submitted'},
    const {'1': 'dismissed', '3': 3, '4': 1, '5': 8, '9': 0, '10': 'dismissed'},
  ],
  '8': const [
    const {'1': 'reason'},
  ],
};

/// Descriptor for `CloseRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List closeRequestDescriptor = $convert.base64Decode('CgxDbG9zZVJlcXVlc3QSFwoHcGFnZV9pZBgBIAEoCVIGcGFnZUlkEh4KCXN1Ym1pdHRlZBgCIAEoCEgAUglzdWJtaXR0ZWQSHgoJZGlzbWlzc2VkGAMgASgISABSCWRpc21pc3NlZEIICgZyZWFzb24=');
@$core.Deprecated('Use pageResponseDescriptor instead')
const PageResponse$json = const {
  '1': 'PageResponse',
  '2': const [
    const {'1': 'page_id', '3': 1, '4': 1, '5': 9, '10': 'pageId'},
    const {'1': 'progress', '3': 2, '4': 1, '5': 11, '6': '.Pages.AsyncProgress', '9': 0, '10': 'progress'},
    const {'1': 'session', '3': 3, '4': 1, '5': 11, '6': '.Pages.SessionStatus', '9': 0, '10': 'session'},
  ],
  '8': const [
    const {'1': 'response'},
  ],
};

/// Descriptor for `PageResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pageResponseDescriptor = $convert.base64Decode('CgxQYWdlUmVzcG9uc2USFwoHcGFnZV9pZBgBIAEoCVIGcGFnZUlkEjIKCHByb2dyZXNzGAIgASgLMhQuUGFnZXMuQXN5bmNQcm9ncmVzc0gAUghwcm9ncmVzcxIwCgdzZXNzaW9uGAMgASgLMhQuUGFnZXMuU2Vzc2lvblN0YXR1c0gAUgdzZXNzaW9uQgoKCHJlc3BvbnNl');
@$core.Deprecated('Use dataRequestDescriptor instead')
const DataRequest$json = const {
  '1': 'DataRequest',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'value', '3': 2, '4': 1, '5': 11, '6': '.Fields.FieldValue', '10': 'value'},
  ],
};

/// Descriptor for `DataRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List dataRequestDescriptor = $convert.base64Decode('CgtEYXRhUmVxdWVzdBISCgRuYW1lGAEgASgJUgRuYW1lEigKBXZhbHVlGAIgASgLMhIuRmllbGRzLkZpZWxkVmFsdWVSBXZhbHVl');
@$core.Deprecated('Use confirmationResultDescriptor instead')
const ConfirmationResult$json = const {
  '1': 'ConfirmationResult',
  '2': const [
    const {'1': 'confirmed', '3': 1, '4': 1, '5': 8, '9': 0, '10': 'confirmed'},
    const {'1': 'failed', '3': 2, '4': 1, '5': 8, '9': 0, '10': 'failed'},
  ],
  '8': const [
    const {'1': 'state'},
  ],
};

/// Descriptor for `ConfirmationResult`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List confirmationResultDescriptor = $convert.base64Decode('ChJDb25maXJtYXRpb25SZXN1bHQSHgoJY29uZmlybWVkGAEgASgISABSCWNvbmZpcm1lZBIYCgZmYWlsZWQYAiABKAhIAFIGZmFpbGVkQgcKBXN0YXRl');
