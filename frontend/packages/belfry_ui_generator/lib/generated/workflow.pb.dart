///
//  Generated code. Do not modify.
//  source: workflow.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class WorkflowContext extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'WorkflowContext', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Workflow'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  WorkflowContext._() : super();
  factory WorkflowContext() => create();
  factory WorkflowContext.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory WorkflowContext.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  WorkflowContext clone() => WorkflowContext()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  WorkflowContext copyWith(void Function(WorkflowContext) updates) => super.copyWith((message) => updates(message as WorkflowContext)) as WorkflowContext; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static WorkflowContext create() => WorkflowContext._();
  WorkflowContext createEmptyInstance() => create();
  static $pb.PbList<WorkflowContext> createRepeated() => $pb.PbList<WorkflowContext>();
  @$core.pragma('dart2js:noInline')
  static WorkflowContext getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<WorkflowContext>(create);
  static WorkflowContext? _defaultInstance;
}

class WorkflowDescriptor extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'WorkflowDescriptor', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Workflow'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'correlationId')
    ..hasRequiredFields = false
  ;

  WorkflowDescriptor._() : super();
  factory WorkflowDescriptor({
    $core.String? correlationId,
  }) {
    final _result = create();
    if (correlationId != null) {
      _result.correlationId = correlationId;
    }
    return _result;
  }
  factory WorkflowDescriptor.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory WorkflowDescriptor.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  WorkflowDescriptor clone() => WorkflowDescriptor()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  WorkflowDescriptor copyWith(void Function(WorkflowDescriptor) updates) => super.copyWith((message) => updates(message as WorkflowDescriptor)) as WorkflowDescriptor; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static WorkflowDescriptor create() => WorkflowDescriptor._();
  WorkflowDescriptor createEmptyInstance() => create();
  static $pb.PbList<WorkflowDescriptor> createRepeated() => $pb.PbList<WorkflowDescriptor>();
  @$core.pragma('dart2js:noInline')
  static WorkflowDescriptor getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<WorkflowDescriptor>(create);
  static WorkflowDescriptor? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get correlationId => $_getSZ(0);
  @$pb.TagNumber(1)
  set correlationId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCorrelationId() => $_has(0);
  @$pb.TagNumber(1)
  void clearCorrelationId() => clearField(1);
}

class WorkflowType extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'WorkflowType', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Workflow'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'workflowId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..hasRequiredFields = false
  ;

  WorkflowType._() : super();
  factory WorkflowType({
    $core.String? workflowId,
    $core.String? name,
  }) {
    final _result = create();
    if (workflowId != null) {
      _result.workflowId = workflowId;
    }
    if (name != null) {
      _result.name = name;
    }
    return _result;
  }
  factory WorkflowType.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory WorkflowType.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  WorkflowType clone() => WorkflowType()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  WorkflowType copyWith(void Function(WorkflowType) updates) => super.copyWith((message) => updates(message as WorkflowType)) as WorkflowType; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static WorkflowType create() => WorkflowType._();
  WorkflowType createEmptyInstance() => create();
  static $pb.PbList<WorkflowType> createRepeated() => $pb.PbList<WorkflowType>();
  @$core.pragma('dart2js:noInline')
  static WorkflowType getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<WorkflowType>(create);
  static WorkflowType? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get workflowId => $_getSZ(0);
  @$pb.TagNumber(1)
  set workflowId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasWorkflowId() => $_has(0);
  @$pb.TagNumber(1)
  void clearWorkflowId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);
}

class WorkflowListRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'WorkflowListRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Workflow'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  WorkflowListRequest._() : super();
  factory WorkflowListRequest() => create();
  factory WorkflowListRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory WorkflowListRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  WorkflowListRequest clone() => WorkflowListRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  WorkflowListRequest copyWith(void Function(WorkflowListRequest) updates) => super.copyWith((message) => updates(message as WorkflowListRequest)) as WorkflowListRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static WorkflowListRequest create() => WorkflowListRequest._();
  WorkflowListRequest createEmptyInstance() => create();
  static $pb.PbList<WorkflowListRequest> createRepeated() => $pb.PbList<WorkflowListRequest>();
  @$core.pragma('dart2js:noInline')
  static WorkflowListRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<WorkflowListRequest>(create);
  static WorkflowListRequest? _defaultInstance;
}

class WorkflowListResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'WorkflowListResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Workflow'), createEmptyInstance: create)
    ..pc<WorkflowItem>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: WorkflowItem.create)
    ..hasRequiredFields = false
  ;

  WorkflowListResponse._() : super();
  factory WorkflowListResponse({
    $core.Iterable<WorkflowItem>? items,
  }) {
    final _result = create();
    if (items != null) {
      _result.items.addAll(items);
    }
    return _result;
  }
  factory WorkflowListResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory WorkflowListResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  WorkflowListResponse clone() => WorkflowListResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  WorkflowListResponse copyWith(void Function(WorkflowListResponse) updates) => super.copyWith((message) => updates(message as WorkflowListResponse)) as WorkflowListResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static WorkflowListResponse create() => WorkflowListResponse._();
  WorkflowListResponse createEmptyInstance() => create();
  static $pb.PbList<WorkflowListResponse> createRepeated() => $pb.PbList<WorkflowListResponse>();
  @$core.pragma('dart2js:noInline')
  static WorkflowListResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<WorkflowListResponse>(create);
  static WorkflowListResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<WorkflowItem> get items => $_getList(0);
}

class WorkflowItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'WorkflowItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Workflow'), createEmptyInstance: create)
    ..aOM<WorkflowType>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type', subBuilder: WorkflowType.create)
    ..pc<WorkflowInstance>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'instances', $pb.PbFieldType.PM, subBuilder: WorkflowInstance.create)
    ..hasRequiredFields = false
  ;

  WorkflowItem._() : super();
  factory WorkflowItem({
    WorkflowType? type,
    $core.Iterable<WorkflowInstance>? instances,
  }) {
    final _result = create();
    if (type != null) {
      _result.type = type;
    }
    if (instances != null) {
      _result.instances.addAll(instances);
    }
    return _result;
  }
  factory WorkflowItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory WorkflowItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  WorkflowItem clone() => WorkflowItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  WorkflowItem copyWith(void Function(WorkflowItem) updates) => super.copyWith((message) => updates(message as WorkflowItem)) as WorkflowItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static WorkflowItem create() => WorkflowItem._();
  WorkflowItem createEmptyInstance() => create();
  static $pb.PbList<WorkflowItem> createRepeated() => $pb.PbList<WorkflowItem>();
  @$core.pragma('dart2js:noInline')
  static WorkflowItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<WorkflowItem>(create);
  static WorkflowItem? _defaultInstance;

  @$pb.TagNumber(1)
  WorkflowType get type => $_getN(0);
  @$pb.TagNumber(1)
  set type(WorkflowType v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasType() => $_has(0);
  @$pb.TagNumber(1)
  void clearType() => clearField(1);
  @$pb.TagNumber(1)
  WorkflowType ensureType() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<WorkflowInstance> get instances => $_getList(1);
}

class WorkflowInstance extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'WorkflowInstance', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Workflow'), createEmptyInstance: create)
    ..aOM<WorkflowDescriptor>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'descriptor', subBuilder: WorkflowDescriptor.create)
    ..hasRequiredFields = false
  ;

  WorkflowInstance._() : super();
  factory WorkflowInstance({
    WorkflowDescriptor? descriptor,
  }) {
    final _result = create();
    if (descriptor != null) {
      _result.descriptor = descriptor;
    }
    return _result;
  }
  factory WorkflowInstance.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory WorkflowInstance.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  WorkflowInstance clone() => WorkflowInstance()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  WorkflowInstance copyWith(void Function(WorkflowInstance) updates) => super.copyWith((message) => updates(message as WorkflowInstance)) as WorkflowInstance; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static WorkflowInstance create() => WorkflowInstance._();
  WorkflowInstance createEmptyInstance() => create();
  static $pb.PbList<WorkflowInstance> createRepeated() => $pb.PbList<WorkflowInstance>();
  @$core.pragma('dart2js:noInline')
  static WorkflowInstance getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<WorkflowInstance>(create);
  static WorkflowInstance? _defaultInstance;

  @$pb.TagNumber(1)
  WorkflowDescriptor get descriptor => $_getN(0);
  @$pb.TagNumber(1)
  set descriptor(WorkflowDescriptor v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasDescriptor() => $_has(0);
  @$pb.TagNumber(1)
  void clearDescriptor() => clearField(1);
  @$pb.TagNumber(1)
  WorkflowDescriptor ensureDescriptor() => $_ensure(0);
}

enum WorkflowRequest_Type {
  create_1, 
  continue_2, 
  notSet
}

class WorkflowRequest extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, WorkflowRequest_Type> _WorkflowRequest_TypeByTag = {
    1 : WorkflowRequest_Type.create_1,
    2 : WorkflowRequest_Type.continue_2,
    0 : WorkflowRequest_Type.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'WorkflowRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Workflow'), createEmptyInstance: create)
    ..oo(0, [1, 2])
    ..aOM<WorkflowType>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'create', subBuilder: WorkflowType.create)
    ..aOM<WorkflowInstance>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'continue', subBuilder: WorkflowInstance.create)
    ..hasRequiredFields = false
  ;

  WorkflowRequest._() : super();
  factory WorkflowRequest({
    WorkflowType? create_1,
    WorkflowInstance? continue_2,
  }) {
    final _result = create();
    if (create_1 != null) {
      _result.create_1 = create_1;
    }
    if (continue_2 != null) {
      _result.continue_2 = continue_2;
    }
    return _result;
  }
  factory WorkflowRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory WorkflowRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  WorkflowRequest clone() => WorkflowRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  WorkflowRequest copyWith(void Function(WorkflowRequest) updates) => super.copyWith((message) => updates(message as WorkflowRequest)) as WorkflowRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static WorkflowRequest create() => WorkflowRequest._();
  WorkflowRequest createEmptyInstance() => create();
  static $pb.PbList<WorkflowRequest> createRepeated() => $pb.PbList<WorkflowRequest>();
  @$core.pragma('dart2js:noInline')
  static WorkflowRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<WorkflowRequest>(create);
  static WorkflowRequest? _defaultInstance;

  WorkflowRequest_Type whichType() => _WorkflowRequest_TypeByTag[$_whichOneof(0)]!;
  void clearType() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  WorkflowType get create_1 => $_getN(0);
  @$pb.TagNumber(1)
  set create_1(WorkflowType v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasCreate_1() => $_has(0);
  @$pb.TagNumber(1)
  void clearCreate_1() => clearField(1);
  @$pb.TagNumber(1)
  WorkflowType ensureCreate_1() => $_ensure(0);

  @$pb.TagNumber(2)
  WorkflowInstance get continue_2 => $_getN(1);
  @$pb.TagNumber(2)
  set continue_2(WorkflowInstance v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasContinue_2() => $_has(1);
  @$pb.TagNumber(2)
  void clearContinue_2() => clearField(2);
  @$pb.TagNumber(2)
  WorkflowInstance ensureContinue_2() => $_ensure(1);
}

