﻿namespace Grpc.Core
{
    public static class AsyncStreamExtensions
    {
        public static async IAsyncEnumerable<T> ToAsyncEnumerable<T>(this IAsyncStreamReader<T> stream)
            where T : class
        {
            while (await stream.MoveNext())
            {
                yield return stream.Current;
            }
        }

        public static async IAsyncEnumerable<T> ToAsyncEnumerable<T>(this AsyncServerStreamingCall<T> stream)
            where T : class
        {
            while (await stream.ResponseStream.MoveNext())
            {
                yield return stream.ResponseStream.Current;
            }
        }
    }
}
