///
//  Generated code. Do not modify.
//  source: fields.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use unitTypeDescriptor instead')
const UnitType$json = const {
  '1': 'UnitType',
  '2': const [
    const {'1': 'nonunit', '2': 0},
    const {'1': 'count', '2': 1},
    const {'1': 'quantity', '2': 2},
    const {'1': 'distance', '2': 3},
    const {'1': 'weight', '2': 4},
  ],
};

/// Descriptor for `UnitType`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List unitTypeDescriptor = $convert.base64Decode('CghVbml0VHlwZRILCgdub251bml0EAASCQoFY291bnQQARIMCghxdWFudGl0eRACEgwKCGRpc3RhbmNlEAMSCgoGd2VpZ2h0EAQ=');
@$core.Deprecated('Use entityTypeDescriptor instead')
const EntityType$json = const {
  '1': 'EntityType',
  '2': const [
    const {'1': 'poco', '2': 0},
  ],
};

/// Descriptor for `EntityType`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List entityTypeDescriptor = $convert.base64Decode('CgpFbnRpdHlUeXBlEggKBHBvY28QAA==');
@$core.Deprecated('Use fieldDescriptor instead')
const Field$json = const {
  '1': 'Field',
  '2': const [
    const {'1': 'field_id', '3': 1, '4': 1, '5': 9, '10': 'fieldId'},
    const {'1': 'schema_id', '3': 2, '4': 1, '5': 9, '10': 'schemaId'},
    const {'1': 'logical', '3': 3, '4': 1, '5': 11, '6': '.Fields.LogicalField', '9': 0, '10': 'logical'},
    const {'1': 'text', '3': 4, '4': 1, '5': 11, '6': '.Fields.StringField', '9': 0, '10': 'text'},
    const {'1': 'number', '3': 5, '4': 1, '5': 11, '6': '.Fields.NumberField', '9': 0, '10': 'number'},
    const {'1': 'unit', '3': 6, '4': 1, '5': 11, '6': '.Fields.UnitField', '9': 0, '10': 'unit'},
    const {'1': 'timestamp', '3': 7, '4': 1, '5': 11, '6': '.Fields.TimeStampField', '9': 0, '10': 'timestamp'},
    const {'1': 'duration', '3': 8, '4': 1, '5': 11, '6': '.Fields.DurationField', '9': 0, '10': 'duration'},
    const {'1': 'reference', '3': 9, '4': 1, '5': 11, '6': '.Fields.ReferenceField', '9': 0, '10': 'reference'},
    const {'1': 'enumerated', '3': 10, '4': 1, '5': 11, '6': '.Fields.EnumeratedField', '9': 0, '10': 'enumerated'},
  ],
  '8': const [
    const {'1': 'type'},
  ],
};

/// Descriptor for `Field`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List fieldDescriptor = $convert.base64Decode('CgVGaWVsZBIZCghmaWVsZF9pZBgBIAEoCVIHZmllbGRJZBIbCglzY2hlbWFfaWQYAiABKAlSCHNjaGVtYUlkEjAKB2xvZ2ljYWwYAyABKAsyFC5GaWVsZHMuTG9naWNhbEZpZWxkSABSB2xvZ2ljYWwSKQoEdGV4dBgEIAEoCzITLkZpZWxkcy5TdHJpbmdGaWVsZEgAUgR0ZXh0Ei0KBm51bWJlchgFIAEoCzITLkZpZWxkcy5OdW1iZXJGaWVsZEgAUgZudW1iZXISJwoEdW5pdBgGIAEoCzIRLkZpZWxkcy5Vbml0RmllbGRIAFIEdW5pdBI2Cgl0aW1lc3RhbXAYByABKAsyFi5GaWVsZHMuVGltZVN0YW1wRmllbGRIAFIJdGltZXN0YW1wEjMKCGR1cmF0aW9uGAggASgLMhUuRmllbGRzLkR1cmF0aW9uRmllbGRIAFIIZHVyYXRpb24SNgoJcmVmZXJlbmNlGAkgASgLMhYuRmllbGRzLlJlZmVyZW5jZUZpZWxkSABSCXJlZmVyZW5jZRI5CgplbnVtZXJhdGVkGAogASgLMhcuRmllbGRzLkVudW1lcmF0ZWRGaWVsZEgAUgplbnVtZXJhdGVkQgYKBHR5cGU=');
@$core.Deprecated('Use logicalFieldDescriptor instead')
const LogicalField$json = const {
  '1': 'LogicalField',
  '2': const [
    const {'1': 'default_value', '3': 1, '4': 1, '5': 8, '10': 'defaultValue'},
  ],
};

/// Descriptor for `LogicalField`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List logicalFieldDescriptor = $convert.base64Decode('CgxMb2dpY2FsRmllbGQSIwoNZGVmYXVsdF92YWx1ZRgBIAEoCFIMZGVmYXVsdFZhbHVl');
@$core.Deprecated('Use stringFieldDescriptor instead')
const StringField$json = const {
  '1': 'StringField',
};

/// Descriptor for `StringField`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List stringFieldDescriptor = $convert.base64Decode('CgtTdHJpbmdGaWVsZA==');
@$core.Deprecated('Use numberFieldDescriptor instead')
const NumberField$json = const {
  '1': 'NumberField',
  '2': const [
    const {'1': 'floating', '3': 1, '4': 1, '5': 8, '10': 'floating'},
  ],
};

/// Descriptor for `NumberField`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List numberFieldDescriptor = $convert.base64Decode('CgtOdW1iZXJGaWVsZBIaCghmbG9hdGluZxgBIAEoCFIIZmxvYXRpbmc=');
@$core.Deprecated('Use unitFieldDescriptor instead')
const UnitField$json = const {
  '1': 'UnitField',
  '2': const [
    const {'1': 'unit', '3': 1, '4': 1, '5': 14, '6': '.Fields.UnitType', '10': 'unit'},
    const {'1': 'defalt_value', '3': 2, '4': 1, '5': 1, '10': 'defaltValue'},
  ],
};

/// Descriptor for `UnitField`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List unitFieldDescriptor = $convert.base64Decode('CglVbml0RmllbGQSJAoEdW5pdBgBIAEoDjIQLkZpZWxkcy5Vbml0VHlwZVIEdW5pdBIhCgxkZWZhbHRfdmFsdWUYAiABKAFSC2RlZmFsdFZhbHVl');
@$core.Deprecated('Use timeStampFieldDescriptor instead')
const TimeStampField$json = const {
  '1': 'TimeStampField',
  '2': const [
    const {'1': 'epoch_seconds', '3': 1, '4': 1, '5': 8, '10': 'epochSeconds'},
    const {'1': 'default_value', '3': 2, '4': 1, '5': 3, '10': 'defaultValue'},
  ],
};

/// Descriptor for `TimeStampField`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List timeStampFieldDescriptor = $convert.base64Decode('Cg5UaW1lU3RhbXBGaWVsZBIjCg1lcG9jaF9zZWNvbmRzGAEgASgIUgxlcG9jaFNlY29uZHMSIwoNZGVmYXVsdF92YWx1ZRgCIAEoA1IMZGVmYXVsdFZhbHVl');
@$core.Deprecated('Use durationFieldDescriptor instead')
const DurationField$json = const {
  '1': 'DurationField',
  '2': const [
    const {'1': 'default_seconds', '3': 1, '4': 1, '5': 5, '10': 'defaultSeconds'},
  ],
};

/// Descriptor for `DurationField`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List durationFieldDescriptor = $convert.base64Decode('Cg1EdXJhdGlvbkZpZWxkEicKD2RlZmF1bHRfc2Vjb25kcxgBIAEoBVIOZGVmYXVsdFNlY29uZHM=');
@$core.Deprecated('Use referenceFieldDescriptor instead')
const ReferenceField$json = const {
  '1': 'ReferenceField',
  '2': const [
    const {'1': 'schema_id', '3': 1, '4': 1, '5': 9, '10': 'schemaId'},
    const {'1': 'options', '3': 2, '4': 3, '5': 11, '6': '.Fields.Item', '10': 'options'},
  ],
};

/// Descriptor for `ReferenceField`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List referenceFieldDescriptor = $convert.base64Decode('Cg5SZWZlcmVuY2VGaWVsZBIbCglzY2hlbWFfaWQYASABKAlSCHNjaGVtYUlkEiYKB29wdGlvbnMYAiADKAsyDC5GaWVsZHMuSXRlbVIHb3B0aW9ucw==');
@$core.Deprecated('Use enumeratedFieldDescriptor instead')
const EnumeratedField$json = const {
  '1': 'EnumeratedField',
  '2': const [
    const {'1': 'values', '3': 1, '4': 3, '5': 11, '6': '.Fields.KeyValue', '10': 'values'},
  ],
};

/// Descriptor for `EnumeratedField`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List enumeratedFieldDescriptor = $convert.base64Decode('Cg9FbnVtZXJhdGVkRmllbGQSKAoGdmFsdWVzGAEgAygLMhAuRmllbGRzLktleVZhbHVlUgZ2YWx1ZXM=');
@$core.Deprecated('Use keyValueDescriptor instead')
const KeyValue$json = const {
  '1': 'KeyValue',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'text', '3': 2, '4': 1, '5': 9, '9': 0, '10': 'text'},
    const {'1': 'number', '3': 3, '4': 1, '5': 5, '9': 0, '10': 'number'},
  ],
  '8': const [
    const {'1': 'value'},
  ],
};

/// Descriptor for `KeyValue`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List keyValueDescriptor = $convert.base64Decode('CghLZXlWYWx1ZRIQCgNrZXkYASABKAlSA2tleRIUCgR0ZXh0GAIgASgJSABSBHRleHQSGAoGbnVtYmVyGAMgASgFSABSBm51bWJlckIHCgV2YWx1ZQ==');
@$core.Deprecated('Use schemaVersionDescriptor instead')
const SchemaVersion$json = const {
  '1': 'SchemaVersion',
  '2': const [
    const {'1': 'number', '3': 2, '4': 1, '5': 5, '10': 'number'},
    const {'1': 'epoch', '3': 3, '4': 1, '5': 3, '10': 'epoch'},
  ],
};

/// Descriptor for `SchemaVersion`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List schemaVersionDescriptor = $convert.base64Decode('Cg1TY2hlbWFWZXJzaW9uEhYKBm51bWJlchgCIAEoBVIGbnVtYmVyEhQKBWVwb2NoGAMgASgDUgVlcG9jaA==');
@$core.Deprecated('Use schemaDescriptor instead')
const Schema$json = const {
  '1': 'Schema',
  '2': const [
    const {'1': 'schema_id', '3': 1, '4': 1, '5': 9, '10': 'schemaId'},
    const {'1': 'type', '3': 2, '4': 1, '5': 14, '6': '.Fields.EntityType', '10': 'type'},
    const {'1': 'version', '3': 3, '4': 1, '5': 11, '6': '.Fields.SchemaVersion', '10': 'version'},
    const {'1': 'fields', '3': 4, '4': 3, '5': 11, '6': '.Fields.Field', '10': 'fields'},
  ],
};

/// Descriptor for `Schema`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List schemaDescriptor = $convert.base64Decode('CgZTY2hlbWESGwoJc2NoZW1hX2lkGAEgASgJUghzY2hlbWFJZBImCgR0eXBlGAIgASgOMhIuRmllbGRzLkVudGl0eVR5cGVSBHR5cGUSLwoHdmVyc2lvbhgDIAEoCzIVLkZpZWxkcy5TY2hlbWFWZXJzaW9uUgd2ZXJzaW9uEiUKBmZpZWxkcxgEIAMoCzINLkZpZWxkcy5GaWVsZFIGZmllbGRz');
@$core.Deprecated('Use enumeratedValueDescriptor instead')
const EnumeratedValue$json = const {
  '1': 'EnumeratedValue',
  '2': const [
    const {'1': 'keys', '3': 1, '4': 3, '5': 9, '10': 'keys'},
  ],
};

/// Descriptor for `EnumeratedValue`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List enumeratedValueDescriptor = $convert.base64Decode('Cg9FbnVtZXJhdGVkVmFsdWUSEgoEa2V5cxgBIAMoCVIEa2V5cw==');
@$core.Deprecated('Use fieldValueDescriptor instead')
const FieldValue$json = const {
  '1': 'FieldValue',
  '2': const [
    const {'1': 'field_id', '3': 1, '4': 1, '5': 9, '10': 'fieldId'},
    const {'1': 'logical', '3': 2, '4': 1, '5': 8, '9': 0, '10': 'logical'},
    const {'1': 'text', '3': 3, '4': 1, '5': 9, '9': 0, '10': 'text'},
    const {'1': 'integer', '3': 4, '4': 1, '5': 5, '9': 0, '10': 'integer'},
    const {'1': 'long', '3': 5, '4': 1, '5': 3, '9': 0, '10': 'long'},
    const {'1': 'double', '3': 6, '4': 1, '5': 1, '9': 0, '10': 'double'},
    const {'1': 'list', '3': 8, '4': 1, '5': 11, '6': '.Fields.EnumeratedValue', '9': 0, '10': 'list'},
    const {'1': 'item_id', '3': 9, '4': 1, '5': 9, '9': 0, '10': 'itemId'},
  ],
  '8': const [
    const {'1': 'value'},
  ],
};

/// Descriptor for `FieldValue`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List fieldValueDescriptor = $convert.base64Decode('CgpGaWVsZFZhbHVlEhkKCGZpZWxkX2lkGAEgASgJUgdmaWVsZElkEhoKB2xvZ2ljYWwYAiABKAhIAFIHbG9naWNhbBIUCgR0ZXh0GAMgASgJSABSBHRleHQSGgoHaW50ZWdlchgEIAEoBUgAUgdpbnRlZ2VyEhQKBGxvbmcYBSABKANIAFIEbG9uZxIYCgZkb3VibGUYBiABKAFIAFIGZG91YmxlEi0KBGxpc3QYCCABKAsyFy5GaWVsZHMuRW51bWVyYXRlZFZhbHVlSABSBGxpc3QSGQoHaXRlbV9pZBgJIAEoCUgAUgZpdGVtSWRCBwoFdmFsdWU=');
@$core.Deprecated('Use itemDescriptor instead')
const Item$json = const {
  '1': 'Item',
  '2': const [
    const {'1': 'schema_id', '3': 1, '4': 1, '5': 9, '10': 'schemaId'},
    const {'1': 'item_id', '3': 2, '4': 1, '5': 9, '10': 'itemId'},
    const {'1': 'values', '3': 3, '4': 3, '5': 11, '6': '.Fields.FieldValue', '10': 'values'},
  ],
};

/// Descriptor for `Item`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemDescriptor = $convert.base64Decode('CgRJdGVtEhsKCXNjaGVtYV9pZBgBIAEoCVIIc2NoZW1hSWQSFwoHaXRlbV9pZBgCIAEoCVIGaXRlbUlkEioKBnZhbHVlcxgDIAMoCzISLkZpZWxkcy5GaWVsZFZhbHVlUgZ2YWx1ZXM=');
