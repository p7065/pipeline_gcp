import 'package:belfry_ui_generator/src/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_extra_fields/form_builder_extra_fields.dart';
import 'package:form_builder_file_picker/form_builder_file_picker.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

import '../generated/forms.pb.dart' as $forms;
import '../generated/fields.pb.dart' as $fields;
import 'sample_data.dart' as $sampledata;

class FormGenerator extends StatelessWidget {
  final $forms.Layout layout;
  final $fields.Schema schema;
  final $fields.Item item;
  final _formKey = GlobalKey<FormBuilderState>();
  final void Function(dynamic val)? onChanged;
  final TextStyle Function(int n)? getTextStyle;

  FormGenerator({
    Key? key,
    required this.layout,
    required this.schema,
    required this.item,
    this.onChanged,
    this.getTextStyle,
  }) : super(key: key);

  void _onChanged(dynamic val) =>
      onChanged != null ? onChanged!(val) : debugPrint('$val');

  void saveForm() {
    _formKey.currentState!.save();
  }

  Map<String, dynamic> getFormValue() {
    saveForm();
    if (_formKey.currentState!.validate()) {
      // print(_formKey.currentState!.value);
      return _formKey.currentState!.value;
    } else {
      debugPrint("validation failed");
      return {};
      // return {"Error": "Validation failed."};
    }
  }

  void resetForm() {
    _formKey.currentState!.reset();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) => FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.always,
        child: Column(children: [
          ...buildForm(layout, context, constraints),
        ]),
      ),
    );
  }

  List<Widget> buildForm(
      $forms.Layout layout, BuildContext context, BoxConstraints constraints) {
    return layout.containers
        .map((e) => _getContainer(e, context, constraints))
        .toList();
  }

  Widget _getContainer(
      $forms.Container elem, BuildContext context, BoxConstraints constraints) {
    if (elem.hasFormField()) {
      return Container(
        width: constraints.maxWidth / elem.columns,
        padding: EdgeInsets.all(layout.gap / 2),
        child: _buildFormField(elem.formField, context, constraints),
      );
    } else if (elem.hasRow()) {
      return SizedBox(
          width: constraints.maxWidth / elem.columns,
          child: LayoutBuilder(
            builder: (context, constraints) => Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: elem.row.children
                  .map((e) => _getContainer(e, context, constraints))
                  .toList(),
            ),
          ));
    } else if (elem.hasColumn()) {
      return SizedBox(
          width: constraints.maxWidth / elem.columns,
          child: LayoutBuilder(
            builder: (context, constraints) => Column(
              children: elem.column.children
                  .map((e) => _getContainer(e, context, constraints))
                  .toList(),
            ),
          ));
    }
    return const Text('No field');
  }

  FormFieldValidator _getValidators(
      BuildContext context, List<$forms.Validator> validators) {
    List<FormFieldValidator> formFieldValidators = [];
    for ($forms.Validator validator in validators) {
      var formFieldValidator = _buildValidator(context, validator);
      if (formFieldValidator != null) {
        formFieldValidators.add(formFieldValidator);
      }
    }
    return FormBuilderValidators.compose(formFieldValidators);
  }

  _buildValidator(BuildContext context, $forms.Validator validator) {
    if (validator.hasRequired()) {
      return FormBuilderValidators.required(
        context,
        errorText: validator.hasErrorText()
            ? validator.errorText
            : FormBuilderLocalizations.of(context).requiredErrorText,
      );
    } else if (validator.hasEqual()) {
      return FormBuilderValidators.equal(
        context,
        validator.equal.value,
        errorText: validator.hasErrorText()
            ? validator.errorText
            : FormBuilderLocalizations.of(context)
                .equalErrorText(validator.equal.value),
      );
    } else if (validator.hasNotEqual()) {
      return FormBuilderValidators.notEqual(
        context,
        validator.notEqual.value,
        errorText: validator.hasErrorText()
            ? validator.errorText
            : FormBuilderLocalizations.of(context)
                .notEqualErrorText(validator.notEqual.value),
      );
    } else if (validator.hasMin()) {
      return FormBuilderValidators.min(
        context,
        validator.min.min,
        inclusive:
            validator.min.hasInclusive() ? validator.min.inclusive : true,
        errorText: validator.hasErrorText()
            ? validator.errorText
            : FormBuilderLocalizations.of(context)
                .minErrorText(validator.min.min),
      );
    } else if (validator.hasMax()) {
      return FormBuilderValidators.max(
        context,
        validator.max.max,
        inclusive:
            validator.max.hasInclusive() ? validator.max.inclusive : true,
        errorText: validator.hasErrorText()
            ? validator.errorText
            : FormBuilderLocalizations.of(context)
                .maxErrorText(validator.max.max),
      );
    } else if (validator.hasMinLength()) {
      return FormBuilderValidators.minLength(
        context,
        validator.minLength.minLength,
        allowEmpty: validator.minLength.hasAllowEmpty()
            ? validator.minLength.allowEmpty
            : false,
        errorText: validator.hasErrorText()
            ? validator.errorText
            : FormBuilderLocalizations.of(context)
                .minLengthErrorText(validator.minLength.minLength),
      );
    } else if (validator.hasMaxLength()) {
      return FormBuilderValidators.maxLength(
        context,
        validator.maxLength.maxLength,
        errorText: validator.hasErrorText()
            ? validator.errorText
            : FormBuilderLocalizations.of(context)
                .maxLengthErrorText(validator.maxLength.maxLength),
      );
    } else if (validator.hasMatch()) {
      return FormBuilderValidators.match(
        context,
        validator.match.pattern,
        errorText: validator.hasErrorText()
            ? validator.errorText
            : FormBuilderLocalizations.of(context).matchErrorText,
      );
    }
    return null;
  }

  InputDecoration _getInputDecoration($forms.FormField formField) {
    return InputDecoration(
      labelText: formField.hasLabel() ? formField.label : '',
      //  Ide jöhet minden label style, hint style stb.
    );
  }

  _getRichText($forms.Text text) {
    return RichText(text: _getTextSpan(text));
  }

  _getTextStyle(int n) => getTextStyle != null ? getTextStyle!(n) : null;

  _getTextSpan($forms.Text title) {
    if (title.hasText()) {
      return TextSpan(
        text: title.text,
        style: title.hasTextStyle() ? _getTextStyle(title.textStyle) : null,
      );
    } else if (title.hasTexts()) {
      List<TextSpan> children = [];
      for (var textSpan in title.texts.children) {
        children.add(_getTextSpan(textSpan));
      }
      return TextSpan(children: children);
    }
  }

  List<FormBuilderFieldOption<String>> _getOptions(String fieldId) {
    var field = schema.fields.firstWhere((e) => e.fieldId == fieldId);
    return field.enumerated.values
        .map((o) => FormBuilderFieldOption(
            value: o.key,
            child: Text(o.hasText() ? o.text : o.number.toString())))
        .toList();
  }

  List<String>? _getSimpleOptions(String fieldId) {
    var field = schema.fields.firstWhere((e) => e.fieldId == fieldId);
    return field.enumerated.values.map((o) => o.key).toList();
  }

  FormBuilderField _buildFormField($forms.FormField formField,
      BuildContext context, BoxConstraints constraints) {
    var field = item.values.firstWhere((v) => v.fieldId == formField.fieldId);

    if (formField.control.hasTextField()) {
      return getTextField(formField, field, context);
    } else if (formField.control.hasFilePicker()) {
      return getFilePicker(formField, context);
    } else if (formField.control.hasCheckbox()) {
      return getCheckbox(formField, field, context);
    } else if (formField.control.hasCheckboxGroup()) {
      return getCheckboxGroup(formField, field, context);
    }
    // TODO: from here on down fields are not returned from a method, these still need significant work.
    else if (formField.control.hasChoiceChip()) {
      return FormBuilderChoiceChip(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        name: formField.fieldId,
        options: _getOptions(formField.fieldId),
        decoration: _getInputDecoration(formField),
        onChanged: _onChanged,
        validator: _getValidators(context, formField.validators),
        selectedColor: hexToColor(layout.theme.accentColor),
      );
    } else if (formField.control.hasDateRangePicker()) {
      return FormBuilderDateRangePicker(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        name: formField.fieldId,
        firstDate: DateTime(1970),
        lastDate: DateTime(2030),
        format: DateFormat('yyyy-MM-dd'),
        decoration: _getInputDecoration(formField),
        onChanged: _onChanged,
        validator: _getValidators(context, formField.validators),
      );
    } else if (formField.control.hasDateTimePicker()) {
      return FormBuilderDateTimePicker(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        name: formField.fieldId,
        inputType: InputType.time,
        initialTime: TimeOfDay(hour: 8, minute: 0),
        // initialValue: DateTime.now(),
        // enabled: true,
        decoration: _getInputDecoration(formField),
        onChanged: _onChanged,
        validator: _getValidators(context, formField.validators),
      );
    } else if (formField.control.hasDropdown()) {
      return FormBuilderDropdown(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        name: formField.fieldId,
        // initialValue: 'Male',
        // allowClear: true,
        hint: Text('Select Gender'),
        items: ['Male', 'Female', 'Other']
            .map((gender) => DropdownMenuItem(
                  value: gender,
                  child: Text('$gender'),
                ))
            .toList(),
        decoration: _getInputDecoration(formField),
        onChanged: _onChanged,
        validator: _getValidators(context, formField.validators),
      );
    } else if (formField.control.hasFilterChip()) {
      return FormBuilderFilterChip(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        name: formField.fieldId,
        options: _getOptions(formField.fieldId),
        decoration: _getInputDecoration(formField),
        onChanged: _onChanged,
        validator: _getValidators(context, formField.validators),
      );
    } else if (formField.control.hasRadioGroup()) {
      return FormBuilderRadioGroup<String>(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        name: formField.fieldId,
        initialValue: null,
        options: _getOptions(formField.fieldId),
        // controlAffinity: ControlAffinity.trailing,
        decoration: _getInputDecoration(formField),
        onChanged: _onChanged,
        validator: _getValidators(context, formField.validators),
        activeColor: hexToColor(layout.theme.accentColor),
      );
    } else if (formField.control.hasRangeSlider()) {
      return FormBuilderRangeSlider(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        name: formField.fieldId,
        min: 0.0,
        max: 100.0,
        initialValue: const RangeValues(4, 7),
        divisions: 20,
        activeColor: hexToColor(layout.theme.primaryColor),
        inactiveColor: hexToColor(layout.theme.primaryColor).withOpacity(0.2),
        decoration: _getInputDecoration(formField),
        onChanged: _onChanged,
        validator: _getValidators(context, formField.validators),
      );
    } else if (formField.control.hasSegmentedControl()) {
      return FormBuilderSegmentedControl(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        name: formField.fieldId,
        borderColor: hexToColor(layout.theme.accentColor),
        pressedColor: hexToColor(layout.theme.accentColor),
        selectedColor: hexToColor(layout.theme.primaryColor),
        options: _getOptions(formField.fieldId),
        decoration: _getInputDecoration(formField),
        onChanged: _onChanged,
        validator: _getValidators(context, formField.validators),
      );
    } else if (formField.control.hasSlider()) {
      return FormBuilderSlider(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        name: formField.fieldId,
        min: 0.0,
        max: 10.0,
        initialValue: 7.0,
        divisions: 20,
        activeColor: hexToColor(layout.theme.primaryColor),
        inactiveColor: hexToColor(layout.theme.primaryColor).withOpacity(0.2),
        decoration: _getInputDecoration(formField),
        onChanged: _onChanged,
        validator: _getValidators(context, formField.validators),
      );
    } else if (formField.control.hasSwitchField()) {
      return FormBuilderSwitch(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        name: formField.fieldId,
        title: _getRichText(formField.title),
        activeColor: hexToColor(layout.theme.primaryColor),
        inactiveTrackColor:
            hexToColor(layout.theme.primaryColor).withOpacity(0.2),
        decoration: _getInputDecoration(formField),
        onChanged: _onChanged,
        validator: _getValidators(context, formField.validators),
      );
    } else if (formField.control.hasSearchableDropdown()) {
      return FormBuilderSearchableDropdown(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        name: formField.fieldId,
        items: const [
          'Africa',
          'Asia',
          'Australia',
          'Europe',
          'North America',
          'South America'
        ],
        decoration: _getInputDecoration(formField),
        onChanged: _onChanged,
        validator: _getValidators(context, formField.validators),
      );
    } else if (formField.control.hasColorPicker()) {
      return FormBuilderColorPickerField(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        name: formField.fieldId,
        initialValue: field.hasText() ? hexToColor(field.text) : null,
        decoration: _getInputDecoration(formField),
        onChanged: _onChanged,
        validator: _getValidators(context, formField.validators),
      );
    } else if (formField.control.hasCupertinoDateTimePicker()) {
      return FormBuilderCupertinoDateTimePicker(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        name: formField.fieldId,
        // initialValue: DateTime.now(),
        inputType: CupertinoDateTimePickerInputType.both,
        decoration: const InputDecoration(
          labelText: 'Cupertino DateTime Picker',
        ),
      );
    } else if (formField.control.hasTypeAhead()) {
      return FormBuilderTypeAhead<String>(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        decoration: const InputDecoration(
            labelText: 'TypeAhead (Autocomplete TextField)',
            hintText: 'Start typing continent name'),
        name: 'continent',
        itemBuilder: (context, continent) {
          return ListTile(title: Text(continent));
        },
        suggestionsCallback: (query) {
          if (query.isNotEmpty) {
            var lowercaseQuery = query.toLowerCase();
            return $sampledata.allCountries.where((continent) {
              return continent.toLowerCase().contains(lowercaseQuery);
            }).toList(growable: false)
              ..sort((a, b) => a
                  .toLowerCase()
                  .indexOf(lowercaseQuery)
                  .compareTo(b.toLowerCase().indexOf(lowercaseQuery)));
          } else {
            return $sampledata.allCountries;
          }
        },
      );
    } else if (formField.control.hasTouchSpin()) {
      return FormBuilderTouchSpin(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        decoration: const InputDecoration(labelText: 'TouchSpin'),
        name: 'touch_spin',
        initialValue: 10,
        step: 1,
        iconSize: 48.0,
        addIcon: const Icon(Icons.arrow_right),
        subtractIcon: const Icon(Icons.arrow_left),
      );
    } else if (formField.control.hasRating()) {
      return FormBuilderRatingBar(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        name: 'rate',
        decoration: const InputDecoration(labelText: 'Rating'),
        itemSize: 32.0,
        initialValue: 1.0,
        maxRating: 5.0,
      );
    } else if (formField.control.hasSignaturePad()) {
      return FormBuilderSignaturePad(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        name: 'signature',
        decoration: const InputDecoration(
          labelText: 'Signature Pad',
        ),
        border: Border.all(color: Colors.green),
      );
    } else if (formField.control.hasChipsInput()) {
      return FormBuilderChipsInput<$sampledata.Contact>(
        enabled: formField.hasEnabled() ? formField.enabled : true,
        name: formField.fieldId,
        decoration: _getInputDecoration(formField),
        onChanged: _onChanged,
        validator: _getValidators(context, formField.validators),
        maxChips: formField.control.chipsInput.maxChips,
        findSuggestions: (String query) {
          if (query.isNotEmpty) {
            var lowercaseQuery = query.toLowerCase();
            return $sampledata.contacts.where((profile) {
              return profile.name.toLowerCase().contains(query.toLowerCase()) ||
                  profile.email.toLowerCase().contains(query.toLowerCase());
            }).toList(growable: false)
              ..sort((a, b) => a.name
                  .toLowerCase()
                  .indexOf(lowercaseQuery)
                  .compareTo(b.name.toLowerCase().indexOf(lowercaseQuery)));
          } else {
            return const <$sampledata.Contact>[];
          }
        },
        chipBuilder: (context, state, profile) {
          return InputChip(
            key: ObjectKey(profile),
            label: Text(profile.name),
            avatar: CircleAvatar(
              backgroundImage: NetworkImage(profile.imageUrl),
            ),
            onDeleted: () => state.deleteChip(profile),
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          );
        },
        suggestionBuilder: (context, state, profile) {
          return ListTile(
            key: ObjectKey(profile),
            leading: CircleAvatar(
              backgroundImage: NetworkImage(profile.imageUrl),
            ),
            title: Text(profile.name),
            subtitle: Text(profile.email),
            onTap: () => state.selectSuggestion(profile),
          );
        },
      );
    }
    // default
    return FormBuilderField(
        builder: (FormFieldState<dynamic> field) => Container(),
        name: 'default field');
  }

  FormBuilderCheckboxGroup<String> getCheckboxGroup($forms.FormField formField,
      $fields.FieldValue field, BuildContext context) {
    var checkboxGroup = formField.control.checkboxGroup;

    _getOrientation() {
      if (checkboxGroup.hasDirection()) {
        return checkboxGroup.direction == $forms.Direction.horizontal
            ? OptionsOrientation.horizontal
            : OptionsOrientation.vertical;
      }
      return OptionsOrientation.wrap;
    }

    // bool isWrap() => _getOrientation() == OptionsOrientation.wrap;

    dynamic _getWrapAlignment($forms.Alignment alignment,
        {bool crossAxis = false}) {
      switch (alignment) {
        case $forms.Alignment.start:
          return crossAxis ? WrapCrossAlignment.start : WrapAlignment.start;
        case $forms.Alignment.end:
          return crossAxis ? WrapCrossAlignment.end : WrapAlignment.end;
        case $forms.Alignment.center:
          return crossAxis ? WrapCrossAlignment.center : WrapAlignment.center;
        case $forms.Alignment.space_around:
          return WrapAlignment.spaceAround;
        case $forms.Alignment.space_between:
          return WrapAlignment.spaceBetween;
        case $forms.Alignment.space_evenly:
          return WrapAlignment.spaceEvenly;
        default:
          return crossAxis ? WrapCrossAlignment.start : WrapAlignment.start;
      }
    }

    return FormBuilderCheckboxGroup<String>(
      enabled: formField.hasEnabled() ? formField.enabled : true,
      name: formField.fieldId,
      initialValue: field.hasList() ? field.list.keys : null,
      options: _getOptions(formField.fieldId),
      separator: const VerticalDivider(
        width: 10,
        thickness: 5,
        color: Colors.grey,
      ),
      decoration: _getInputDecoration(formField),
      onChanged: _onChanged,
      validator: _getValidators(context, formField.validators),
      activeColor: hexToColor(checkboxGroup.peculiar.activeColor),
      checkColor: hexToColor(checkboxGroup.peculiar.checkColor),
      tristate: checkboxGroup.peculiar.tristate,
      focusColor: hexToColor(checkboxGroup.focusColor),
      hoverColor: hexToColor(checkboxGroup.hoverColor),
      orientation: _getOrientation(),
      wrapDirection: checkboxGroup.wrap.hasDirection() &&
              checkboxGroup.wrap.direction == $forms.Direction.vertical
          ? Axis.vertical
          : Axis.horizontal,
      wrapAlignment: _getWrapAlignment(checkboxGroup.wrap.alignment),
      wrapSpacing: checkboxGroup.wrap.spacing,
      wrapRunAlignment: _getWrapAlignment(checkboxGroup.wrap.runAlignment),
      wrapRunSpacing: checkboxGroup.wrap.runSpacing,
      wrapCrossAxisAlignment: _getWrapAlignment(
          checkboxGroup.wrap.crossAxisAlignment,
          crossAxis: true),
      wrapVerticalDirection:
          checkboxGroup.wrap.verticalDirection == $forms.VerticalDirection.up
              ? VerticalDirection.up
              : VerticalDirection.down,
    );
  }

  FormBuilderTextField getTextField($forms.FormField formField,
      $fields.FieldValue field, BuildContext context) {
    var textField = formField.control.textField;
    return FormBuilderTextField(
      enabled: formField.hasEnabled() ? formField.enabled : true,
      name: formField.fieldId,
      initialValue: field.hasText() ? field.text : null,
      decoration: _getInputDecoration(formField),
      onChanged: _onChanged,
      validator: _getValidators(context, formField.validators),
      maxLines: textField.hasMaxLines() ? textField.maxLines : 1,
      minLines: textField.hasMinLines() ? textField.minLines : 1,
      style: _getTextStyle(textField.textStyle),
      // maxLength: textField.hasMaxLength() ? textField.maxLength : -1,
    );
  }

  FormBuilderFilePicker getFilePicker(
      $forms.FormField formField, BuildContext context) {
    var filePicker = formField.control.filePicker;
    return FormBuilderFilePicker(
      enabled: formField.hasEnabled() ? formField.enabled : true,
      name: formField.fieldId,
      // decoration: _getInputDecoration(input),
      onChanged: _onChanged,
      validator: _getValidators(context, formField.validators),
      previewImages: false,
      allowedExtensions: _getSimpleOptions(formField.fieldId),
      selector: Row(
        children: [
          const Icon(Icons.upload_file),
          Text(formField.label),
        ],
      ),
      // onFileLoading: (val) {
      //   debugPrint(val.toString());
      // },
      customFileViewerBuilder: customFileViewerBuilder,
      allowMultiple: filePicker.allowMultiple,
      withData: filePicker.withData,
      withReadStream: filePicker.withReadStream,
      type: FileType.values[filePicker.fileTypeIndex],
    );
  }

  FormBuilderCheckbox getCheckbox($forms.FormField formField,
      $fields.FieldValue field, BuildContext context) {
    var checkbox = formField.control.checkbox;
    return FormBuilderCheckbox(
      enabled: formField.hasEnabled() ? formField.enabled : true,
      name: formField.fieldId,
      initialValue: field.hasLogical() ? field.logical : null,
      title: _getRichText(formField.title),
      decoration: _getInputDecoration(formField),
      onChanged: _onChanged,
      validator: _getValidators(context, formField.validators),
      activeColor: hexToColor(checkbox.peculiar.activeColor),
      autofocus: checkbox.autofocus,
      checkColor: hexToColor(checkbox.peculiar.checkColor),
      selected: checkbox.selected,
      subtitle: _getRichText(checkbox.subtitle),
      tristate: checkbox.peculiar.tristate,
    );
  }

  Widget customFileViewerBuilder(
    List<PlatformFile>? files,
    FormFieldSetter<List<PlatformFile>> setter,
  ) {
    return ListView.separated(
      shrinkWrap: true,
      itemBuilder: (context, index) {
        final file = files![index];
        return ListTile(
          title: Text(file.name),
          trailing: IconButton(
            icon: const Icon(Icons.delete),
            onPressed: () {
              files.removeAt(index);
              setter.call([...files]);
            },
          ),
        );
      },
      separatorBuilder: (context, index) => const Divider(
        color: Colors.blueAccent,
      ),
      itemCount: files!.length,
    );
  }
}
