class Environment {
  static const String issuerUrl = "https://localhost:50000";
  /* ---- DEV ----- */
  static const String serviceHost = "localhost";
  static const String serviceSchema = "http";
  static const int servicePort = 8000;
  static bool insecure = false;
  /* ---- PROD ---- */
  /*  static const String serviceHost = "belfry-dashboard.camino.solutions";
  static const String serviceSchema = "https";
  static const int servicePort = 443;
  static bool insecure = false; */
}
