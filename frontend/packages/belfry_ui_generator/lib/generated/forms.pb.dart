///
//  Generated code. Do not modify.
//  source: forms.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'fields.pb.dart' as $0;

import 'forms.pbenum.dart';

export 'forms.pbenum.dart';

class Required extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Required', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Required._() : super();
  factory Required() => create();
  factory Required.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Required.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Required clone() => Required()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Required copyWith(void Function(Required) updates) => super.copyWith((message) => updates(message as Required)) as Required; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Required create() => Required._();
  Required createEmptyInstance() => create();
  static $pb.PbList<Required> createRepeated() => $pb.PbList<Required>();
  @$core.pragma('dart2js:noInline')
  static Required getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Required>(create);
  static Required? _defaultInstance;
}

class Equal extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Equal', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..aOM<$0.FieldValue>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value', subBuilder: $0.FieldValue.create)
    ..hasRequiredFields = false
  ;

  Equal._() : super();
  factory Equal({
    $0.FieldValue? value,
  }) {
    final _result = create();
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory Equal.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Equal.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Equal clone() => Equal()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Equal copyWith(void Function(Equal) updates) => super.copyWith((message) => updates(message as Equal)) as Equal; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Equal create() => Equal._();
  Equal createEmptyInstance() => create();
  static $pb.PbList<Equal> createRepeated() => $pb.PbList<Equal>();
  @$core.pragma('dart2js:noInline')
  static Equal getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Equal>(create);
  static Equal? _defaultInstance;

  @$pb.TagNumber(1)
  $0.FieldValue get value => $_getN(0);
  @$pb.TagNumber(1)
  set value($0.FieldValue v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasValue() => $_has(0);
  @$pb.TagNumber(1)
  void clearValue() => clearField(1);
  @$pb.TagNumber(1)
  $0.FieldValue ensureValue() => $_ensure(0);
}

class NotEqual extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NotEqual', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..aOM<$0.FieldValue>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value', subBuilder: $0.FieldValue.create)
    ..hasRequiredFields = false
  ;

  NotEqual._() : super();
  factory NotEqual({
    $0.FieldValue? value,
  }) {
    final _result = create();
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory NotEqual.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NotEqual.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NotEqual clone() => NotEqual()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NotEqual copyWith(void Function(NotEqual) updates) => super.copyWith((message) => updates(message as NotEqual)) as NotEqual; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NotEqual create() => NotEqual._();
  NotEqual createEmptyInstance() => create();
  static $pb.PbList<NotEqual> createRepeated() => $pb.PbList<NotEqual>();
  @$core.pragma('dart2js:noInline')
  static NotEqual getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NotEqual>(create);
  static NotEqual? _defaultInstance;

  @$pb.TagNumber(1)
  $0.FieldValue get value => $_getN(0);
  @$pb.TagNumber(1)
  set value($0.FieldValue v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasValue() => $_has(0);
  @$pb.TagNumber(1)
  void clearValue() => clearField(1);
  @$pb.TagNumber(1)
  $0.FieldValue ensureValue() => $_ensure(0);
}

class Min extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Min', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'min', $pb.PbFieldType.OD)
    ..aOB(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'inclusive')
    ..hasRequiredFields = false
  ;

  Min._() : super();
  factory Min({
    $core.double? min,
    $core.bool? inclusive,
  }) {
    final _result = create();
    if (min != null) {
      _result.min = min;
    }
    if (inclusive != null) {
      _result.inclusive = inclusive;
    }
    return _result;
  }
  factory Min.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Min.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Min clone() => Min()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Min copyWith(void Function(Min) updates) => super.copyWith((message) => updates(message as Min)) as Min; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Min create() => Min._();
  Min createEmptyInstance() => create();
  static $pb.PbList<Min> createRepeated() => $pb.PbList<Min>();
  @$core.pragma('dart2js:noInline')
  static Min getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Min>(create);
  static Min? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get min => $_getN(0);
  @$pb.TagNumber(1)
  set min($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMin() => $_has(0);
  @$pb.TagNumber(1)
  void clearMin() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get inclusive => $_getBF(1);
  @$pb.TagNumber(2)
  set inclusive($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasInclusive() => $_has(1);
  @$pb.TagNumber(2)
  void clearInclusive() => clearField(2);
}

class Max extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Max', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'max', $pb.PbFieldType.OD)
    ..aOB(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'inclusive')
    ..hasRequiredFields = false
  ;

  Max._() : super();
  factory Max({
    $core.double? max,
    $core.bool? inclusive,
  }) {
    final _result = create();
    if (max != null) {
      _result.max = max;
    }
    if (inclusive != null) {
      _result.inclusive = inclusive;
    }
    return _result;
  }
  factory Max.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Max.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Max clone() => Max()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Max copyWith(void Function(Max) updates) => super.copyWith((message) => updates(message as Max)) as Max; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Max create() => Max._();
  Max createEmptyInstance() => create();
  static $pb.PbList<Max> createRepeated() => $pb.PbList<Max>();
  @$core.pragma('dart2js:noInline')
  static Max getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Max>(create);
  static Max? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get max => $_getN(0);
  @$pb.TagNumber(1)
  set max($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMax() => $_has(0);
  @$pb.TagNumber(1)
  void clearMax() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get inclusive => $_getBF(1);
  @$pb.TagNumber(2)
  set inclusive($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasInclusive() => $_has(1);
  @$pb.TagNumber(2)
  void clearInclusive() => clearField(2);
}

class MinLength extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MinLength', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'minLength', $pb.PbFieldType.O3)
    ..aOB(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'allowEmpty')
    ..hasRequiredFields = false
  ;

  MinLength._() : super();
  factory MinLength({
    $core.int? minLength,
    $core.bool? allowEmpty,
  }) {
    final _result = create();
    if (minLength != null) {
      _result.minLength = minLength;
    }
    if (allowEmpty != null) {
      _result.allowEmpty = allowEmpty;
    }
    return _result;
  }
  factory MinLength.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MinLength.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MinLength clone() => MinLength()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MinLength copyWith(void Function(MinLength) updates) => super.copyWith((message) => updates(message as MinLength)) as MinLength; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MinLength create() => MinLength._();
  MinLength createEmptyInstance() => create();
  static $pb.PbList<MinLength> createRepeated() => $pb.PbList<MinLength>();
  @$core.pragma('dart2js:noInline')
  static MinLength getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MinLength>(create);
  static MinLength? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get minLength => $_getIZ(0);
  @$pb.TagNumber(1)
  set minLength($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMinLength() => $_has(0);
  @$pb.TagNumber(1)
  void clearMinLength() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get allowEmpty => $_getBF(1);
  @$pb.TagNumber(2)
  set allowEmpty($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAllowEmpty() => $_has(1);
  @$pb.TagNumber(2)
  void clearAllowEmpty() => clearField(2);
}

class MaxLength extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MaxLength', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxLength', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  MaxLength._() : super();
  factory MaxLength({
    $core.int? maxLength,
  }) {
    final _result = create();
    if (maxLength != null) {
      _result.maxLength = maxLength;
    }
    return _result;
  }
  factory MaxLength.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MaxLength.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MaxLength clone() => MaxLength()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MaxLength copyWith(void Function(MaxLength) updates) => super.copyWith((message) => updates(message as MaxLength)) as MaxLength; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MaxLength create() => MaxLength._();
  MaxLength createEmptyInstance() => create();
  static $pb.PbList<MaxLength> createRepeated() => $pb.PbList<MaxLength>();
  @$core.pragma('dart2js:noInline')
  static MaxLength getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MaxLength>(create);
  static MaxLength? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get maxLength => $_getIZ(0);
  @$pb.TagNumber(1)
  set maxLength($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMaxLength() => $_has(0);
  @$pb.TagNumber(1)
  void clearMaxLength() => clearField(1);
}

class Match extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Match', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pattern')
    ..hasRequiredFields = false
  ;

  Match._() : super();
  factory Match({
    $core.String? pattern,
  }) {
    final _result = create();
    if (pattern != null) {
      _result.pattern = pattern;
    }
    return _result;
  }
  factory Match.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Match.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Match clone() => Match()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Match copyWith(void Function(Match) updates) => super.copyWith((message) => updates(message as Match)) as Match; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Match create() => Match._();
  Match createEmptyInstance() => create();
  static $pb.PbList<Match> createRepeated() => $pb.PbList<Match>();
  @$core.pragma('dart2js:noInline')
  static Match getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Match>(create);
  static Match? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get pattern => $_getSZ(0);
  @$pb.TagNumber(1)
  set pattern($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPattern() => $_has(0);
  @$pb.TagNumber(1)
  void clearPattern() => clearField(1);
}

enum Validator_Type {
  required, 
  equal, 
  notEqual, 
  min, 
  max, 
  minLength, 
  maxLength, 
  match, 
  notSet
}

class Validator extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Validator_Type> _Validator_TypeByTag = {
    2 : Validator_Type.required,
    3 : Validator_Type.equal,
    4 : Validator_Type.notEqual,
    5 : Validator_Type.min,
    6 : Validator_Type.max,
    7 : Validator_Type.minLength,
    8 : Validator_Type.maxLength,
    9 : Validator_Type.match,
    0 : Validator_Type.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Validator', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..oo(0, [2, 3, 4, 5, 6, 7, 8, 9])
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'errorText')
    ..aOM<Required>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'required', subBuilder: Required.create)
    ..aOM<Equal>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'equal', subBuilder: Equal.create)
    ..aOM<NotEqual>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'notEqual', subBuilder: NotEqual.create)
    ..aOM<Min>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'min', subBuilder: Min.create)
    ..aOM<Max>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'max', subBuilder: Max.create)
    ..aOM<MinLength>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'minLength', subBuilder: MinLength.create)
    ..aOM<MaxLength>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxLength', subBuilder: MaxLength.create)
    ..aOM<Match>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'match', subBuilder: Match.create)
    ..hasRequiredFields = false
  ;

  Validator._() : super();
  factory Validator({
    $core.String? errorText,
    Required? required,
    Equal? equal,
    NotEqual? notEqual,
    Min? min,
    Max? max,
    MinLength? minLength,
    MaxLength? maxLength,
    Match? match,
  }) {
    final _result = create();
    if (errorText != null) {
      _result.errorText = errorText;
    }
    if (required != null) {
      _result.required = required;
    }
    if (equal != null) {
      _result.equal = equal;
    }
    if (notEqual != null) {
      _result.notEqual = notEqual;
    }
    if (min != null) {
      _result.min = min;
    }
    if (max != null) {
      _result.max = max;
    }
    if (minLength != null) {
      _result.minLength = minLength;
    }
    if (maxLength != null) {
      _result.maxLength = maxLength;
    }
    if (match != null) {
      _result.match = match;
    }
    return _result;
  }
  factory Validator.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Validator.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Validator clone() => Validator()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Validator copyWith(void Function(Validator) updates) => super.copyWith((message) => updates(message as Validator)) as Validator; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Validator create() => Validator._();
  Validator createEmptyInstance() => create();
  static $pb.PbList<Validator> createRepeated() => $pb.PbList<Validator>();
  @$core.pragma('dart2js:noInline')
  static Validator getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Validator>(create);
  static Validator? _defaultInstance;

  Validator_Type whichType() => _Validator_TypeByTag[$_whichOneof(0)]!;
  void clearType() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.String get errorText => $_getSZ(0);
  @$pb.TagNumber(1)
  set errorText($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasErrorText() => $_has(0);
  @$pb.TagNumber(1)
  void clearErrorText() => clearField(1);

  @$pb.TagNumber(2)
  Required get required => $_getN(1);
  @$pb.TagNumber(2)
  set required(Required v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasRequired() => $_has(1);
  @$pb.TagNumber(2)
  void clearRequired() => clearField(2);
  @$pb.TagNumber(2)
  Required ensureRequired() => $_ensure(1);

  @$pb.TagNumber(3)
  Equal get equal => $_getN(2);
  @$pb.TagNumber(3)
  set equal(Equal v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasEqual() => $_has(2);
  @$pb.TagNumber(3)
  void clearEqual() => clearField(3);
  @$pb.TagNumber(3)
  Equal ensureEqual() => $_ensure(2);

  @$pb.TagNumber(4)
  NotEqual get notEqual => $_getN(3);
  @$pb.TagNumber(4)
  set notEqual(NotEqual v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasNotEqual() => $_has(3);
  @$pb.TagNumber(4)
  void clearNotEqual() => clearField(4);
  @$pb.TagNumber(4)
  NotEqual ensureNotEqual() => $_ensure(3);

  @$pb.TagNumber(5)
  Min get min => $_getN(4);
  @$pb.TagNumber(5)
  set min(Min v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasMin() => $_has(4);
  @$pb.TagNumber(5)
  void clearMin() => clearField(5);
  @$pb.TagNumber(5)
  Min ensureMin() => $_ensure(4);

  @$pb.TagNumber(6)
  Max get max => $_getN(5);
  @$pb.TagNumber(6)
  set max(Max v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasMax() => $_has(5);
  @$pb.TagNumber(6)
  void clearMax() => clearField(6);
  @$pb.TagNumber(6)
  Max ensureMax() => $_ensure(5);

  @$pb.TagNumber(7)
  MinLength get minLength => $_getN(6);
  @$pb.TagNumber(7)
  set minLength(MinLength v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasMinLength() => $_has(6);
  @$pb.TagNumber(7)
  void clearMinLength() => clearField(7);
  @$pb.TagNumber(7)
  MinLength ensureMinLength() => $_ensure(6);

  @$pb.TagNumber(8)
  MaxLength get maxLength => $_getN(7);
  @$pb.TagNumber(8)
  set maxLength(MaxLength v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasMaxLength() => $_has(7);
  @$pb.TagNumber(8)
  void clearMaxLength() => clearField(8);
  @$pb.TagNumber(8)
  MaxLength ensureMaxLength() => $_ensure(7);

  @$pb.TagNumber(9)
  Match get match => $_getN(8);
  @$pb.TagNumber(9)
  set match(Match v) { setField(9, v); }
  @$pb.TagNumber(9)
  $core.bool hasMatch() => $_has(8);
  @$pb.TagNumber(9)
  void clearMatch() => clearField(9);
  @$pb.TagNumber(9)
  Match ensureMatch() => $_ensure(8);
}

class FormRow extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'FormRow', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..pc<Container>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'children', $pb.PbFieldType.PM, subBuilder: Container.create)
    ..hasRequiredFields = false
  ;

  FormRow._() : super();
  factory FormRow({
    $core.Iterable<Container>? children,
  }) {
    final _result = create();
    if (children != null) {
      _result.children.addAll(children);
    }
    return _result;
  }
  factory FormRow.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory FormRow.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  FormRow clone() => FormRow()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  FormRow copyWith(void Function(FormRow) updates) => super.copyWith((message) => updates(message as FormRow)) as FormRow; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static FormRow create() => FormRow._();
  FormRow createEmptyInstance() => create();
  static $pb.PbList<FormRow> createRepeated() => $pb.PbList<FormRow>();
  @$core.pragma('dart2js:noInline')
  static FormRow getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<FormRow>(create);
  static FormRow? _defaultInstance;

  @$pb.TagNumber(2)
  $core.List<Container> get children => $_getList(0);
}

class FormColumn extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'FormColumn', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..pc<Container>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'children', $pb.PbFieldType.PM, subBuilder: Container.create)
    ..hasRequiredFields = false
  ;

  FormColumn._() : super();
  factory FormColumn({
    $core.Iterable<Container>? children,
  }) {
    final _result = create();
    if (children != null) {
      _result.children.addAll(children);
    }
    return _result;
  }
  factory FormColumn.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory FormColumn.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  FormColumn clone() => FormColumn()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  FormColumn copyWith(void Function(FormColumn) updates) => super.copyWith((message) => updates(message as FormColumn)) as FormColumn; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static FormColumn create() => FormColumn._();
  FormColumn createEmptyInstance() => create();
  static $pb.PbList<FormColumn> createRepeated() => $pb.PbList<FormColumn>();
  @$core.pragma('dart2js:noInline')
  static FormColumn getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<FormColumn>(create);
  static FormColumn? _defaultInstance;

  @$pb.TagNumber(2)
  $core.List<Container> get children => $_getList(0);
}

enum FieldControl_Type {
  checkbox, 
  checkboxGroup, 
  choiceChip, 
  dateRangePicker, 
  dateTimePicker, 
  dropdown, 
  filterChip, 
  radioGroup, 
  rangeSlider, 
  segmentedControl, 
  slider, 
  switchField, 
  textField, 
  searchableDropdown, 
  colorPicker, 
  cupertinoDateTimePicker, 
  typeAhead, 
  touchSpin, 
  rating, 
  signaturePad, 
  filePicker, 
  chipsInput, 
  notSet
}

class FieldControl extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, FieldControl_Type> _FieldControl_TypeByTag = {
    1 : FieldControl_Type.checkbox,
    2 : FieldControl_Type.checkboxGroup,
    3 : FieldControl_Type.choiceChip,
    4 : FieldControl_Type.dateRangePicker,
    5 : FieldControl_Type.dateTimePicker,
    6 : FieldControl_Type.dropdown,
    7 : FieldControl_Type.filterChip,
    8 : FieldControl_Type.radioGroup,
    9 : FieldControl_Type.rangeSlider,
    10 : FieldControl_Type.segmentedControl,
    11 : FieldControl_Type.slider,
    12 : FieldControl_Type.switchField,
    13 : FieldControl_Type.textField,
    14 : FieldControl_Type.searchableDropdown,
    15 : FieldControl_Type.colorPicker,
    16 : FieldControl_Type.cupertinoDateTimePicker,
    17 : FieldControl_Type.typeAhead,
    18 : FieldControl_Type.touchSpin,
    19 : FieldControl_Type.rating,
    20 : FieldControl_Type.signaturePad,
    21 : FieldControl_Type.filePicker,
    22 : FieldControl_Type.chipsInput,
    0 : FieldControl_Type.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'FieldControl', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..oo(0, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22])
    ..aOM<Checkbox>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'checkbox', subBuilder: Checkbox.create)
    ..aOM<CheckboxGroup>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'checkboxGroup', subBuilder: CheckboxGroup.create)
    ..aOM<ChoiceChip>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'choiceChip', subBuilder: ChoiceChip.create)
    ..aOM<DateRangePicker>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'dateRangePicker', subBuilder: DateRangePicker.create)
    ..aOM<DateTimePicker>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'dateTimePicker', subBuilder: DateTimePicker.create)
    ..aOM<Dropdown>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'dropdown', subBuilder: Dropdown.create)
    ..aOM<FilterChip>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'filterChip', subBuilder: FilterChip.create)
    ..aOM<RadioGroup>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'radioGroup', subBuilder: RadioGroup.create)
    ..aOM<RangeSlider>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rangeSlider', subBuilder: RangeSlider.create)
    ..aOM<SegmentedControl>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'segmentedControl', subBuilder: SegmentedControl.create)
    ..aOM<Slider>(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'slider', subBuilder: Slider.create)
    ..aOM<Switch>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'switchField', subBuilder: Switch.create)
    ..aOM<TextField>(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'textField', subBuilder: TextField.create)
    ..aOM<SearchableDropdown>(14, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'searchableDropdown', subBuilder: SearchableDropdown.create)
    ..aOM<ColorPicker>(15, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'colorPicker', subBuilder: ColorPicker.create)
    ..aOM<CupertinoDateTimePicker>(16, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'cupertinoDateTimePicker', subBuilder: CupertinoDateTimePicker.create)
    ..aOM<TypeAhead>(17, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'typeAhead', subBuilder: TypeAhead.create)
    ..aOM<TouchSpin>(18, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'touchSpin', subBuilder: TouchSpin.create)
    ..aOM<Rating>(19, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rating', subBuilder: Rating.create)
    ..aOM<SignaturePad>(20, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'signaturePad', subBuilder: SignaturePad.create)
    ..aOM<FilePicker>(21, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'filePicker', subBuilder: FilePicker.create)
    ..aOM<ChipsInput>(22, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'chipsInput', subBuilder: ChipsInput.create)
    ..hasRequiredFields = false
  ;

  FieldControl._() : super();
  factory FieldControl({
    Checkbox? checkbox,
    CheckboxGroup? checkboxGroup,
    ChoiceChip? choiceChip,
    DateRangePicker? dateRangePicker,
    DateTimePicker? dateTimePicker,
    Dropdown? dropdown,
    FilterChip? filterChip,
    RadioGroup? radioGroup,
    RangeSlider? rangeSlider,
    SegmentedControl? segmentedControl,
    Slider? slider,
    Switch? switchField,
    TextField? textField,
    SearchableDropdown? searchableDropdown,
    ColorPicker? colorPicker,
    CupertinoDateTimePicker? cupertinoDateTimePicker,
    TypeAhead? typeAhead,
    TouchSpin? touchSpin,
    Rating? rating,
    SignaturePad? signaturePad,
    FilePicker? filePicker,
    ChipsInput? chipsInput,
  }) {
    final _result = create();
    if (checkbox != null) {
      _result.checkbox = checkbox;
    }
    if (checkboxGroup != null) {
      _result.checkboxGroup = checkboxGroup;
    }
    if (choiceChip != null) {
      _result.choiceChip = choiceChip;
    }
    if (dateRangePicker != null) {
      _result.dateRangePicker = dateRangePicker;
    }
    if (dateTimePicker != null) {
      _result.dateTimePicker = dateTimePicker;
    }
    if (dropdown != null) {
      _result.dropdown = dropdown;
    }
    if (filterChip != null) {
      _result.filterChip = filterChip;
    }
    if (radioGroup != null) {
      _result.radioGroup = radioGroup;
    }
    if (rangeSlider != null) {
      _result.rangeSlider = rangeSlider;
    }
    if (segmentedControl != null) {
      _result.segmentedControl = segmentedControl;
    }
    if (slider != null) {
      _result.slider = slider;
    }
    if (switchField != null) {
      _result.switchField = switchField;
    }
    if (textField != null) {
      _result.textField = textField;
    }
    if (searchableDropdown != null) {
      _result.searchableDropdown = searchableDropdown;
    }
    if (colorPicker != null) {
      _result.colorPicker = colorPicker;
    }
    if (cupertinoDateTimePicker != null) {
      _result.cupertinoDateTimePicker = cupertinoDateTimePicker;
    }
    if (typeAhead != null) {
      _result.typeAhead = typeAhead;
    }
    if (touchSpin != null) {
      _result.touchSpin = touchSpin;
    }
    if (rating != null) {
      _result.rating = rating;
    }
    if (signaturePad != null) {
      _result.signaturePad = signaturePad;
    }
    if (filePicker != null) {
      _result.filePicker = filePicker;
    }
    if (chipsInput != null) {
      _result.chipsInput = chipsInput;
    }
    return _result;
  }
  factory FieldControl.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory FieldControl.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  FieldControl clone() => FieldControl()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  FieldControl copyWith(void Function(FieldControl) updates) => super.copyWith((message) => updates(message as FieldControl)) as FieldControl; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static FieldControl create() => FieldControl._();
  FieldControl createEmptyInstance() => create();
  static $pb.PbList<FieldControl> createRepeated() => $pb.PbList<FieldControl>();
  @$core.pragma('dart2js:noInline')
  static FieldControl getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<FieldControl>(create);
  static FieldControl? _defaultInstance;

  FieldControl_Type whichType() => _FieldControl_TypeByTag[$_whichOneof(0)]!;
  void clearType() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  Checkbox get checkbox => $_getN(0);
  @$pb.TagNumber(1)
  set checkbox(Checkbox v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasCheckbox() => $_has(0);
  @$pb.TagNumber(1)
  void clearCheckbox() => clearField(1);
  @$pb.TagNumber(1)
  Checkbox ensureCheckbox() => $_ensure(0);

  @$pb.TagNumber(2)
  CheckboxGroup get checkboxGroup => $_getN(1);
  @$pb.TagNumber(2)
  set checkboxGroup(CheckboxGroup v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasCheckboxGroup() => $_has(1);
  @$pb.TagNumber(2)
  void clearCheckboxGroup() => clearField(2);
  @$pb.TagNumber(2)
  CheckboxGroup ensureCheckboxGroup() => $_ensure(1);

  @$pb.TagNumber(3)
  ChoiceChip get choiceChip => $_getN(2);
  @$pb.TagNumber(3)
  set choiceChip(ChoiceChip v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasChoiceChip() => $_has(2);
  @$pb.TagNumber(3)
  void clearChoiceChip() => clearField(3);
  @$pb.TagNumber(3)
  ChoiceChip ensureChoiceChip() => $_ensure(2);

  @$pb.TagNumber(4)
  DateRangePicker get dateRangePicker => $_getN(3);
  @$pb.TagNumber(4)
  set dateRangePicker(DateRangePicker v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasDateRangePicker() => $_has(3);
  @$pb.TagNumber(4)
  void clearDateRangePicker() => clearField(4);
  @$pb.TagNumber(4)
  DateRangePicker ensureDateRangePicker() => $_ensure(3);

  @$pb.TagNumber(5)
  DateTimePicker get dateTimePicker => $_getN(4);
  @$pb.TagNumber(5)
  set dateTimePicker(DateTimePicker v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasDateTimePicker() => $_has(4);
  @$pb.TagNumber(5)
  void clearDateTimePicker() => clearField(5);
  @$pb.TagNumber(5)
  DateTimePicker ensureDateTimePicker() => $_ensure(4);

  @$pb.TagNumber(6)
  Dropdown get dropdown => $_getN(5);
  @$pb.TagNumber(6)
  set dropdown(Dropdown v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasDropdown() => $_has(5);
  @$pb.TagNumber(6)
  void clearDropdown() => clearField(6);
  @$pb.TagNumber(6)
  Dropdown ensureDropdown() => $_ensure(5);

  @$pb.TagNumber(7)
  FilterChip get filterChip => $_getN(6);
  @$pb.TagNumber(7)
  set filterChip(FilterChip v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasFilterChip() => $_has(6);
  @$pb.TagNumber(7)
  void clearFilterChip() => clearField(7);
  @$pb.TagNumber(7)
  FilterChip ensureFilterChip() => $_ensure(6);

  @$pb.TagNumber(8)
  RadioGroup get radioGroup => $_getN(7);
  @$pb.TagNumber(8)
  set radioGroup(RadioGroup v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasRadioGroup() => $_has(7);
  @$pb.TagNumber(8)
  void clearRadioGroup() => clearField(8);
  @$pb.TagNumber(8)
  RadioGroup ensureRadioGroup() => $_ensure(7);

  @$pb.TagNumber(9)
  RangeSlider get rangeSlider => $_getN(8);
  @$pb.TagNumber(9)
  set rangeSlider(RangeSlider v) { setField(9, v); }
  @$pb.TagNumber(9)
  $core.bool hasRangeSlider() => $_has(8);
  @$pb.TagNumber(9)
  void clearRangeSlider() => clearField(9);
  @$pb.TagNumber(9)
  RangeSlider ensureRangeSlider() => $_ensure(8);

  @$pb.TagNumber(10)
  SegmentedControl get segmentedControl => $_getN(9);
  @$pb.TagNumber(10)
  set segmentedControl(SegmentedControl v) { setField(10, v); }
  @$pb.TagNumber(10)
  $core.bool hasSegmentedControl() => $_has(9);
  @$pb.TagNumber(10)
  void clearSegmentedControl() => clearField(10);
  @$pb.TagNumber(10)
  SegmentedControl ensureSegmentedControl() => $_ensure(9);

  @$pb.TagNumber(11)
  Slider get slider => $_getN(10);
  @$pb.TagNumber(11)
  set slider(Slider v) { setField(11, v); }
  @$pb.TagNumber(11)
  $core.bool hasSlider() => $_has(10);
  @$pb.TagNumber(11)
  void clearSlider() => clearField(11);
  @$pb.TagNumber(11)
  Slider ensureSlider() => $_ensure(10);

  @$pb.TagNumber(12)
  Switch get switchField => $_getN(11);
  @$pb.TagNumber(12)
  set switchField(Switch v) { setField(12, v); }
  @$pb.TagNumber(12)
  $core.bool hasSwitchField() => $_has(11);
  @$pb.TagNumber(12)
  void clearSwitchField() => clearField(12);
  @$pb.TagNumber(12)
  Switch ensureSwitchField() => $_ensure(11);

  @$pb.TagNumber(13)
  TextField get textField => $_getN(12);
  @$pb.TagNumber(13)
  set textField(TextField v) { setField(13, v); }
  @$pb.TagNumber(13)
  $core.bool hasTextField() => $_has(12);
  @$pb.TagNumber(13)
  void clearTextField() => clearField(13);
  @$pb.TagNumber(13)
  TextField ensureTextField() => $_ensure(12);

  @$pb.TagNumber(14)
  SearchableDropdown get searchableDropdown => $_getN(13);
  @$pb.TagNumber(14)
  set searchableDropdown(SearchableDropdown v) { setField(14, v); }
  @$pb.TagNumber(14)
  $core.bool hasSearchableDropdown() => $_has(13);
  @$pb.TagNumber(14)
  void clearSearchableDropdown() => clearField(14);
  @$pb.TagNumber(14)
  SearchableDropdown ensureSearchableDropdown() => $_ensure(13);

  @$pb.TagNumber(15)
  ColorPicker get colorPicker => $_getN(14);
  @$pb.TagNumber(15)
  set colorPicker(ColorPicker v) { setField(15, v); }
  @$pb.TagNumber(15)
  $core.bool hasColorPicker() => $_has(14);
  @$pb.TagNumber(15)
  void clearColorPicker() => clearField(15);
  @$pb.TagNumber(15)
  ColorPicker ensureColorPicker() => $_ensure(14);

  @$pb.TagNumber(16)
  CupertinoDateTimePicker get cupertinoDateTimePicker => $_getN(15);
  @$pb.TagNumber(16)
  set cupertinoDateTimePicker(CupertinoDateTimePicker v) { setField(16, v); }
  @$pb.TagNumber(16)
  $core.bool hasCupertinoDateTimePicker() => $_has(15);
  @$pb.TagNumber(16)
  void clearCupertinoDateTimePicker() => clearField(16);
  @$pb.TagNumber(16)
  CupertinoDateTimePicker ensureCupertinoDateTimePicker() => $_ensure(15);

  @$pb.TagNumber(17)
  TypeAhead get typeAhead => $_getN(16);
  @$pb.TagNumber(17)
  set typeAhead(TypeAhead v) { setField(17, v); }
  @$pb.TagNumber(17)
  $core.bool hasTypeAhead() => $_has(16);
  @$pb.TagNumber(17)
  void clearTypeAhead() => clearField(17);
  @$pb.TagNumber(17)
  TypeAhead ensureTypeAhead() => $_ensure(16);

  @$pb.TagNumber(18)
  TouchSpin get touchSpin => $_getN(17);
  @$pb.TagNumber(18)
  set touchSpin(TouchSpin v) { setField(18, v); }
  @$pb.TagNumber(18)
  $core.bool hasTouchSpin() => $_has(17);
  @$pb.TagNumber(18)
  void clearTouchSpin() => clearField(18);
  @$pb.TagNumber(18)
  TouchSpin ensureTouchSpin() => $_ensure(17);

  @$pb.TagNumber(19)
  Rating get rating => $_getN(18);
  @$pb.TagNumber(19)
  set rating(Rating v) { setField(19, v); }
  @$pb.TagNumber(19)
  $core.bool hasRating() => $_has(18);
  @$pb.TagNumber(19)
  void clearRating() => clearField(19);
  @$pb.TagNumber(19)
  Rating ensureRating() => $_ensure(18);

  @$pb.TagNumber(20)
  SignaturePad get signaturePad => $_getN(19);
  @$pb.TagNumber(20)
  set signaturePad(SignaturePad v) { setField(20, v); }
  @$pb.TagNumber(20)
  $core.bool hasSignaturePad() => $_has(19);
  @$pb.TagNumber(20)
  void clearSignaturePad() => clearField(20);
  @$pb.TagNumber(20)
  SignaturePad ensureSignaturePad() => $_ensure(19);

  @$pb.TagNumber(21)
  FilePicker get filePicker => $_getN(20);
  @$pb.TagNumber(21)
  set filePicker(FilePicker v) { setField(21, v); }
  @$pb.TagNumber(21)
  $core.bool hasFilePicker() => $_has(20);
  @$pb.TagNumber(21)
  void clearFilePicker() => clearField(21);
  @$pb.TagNumber(21)
  FilePicker ensureFilePicker() => $_ensure(20);

  @$pb.TagNumber(22)
  ChipsInput get chipsInput => $_getN(21);
  @$pb.TagNumber(22)
  set chipsInput(ChipsInput v) { setField(22, v); }
  @$pb.TagNumber(22)
  $core.bool hasChipsInput() => $_has(21);
  @$pb.TagNumber(22)
  void clearChipsInput() => clearField(22);
  @$pb.TagNumber(22)
  ChipsInput ensureChipsInput() => $_ensure(21);
}

class Checkbox extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Checkbox', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..aOM<CheckboxPeculiar>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'peculiar', subBuilder: CheckboxPeculiar.create)
    ..aOB(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'autofocus')
    ..aOB(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'selected')
    ..aOM<Text>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'subtitle', subBuilder: Text.create)
    ..hasRequiredFields = false
  ;

  Checkbox._() : super();
  factory Checkbox({
    CheckboxPeculiar? peculiar,
    $core.bool? autofocus,
    $core.bool? selected,
    Text? subtitle,
  }) {
    final _result = create();
    if (peculiar != null) {
      _result.peculiar = peculiar;
    }
    if (autofocus != null) {
      _result.autofocus = autofocus;
    }
    if (selected != null) {
      _result.selected = selected;
    }
    if (subtitle != null) {
      _result.subtitle = subtitle;
    }
    return _result;
  }
  factory Checkbox.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Checkbox.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Checkbox clone() => Checkbox()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Checkbox copyWith(void Function(Checkbox) updates) => super.copyWith((message) => updates(message as Checkbox)) as Checkbox; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Checkbox create() => Checkbox._();
  Checkbox createEmptyInstance() => create();
  static $pb.PbList<Checkbox> createRepeated() => $pb.PbList<Checkbox>();
  @$core.pragma('dart2js:noInline')
  static Checkbox getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Checkbox>(create);
  static Checkbox? _defaultInstance;

  @$pb.TagNumber(1)
  CheckboxPeculiar get peculiar => $_getN(0);
  @$pb.TagNumber(1)
  set peculiar(CheckboxPeculiar v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasPeculiar() => $_has(0);
  @$pb.TagNumber(1)
  void clearPeculiar() => clearField(1);
  @$pb.TagNumber(1)
  CheckboxPeculiar ensurePeculiar() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.bool get autofocus => $_getBF(1);
  @$pb.TagNumber(2)
  set autofocus($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAutofocus() => $_has(1);
  @$pb.TagNumber(2)
  void clearAutofocus() => clearField(2);

  @$pb.TagNumber(3)
  $core.bool get selected => $_getBF(2);
  @$pb.TagNumber(3)
  set selected($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasSelected() => $_has(2);
  @$pb.TagNumber(3)
  void clearSelected() => clearField(3);

  @$pb.TagNumber(4)
  Text get subtitle => $_getN(3);
  @$pb.TagNumber(4)
  set subtitle(Text v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasSubtitle() => $_has(3);
  @$pb.TagNumber(4)
  void clearSubtitle() => clearField(4);
  @$pb.TagNumber(4)
  Text ensureSubtitle() => $_ensure(3);
}

enum CheckboxGroup_Orientation {
  wrap, 
  direction, 
  notSet
}

class CheckboxGroup extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, CheckboxGroup_Orientation> _CheckboxGroup_OrientationByTag = {
    4 : CheckboxGroup_Orientation.wrap,
    5 : CheckboxGroup_Orientation.direction,
    0 : CheckboxGroup_Orientation.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CheckboxGroup', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..oo(0, [4, 5])
    ..aOM<CheckboxPeculiar>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'peculiar', subBuilder: CheckboxPeculiar.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'focusColor')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'hoverColor')
    ..aOM<Wrap>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'wrap', subBuilder: Wrap.create)
    ..e<Direction>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'direction', $pb.PbFieldType.OE, defaultOrMaker: Direction.horizontal, valueOf: Direction.valueOf, enumValues: Direction.values)
    ..hasRequiredFields = false
  ;

  CheckboxGroup._() : super();
  factory CheckboxGroup({
    CheckboxPeculiar? peculiar,
    $core.String? focusColor,
    $core.String? hoverColor,
    Wrap? wrap,
    Direction? direction,
  }) {
    final _result = create();
    if (peculiar != null) {
      _result.peculiar = peculiar;
    }
    if (focusColor != null) {
      _result.focusColor = focusColor;
    }
    if (hoverColor != null) {
      _result.hoverColor = hoverColor;
    }
    if (wrap != null) {
      _result.wrap = wrap;
    }
    if (direction != null) {
      _result.direction = direction;
    }
    return _result;
  }
  factory CheckboxGroup.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CheckboxGroup.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CheckboxGroup clone() => CheckboxGroup()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CheckboxGroup copyWith(void Function(CheckboxGroup) updates) => super.copyWith((message) => updates(message as CheckboxGroup)) as CheckboxGroup; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CheckboxGroup create() => CheckboxGroup._();
  CheckboxGroup createEmptyInstance() => create();
  static $pb.PbList<CheckboxGroup> createRepeated() => $pb.PbList<CheckboxGroup>();
  @$core.pragma('dart2js:noInline')
  static CheckboxGroup getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CheckboxGroup>(create);
  static CheckboxGroup? _defaultInstance;

  CheckboxGroup_Orientation whichOrientation() => _CheckboxGroup_OrientationByTag[$_whichOneof(0)]!;
  void clearOrientation() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  CheckboxPeculiar get peculiar => $_getN(0);
  @$pb.TagNumber(1)
  set peculiar(CheckboxPeculiar v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasPeculiar() => $_has(0);
  @$pb.TagNumber(1)
  void clearPeculiar() => clearField(1);
  @$pb.TagNumber(1)
  CheckboxPeculiar ensurePeculiar() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.String get focusColor => $_getSZ(1);
  @$pb.TagNumber(2)
  set focusColor($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasFocusColor() => $_has(1);
  @$pb.TagNumber(2)
  void clearFocusColor() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get hoverColor => $_getSZ(2);
  @$pb.TagNumber(3)
  set hoverColor($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasHoverColor() => $_has(2);
  @$pb.TagNumber(3)
  void clearHoverColor() => clearField(3);

  @$pb.TagNumber(4)
  Wrap get wrap => $_getN(3);
  @$pb.TagNumber(4)
  set wrap(Wrap v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasWrap() => $_has(3);
  @$pb.TagNumber(4)
  void clearWrap() => clearField(4);
  @$pb.TagNumber(4)
  Wrap ensureWrap() => $_ensure(3);

  @$pb.TagNumber(5)
  Direction get direction => $_getN(4);
  @$pb.TagNumber(5)
  set direction(Direction v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasDirection() => $_has(4);
  @$pb.TagNumber(5)
  void clearDirection() => clearField(5);
}

class ChoiceChip extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ChoiceChip', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  ChoiceChip._() : super();
  factory ChoiceChip() => create();
  factory ChoiceChip.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ChoiceChip.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ChoiceChip clone() => ChoiceChip()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ChoiceChip copyWith(void Function(ChoiceChip) updates) => super.copyWith((message) => updates(message as ChoiceChip)) as ChoiceChip; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ChoiceChip create() => ChoiceChip._();
  ChoiceChip createEmptyInstance() => create();
  static $pb.PbList<ChoiceChip> createRepeated() => $pb.PbList<ChoiceChip>();
  @$core.pragma('dart2js:noInline')
  static ChoiceChip getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ChoiceChip>(create);
  static ChoiceChip? _defaultInstance;
}

class DateRangePicker extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DateRangePicker', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  DateRangePicker._() : super();
  factory DateRangePicker() => create();
  factory DateRangePicker.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DateRangePicker.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DateRangePicker clone() => DateRangePicker()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DateRangePicker copyWith(void Function(DateRangePicker) updates) => super.copyWith((message) => updates(message as DateRangePicker)) as DateRangePicker; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DateRangePicker create() => DateRangePicker._();
  DateRangePicker createEmptyInstance() => create();
  static $pb.PbList<DateRangePicker> createRepeated() => $pb.PbList<DateRangePicker>();
  @$core.pragma('dart2js:noInline')
  static DateRangePicker getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DateRangePicker>(create);
  static DateRangePicker? _defaultInstance;
}

class DateTimePicker extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DateTimePicker', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  DateTimePicker._() : super();
  factory DateTimePicker() => create();
  factory DateTimePicker.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DateTimePicker.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DateTimePicker clone() => DateTimePicker()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DateTimePicker copyWith(void Function(DateTimePicker) updates) => super.copyWith((message) => updates(message as DateTimePicker)) as DateTimePicker; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DateTimePicker create() => DateTimePicker._();
  DateTimePicker createEmptyInstance() => create();
  static $pb.PbList<DateTimePicker> createRepeated() => $pb.PbList<DateTimePicker>();
  @$core.pragma('dart2js:noInline')
  static DateTimePicker getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DateTimePicker>(create);
  static DateTimePicker? _defaultInstance;
}

class Dropdown extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Dropdown', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Dropdown._() : super();
  factory Dropdown() => create();
  factory Dropdown.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Dropdown.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Dropdown clone() => Dropdown()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Dropdown copyWith(void Function(Dropdown) updates) => super.copyWith((message) => updates(message as Dropdown)) as Dropdown; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Dropdown create() => Dropdown._();
  Dropdown createEmptyInstance() => create();
  static $pb.PbList<Dropdown> createRepeated() => $pb.PbList<Dropdown>();
  @$core.pragma('dart2js:noInline')
  static Dropdown getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Dropdown>(create);
  static Dropdown? _defaultInstance;
}

class FilterChip extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'FilterChip', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  FilterChip._() : super();
  factory FilterChip() => create();
  factory FilterChip.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory FilterChip.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  FilterChip clone() => FilterChip()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  FilterChip copyWith(void Function(FilterChip) updates) => super.copyWith((message) => updates(message as FilterChip)) as FilterChip; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static FilterChip create() => FilterChip._();
  FilterChip createEmptyInstance() => create();
  static $pb.PbList<FilterChip> createRepeated() => $pb.PbList<FilterChip>();
  @$core.pragma('dart2js:noInline')
  static FilterChip getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<FilterChip>(create);
  static FilterChip? _defaultInstance;
}

class RadioGroup extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RadioGroup', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  RadioGroup._() : super();
  factory RadioGroup() => create();
  factory RadioGroup.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RadioGroup.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RadioGroup clone() => RadioGroup()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RadioGroup copyWith(void Function(RadioGroup) updates) => super.copyWith((message) => updates(message as RadioGroup)) as RadioGroup; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RadioGroup create() => RadioGroup._();
  RadioGroup createEmptyInstance() => create();
  static $pb.PbList<RadioGroup> createRepeated() => $pb.PbList<RadioGroup>();
  @$core.pragma('dart2js:noInline')
  static RadioGroup getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RadioGroup>(create);
  static RadioGroup? _defaultInstance;
}

class RangeSlider extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RangeSlider', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  RangeSlider._() : super();
  factory RangeSlider() => create();
  factory RangeSlider.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RangeSlider.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RangeSlider clone() => RangeSlider()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RangeSlider copyWith(void Function(RangeSlider) updates) => super.copyWith((message) => updates(message as RangeSlider)) as RangeSlider; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RangeSlider create() => RangeSlider._();
  RangeSlider createEmptyInstance() => create();
  static $pb.PbList<RangeSlider> createRepeated() => $pb.PbList<RangeSlider>();
  @$core.pragma('dart2js:noInline')
  static RangeSlider getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RangeSlider>(create);
  static RangeSlider? _defaultInstance;
}

class SegmentedControl extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SegmentedControl', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  SegmentedControl._() : super();
  factory SegmentedControl() => create();
  factory SegmentedControl.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SegmentedControl.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SegmentedControl clone() => SegmentedControl()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SegmentedControl copyWith(void Function(SegmentedControl) updates) => super.copyWith((message) => updates(message as SegmentedControl)) as SegmentedControl; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SegmentedControl create() => SegmentedControl._();
  SegmentedControl createEmptyInstance() => create();
  static $pb.PbList<SegmentedControl> createRepeated() => $pb.PbList<SegmentedControl>();
  @$core.pragma('dart2js:noInline')
  static SegmentedControl getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SegmentedControl>(create);
  static SegmentedControl? _defaultInstance;
}

class Slider extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Slider', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Slider._() : super();
  factory Slider() => create();
  factory Slider.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Slider.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Slider clone() => Slider()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Slider copyWith(void Function(Slider) updates) => super.copyWith((message) => updates(message as Slider)) as Slider; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Slider create() => Slider._();
  Slider createEmptyInstance() => create();
  static $pb.PbList<Slider> createRepeated() => $pb.PbList<Slider>();
  @$core.pragma('dart2js:noInline')
  static Slider getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Slider>(create);
  static Slider? _defaultInstance;
}

class Switch extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Switch', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Switch._() : super();
  factory Switch() => create();
  factory Switch.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Switch.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Switch clone() => Switch()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Switch copyWith(void Function(Switch) updates) => super.copyWith((message) => updates(message as Switch)) as Switch; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Switch create() => Switch._();
  Switch createEmptyInstance() => create();
  static $pb.PbList<Switch> createRepeated() => $pb.PbList<Switch>();
  @$core.pragma('dart2js:noInline')
  static Switch getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Switch>(create);
  static Switch? _defaultInstance;
}

class TextField extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TextField', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxLines', $pb.PbFieldType.O3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'minLines', $pb.PbFieldType.O3)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'textStyle', $pb.PbFieldType.O3)
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxLength', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  TextField._() : super();
  factory TextField({
    $core.int? maxLines,
    $core.int? minLines,
    $core.int? textStyle,
    $core.int? maxLength,
  }) {
    final _result = create();
    if (maxLines != null) {
      _result.maxLines = maxLines;
    }
    if (minLines != null) {
      _result.minLines = minLines;
    }
    if (textStyle != null) {
      _result.textStyle = textStyle;
    }
    if (maxLength != null) {
      _result.maxLength = maxLength;
    }
    return _result;
  }
  factory TextField.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TextField.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TextField clone() => TextField()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TextField copyWith(void Function(TextField) updates) => super.copyWith((message) => updates(message as TextField)) as TextField; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TextField create() => TextField._();
  TextField createEmptyInstance() => create();
  static $pb.PbList<TextField> createRepeated() => $pb.PbList<TextField>();
  @$core.pragma('dart2js:noInline')
  static TextField getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TextField>(create);
  static TextField? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get maxLines => $_getIZ(0);
  @$pb.TagNumber(1)
  set maxLines($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMaxLines() => $_has(0);
  @$pb.TagNumber(1)
  void clearMaxLines() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get minLines => $_getIZ(1);
  @$pb.TagNumber(2)
  set minLines($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMinLines() => $_has(1);
  @$pb.TagNumber(2)
  void clearMinLines() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get textStyle => $_getIZ(2);
  @$pb.TagNumber(3)
  set textStyle($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasTextStyle() => $_has(2);
  @$pb.TagNumber(3)
  void clearTextStyle() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get maxLength => $_getIZ(3);
  @$pb.TagNumber(4)
  set maxLength($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasMaxLength() => $_has(3);
  @$pb.TagNumber(4)
  void clearMaxLength() => clearField(4);
}

class SearchableDropdown extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SearchableDropdown', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  SearchableDropdown._() : super();
  factory SearchableDropdown() => create();
  factory SearchableDropdown.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SearchableDropdown.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SearchableDropdown clone() => SearchableDropdown()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SearchableDropdown copyWith(void Function(SearchableDropdown) updates) => super.copyWith((message) => updates(message as SearchableDropdown)) as SearchableDropdown; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SearchableDropdown create() => SearchableDropdown._();
  SearchableDropdown createEmptyInstance() => create();
  static $pb.PbList<SearchableDropdown> createRepeated() => $pb.PbList<SearchableDropdown>();
  @$core.pragma('dart2js:noInline')
  static SearchableDropdown getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SearchableDropdown>(create);
  static SearchableDropdown? _defaultInstance;
}

class ColorPicker extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ColorPicker', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  ColorPicker._() : super();
  factory ColorPicker() => create();
  factory ColorPicker.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ColorPicker.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ColorPicker clone() => ColorPicker()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ColorPicker copyWith(void Function(ColorPicker) updates) => super.copyWith((message) => updates(message as ColorPicker)) as ColorPicker; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ColorPicker create() => ColorPicker._();
  ColorPicker createEmptyInstance() => create();
  static $pb.PbList<ColorPicker> createRepeated() => $pb.PbList<ColorPicker>();
  @$core.pragma('dart2js:noInline')
  static ColorPicker getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ColorPicker>(create);
  static ColorPicker? _defaultInstance;
}

class CupertinoDateTimePicker extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CupertinoDateTimePicker', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  CupertinoDateTimePicker._() : super();
  factory CupertinoDateTimePicker() => create();
  factory CupertinoDateTimePicker.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CupertinoDateTimePicker.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CupertinoDateTimePicker clone() => CupertinoDateTimePicker()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CupertinoDateTimePicker copyWith(void Function(CupertinoDateTimePicker) updates) => super.copyWith((message) => updates(message as CupertinoDateTimePicker)) as CupertinoDateTimePicker; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CupertinoDateTimePicker create() => CupertinoDateTimePicker._();
  CupertinoDateTimePicker createEmptyInstance() => create();
  static $pb.PbList<CupertinoDateTimePicker> createRepeated() => $pb.PbList<CupertinoDateTimePicker>();
  @$core.pragma('dart2js:noInline')
  static CupertinoDateTimePicker getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CupertinoDateTimePicker>(create);
  static CupertinoDateTimePicker? _defaultInstance;
}

class TypeAhead extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TypeAhead', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  TypeAhead._() : super();
  factory TypeAhead() => create();
  factory TypeAhead.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TypeAhead.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TypeAhead clone() => TypeAhead()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TypeAhead copyWith(void Function(TypeAhead) updates) => super.copyWith((message) => updates(message as TypeAhead)) as TypeAhead; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TypeAhead create() => TypeAhead._();
  TypeAhead createEmptyInstance() => create();
  static $pb.PbList<TypeAhead> createRepeated() => $pb.PbList<TypeAhead>();
  @$core.pragma('dart2js:noInline')
  static TypeAhead getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TypeAhead>(create);
  static TypeAhead? _defaultInstance;
}

class TouchSpin extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TouchSpin', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  TouchSpin._() : super();
  factory TouchSpin() => create();
  factory TouchSpin.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TouchSpin.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TouchSpin clone() => TouchSpin()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TouchSpin copyWith(void Function(TouchSpin) updates) => super.copyWith((message) => updates(message as TouchSpin)) as TouchSpin; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TouchSpin create() => TouchSpin._();
  TouchSpin createEmptyInstance() => create();
  static $pb.PbList<TouchSpin> createRepeated() => $pb.PbList<TouchSpin>();
  @$core.pragma('dart2js:noInline')
  static TouchSpin getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TouchSpin>(create);
  static TouchSpin? _defaultInstance;
}

class Rating extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Rating', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Rating._() : super();
  factory Rating() => create();
  factory Rating.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Rating.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Rating clone() => Rating()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Rating copyWith(void Function(Rating) updates) => super.copyWith((message) => updates(message as Rating)) as Rating; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Rating create() => Rating._();
  Rating createEmptyInstance() => create();
  static $pb.PbList<Rating> createRepeated() => $pb.PbList<Rating>();
  @$core.pragma('dart2js:noInline')
  static Rating getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Rating>(create);
  static Rating? _defaultInstance;
}

class SignaturePad extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SignaturePad', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  SignaturePad._() : super();
  factory SignaturePad() => create();
  factory SignaturePad.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SignaturePad.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SignaturePad clone() => SignaturePad()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SignaturePad copyWith(void Function(SignaturePad) updates) => super.copyWith((message) => updates(message as SignaturePad)) as SignaturePad; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SignaturePad create() => SignaturePad._();
  SignaturePad createEmptyInstance() => create();
  static $pb.PbList<SignaturePad> createRepeated() => $pb.PbList<SignaturePad>();
  @$core.pragma('dart2js:noInline')
  static SignaturePad getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SignaturePad>(create);
  static SignaturePad? _defaultInstance;
}

class FilePicker extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'FilePicker', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'allowMultiple')
    ..aOB(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'withData')
    ..aOB(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'withReadStream')
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxFiles', $pb.PbFieldType.O3)
    ..aOB(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'previewImages')
    ..a<$core.int>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'fileTypeIndex', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  FilePicker._() : super();
  factory FilePicker({
    $core.bool? allowMultiple,
    $core.bool? withData,
    $core.bool? withReadStream,
    $core.int? maxFiles,
    $core.bool? previewImages,
    $core.int? fileTypeIndex,
  }) {
    final _result = create();
    if (allowMultiple != null) {
      _result.allowMultiple = allowMultiple;
    }
    if (withData != null) {
      _result.withData = withData;
    }
    if (withReadStream != null) {
      _result.withReadStream = withReadStream;
    }
    if (maxFiles != null) {
      _result.maxFiles = maxFiles;
    }
    if (previewImages != null) {
      _result.previewImages = previewImages;
    }
    if (fileTypeIndex != null) {
      _result.fileTypeIndex = fileTypeIndex;
    }
    return _result;
  }
  factory FilePicker.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory FilePicker.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  FilePicker clone() => FilePicker()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  FilePicker copyWith(void Function(FilePicker) updates) => super.copyWith((message) => updates(message as FilePicker)) as FilePicker; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static FilePicker create() => FilePicker._();
  FilePicker createEmptyInstance() => create();
  static $pb.PbList<FilePicker> createRepeated() => $pb.PbList<FilePicker>();
  @$core.pragma('dart2js:noInline')
  static FilePicker getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<FilePicker>(create);
  static FilePicker? _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get allowMultiple => $_getBF(0);
  @$pb.TagNumber(1)
  set allowMultiple($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAllowMultiple() => $_has(0);
  @$pb.TagNumber(1)
  void clearAllowMultiple() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get withData => $_getBF(1);
  @$pb.TagNumber(2)
  set withData($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasWithData() => $_has(1);
  @$pb.TagNumber(2)
  void clearWithData() => clearField(2);

  @$pb.TagNumber(3)
  $core.bool get withReadStream => $_getBF(2);
  @$pb.TagNumber(3)
  set withReadStream($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasWithReadStream() => $_has(2);
  @$pb.TagNumber(3)
  void clearWithReadStream() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get maxFiles => $_getIZ(3);
  @$pb.TagNumber(4)
  set maxFiles($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasMaxFiles() => $_has(3);
  @$pb.TagNumber(4)
  void clearMaxFiles() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get previewImages => $_getBF(4);
  @$pb.TagNumber(5)
  set previewImages($core.bool v) { $_setBool(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPreviewImages() => $_has(4);
  @$pb.TagNumber(5)
  void clearPreviewImages() => clearField(5);

  @$pb.TagNumber(6)
  $core.int get fileTypeIndex => $_getIZ(5);
  @$pb.TagNumber(6)
  set fileTypeIndex($core.int v) { $_setSignedInt32(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasFileTypeIndex() => $_has(5);
  @$pb.TagNumber(6)
  void clearFileTypeIndex() => clearField(6);
}

class ChipsInput extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ChipsInput', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxChips', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  ChipsInput._() : super();
  factory ChipsInput({
    $core.int? maxChips,
  }) {
    final _result = create();
    if (maxChips != null) {
      _result.maxChips = maxChips;
    }
    return _result;
  }
  factory ChipsInput.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ChipsInput.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ChipsInput clone() => ChipsInput()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ChipsInput copyWith(void Function(ChipsInput) updates) => super.copyWith((message) => updates(message as ChipsInput)) as ChipsInput; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ChipsInput create() => ChipsInput._();
  ChipsInput createEmptyInstance() => create();
  static $pb.PbList<ChipsInput> createRepeated() => $pb.PbList<ChipsInput>();
  @$core.pragma('dart2js:noInline')
  static ChipsInput getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ChipsInput>(create);
  static ChipsInput? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get maxChips => $_getIZ(0);
  @$pb.TagNumber(1)
  set maxChips($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMaxChips() => $_has(0);
  @$pb.TagNumber(1)
  void clearMaxChips() => clearField(1);
}

class CheckboxPeculiar extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CheckboxPeculiar', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'activeColor')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'checkColor')
    ..aOB(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tristate')
    ..hasRequiredFields = false
  ;

  CheckboxPeculiar._() : super();
  factory CheckboxPeculiar({
    $core.String? activeColor,
    $core.String? checkColor,
    $core.bool? tristate,
  }) {
    final _result = create();
    if (activeColor != null) {
      _result.activeColor = activeColor;
    }
    if (checkColor != null) {
      _result.checkColor = checkColor;
    }
    if (tristate != null) {
      _result.tristate = tristate;
    }
    return _result;
  }
  factory CheckboxPeculiar.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CheckboxPeculiar.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CheckboxPeculiar clone() => CheckboxPeculiar()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CheckboxPeculiar copyWith(void Function(CheckboxPeculiar) updates) => super.copyWith((message) => updates(message as CheckboxPeculiar)) as CheckboxPeculiar; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CheckboxPeculiar create() => CheckboxPeculiar._();
  CheckboxPeculiar createEmptyInstance() => create();
  static $pb.PbList<CheckboxPeculiar> createRepeated() => $pb.PbList<CheckboxPeculiar>();
  @$core.pragma('dart2js:noInline')
  static CheckboxPeculiar getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CheckboxPeculiar>(create);
  static CheckboxPeculiar? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get activeColor => $_getSZ(0);
  @$pb.TagNumber(1)
  set activeColor($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasActiveColor() => $_has(0);
  @$pb.TagNumber(1)
  void clearActiveColor() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get checkColor => $_getSZ(1);
  @$pb.TagNumber(2)
  set checkColor($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCheckColor() => $_has(1);
  @$pb.TagNumber(2)
  void clearCheckColor() => clearField(2);

  @$pb.TagNumber(3)
  $core.bool get tristate => $_getBF(2);
  @$pb.TagNumber(3)
  set tristate($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasTristate() => $_has(2);
  @$pb.TagNumber(3)
  void clearTristate() => clearField(3);
}

class Wrap extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Wrap', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..e<Direction>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'direction', $pb.PbFieldType.OE, defaultOrMaker: Direction.horizontal, valueOf: Direction.valueOf, enumValues: Direction.values)
    ..e<Alignment>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'alignment', $pb.PbFieldType.OE, defaultOrMaker: Alignment.start, valueOf: Alignment.valueOf, enumValues: Alignment.values)
    ..a<$core.double>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'spacing', $pb.PbFieldType.OD)
    ..e<Alignment>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'runAlignment', $pb.PbFieldType.OE, defaultOrMaker: Alignment.start, valueOf: Alignment.valueOf, enumValues: Alignment.values)
    ..a<$core.double>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'runSpacing', $pb.PbFieldType.OD)
    ..e<Alignment>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'crossAxisAlignment', $pb.PbFieldType.OE, defaultOrMaker: Alignment.start, valueOf: Alignment.valueOf, enumValues: Alignment.values)
    ..e<VerticalDirection>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'verticalDirection', $pb.PbFieldType.OE, defaultOrMaker: VerticalDirection.up, valueOf: VerticalDirection.valueOf, enumValues: VerticalDirection.values)
    ..hasRequiredFields = false
  ;

  Wrap._() : super();
  factory Wrap({
    Direction? direction,
    Alignment? alignment,
    $core.double? spacing,
    Alignment? runAlignment,
    $core.double? runSpacing,
    Alignment? crossAxisAlignment,
    VerticalDirection? verticalDirection,
  }) {
    final _result = create();
    if (direction != null) {
      _result.direction = direction;
    }
    if (alignment != null) {
      _result.alignment = alignment;
    }
    if (spacing != null) {
      _result.spacing = spacing;
    }
    if (runAlignment != null) {
      _result.runAlignment = runAlignment;
    }
    if (runSpacing != null) {
      _result.runSpacing = runSpacing;
    }
    if (crossAxisAlignment != null) {
      _result.crossAxisAlignment = crossAxisAlignment;
    }
    if (verticalDirection != null) {
      _result.verticalDirection = verticalDirection;
    }
    return _result;
  }
  factory Wrap.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Wrap.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Wrap clone() => Wrap()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Wrap copyWith(void Function(Wrap) updates) => super.copyWith((message) => updates(message as Wrap)) as Wrap; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Wrap create() => Wrap._();
  Wrap createEmptyInstance() => create();
  static $pb.PbList<Wrap> createRepeated() => $pb.PbList<Wrap>();
  @$core.pragma('dart2js:noInline')
  static Wrap getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Wrap>(create);
  static Wrap? _defaultInstance;

  @$pb.TagNumber(1)
  Direction get direction => $_getN(0);
  @$pb.TagNumber(1)
  set direction(Direction v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasDirection() => $_has(0);
  @$pb.TagNumber(1)
  void clearDirection() => clearField(1);

  @$pb.TagNumber(2)
  Alignment get alignment => $_getN(1);
  @$pb.TagNumber(2)
  set alignment(Alignment v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasAlignment() => $_has(1);
  @$pb.TagNumber(2)
  void clearAlignment() => clearField(2);

  @$pb.TagNumber(3)
  $core.double get spacing => $_getN(2);
  @$pb.TagNumber(3)
  set spacing($core.double v) { $_setDouble(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasSpacing() => $_has(2);
  @$pb.TagNumber(3)
  void clearSpacing() => clearField(3);

  @$pb.TagNumber(4)
  Alignment get runAlignment => $_getN(3);
  @$pb.TagNumber(4)
  set runAlignment(Alignment v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasRunAlignment() => $_has(3);
  @$pb.TagNumber(4)
  void clearRunAlignment() => clearField(4);

  @$pb.TagNumber(5)
  $core.double get runSpacing => $_getN(4);
  @$pb.TagNumber(5)
  set runSpacing($core.double v) { $_setDouble(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasRunSpacing() => $_has(4);
  @$pb.TagNumber(5)
  void clearRunSpacing() => clearField(5);

  @$pb.TagNumber(6)
  Alignment get crossAxisAlignment => $_getN(5);
  @$pb.TagNumber(6)
  set crossAxisAlignment(Alignment v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasCrossAxisAlignment() => $_has(5);
  @$pb.TagNumber(6)
  void clearCrossAxisAlignment() => clearField(6);

  @$pb.TagNumber(7)
  VerticalDirection get verticalDirection => $_getN(6);
  @$pb.TagNumber(7)
  set verticalDirection(VerticalDirection v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasVerticalDirection() => $_has(6);
  @$pb.TagNumber(7)
  void clearVerticalDirection() => clearField(7);
}

enum Text_Type {
  text, 
  texts, 
  notSet
}

class Text extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Text_Type> _Text_TypeByTag = {
    1 : Text_Type.text,
    2 : Text_Type.texts,
    0 : Text_Type.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Text', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..oo(0, [1, 2])
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'text')
    ..aOM<TextChildren>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'texts', subBuilder: TextChildren.create)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'textStyle', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  Text._() : super();
  factory Text({
    $core.String? text,
    TextChildren? texts,
    $core.int? textStyle,
  }) {
    final _result = create();
    if (text != null) {
      _result.text = text;
    }
    if (texts != null) {
      _result.texts = texts;
    }
    if (textStyle != null) {
      _result.textStyle = textStyle;
    }
    return _result;
  }
  factory Text.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Text.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Text clone() => Text()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Text copyWith(void Function(Text) updates) => super.copyWith((message) => updates(message as Text)) as Text; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Text create() => Text._();
  Text createEmptyInstance() => create();
  static $pb.PbList<Text> createRepeated() => $pb.PbList<Text>();
  @$core.pragma('dart2js:noInline')
  static Text getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Text>(create);
  static Text? _defaultInstance;

  Text_Type whichType() => _Text_TypeByTag[$_whichOneof(0)]!;
  void clearType() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.String get text => $_getSZ(0);
  @$pb.TagNumber(1)
  set text($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasText() => $_has(0);
  @$pb.TagNumber(1)
  void clearText() => clearField(1);

  @$pb.TagNumber(2)
  TextChildren get texts => $_getN(1);
  @$pb.TagNumber(2)
  set texts(TextChildren v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasTexts() => $_has(1);
  @$pb.TagNumber(2)
  void clearTexts() => clearField(2);
  @$pb.TagNumber(2)
  TextChildren ensureTexts() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.int get textStyle => $_getIZ(2);
  @$pb.TagNumber(3)
  set textStyle($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasTextStyle() => $_has(2);
  @$pb.TagNumber(3)
  void clearTextStyle() => clearField(3);
}

class TextChildren extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TextChildren', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..pc<Text>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'children', $pb.PbFieldType.PM, subBuilder: Text.create)
    ..hasRequiredFields = false
  ;

  TextChildren._() : super();
  factory TextChildren({
    $core.Iterable<Text>? children,
  }) {
    final _result = create();
    if (children != null) {
      _result.children.addAll(children);
    }
    return _result;
  }
  factory TextChildren.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TextChildren.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TextChildren clone() => TextChildren()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TextChildren copyWith(void Function(TextChildren) updates) => super.copyWith((message) => updates(message as TextChildren)) as TextChildren; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TextChildren create() => TextChildren._();
  TextChildren createEmptyInstance() => create();
  static $pb.PbList<TextChildren> createRepeated() => $pb.PbList<TextChildren>();
  @$core.pragma('dart2js:noInline')
  static TextChildren getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TextChildren>(create);
  static TextChildren? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Text> get children => $_getList(0);
}

class Theme extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Theme', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'textColor')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'primaryColor')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'accentColor')
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'fontSize1', $pb.PbFieldType.O3, protoName: 'font_size_1')
    ..a<$core.int>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'fontSize2', $pb.PbFieldType.O3, protoName: 'font_size_2')
    ..hasRequiredFields = false
  ;

  Theme._() : super();
  factory Theme({
    $core.String? textColor,
    $core.String? primaryColor,
    $core.String? accentColor,
    $core.int? fontSize1,
    $core.int? fontSize2,
  }) {
    final _result = create();
    if (textColor != null) {
      _result.textColor = textColor;
    }
    if (primaryColor != null) {
      _result.primaryColor = primaryColor;
    }
    if (accentColor != null) {
      _result.accentColor = accentColor;
    }
    if (fontSize1 != null) {
      _result.fontSize1 = fontSize1;
    }
    if (fontSize2 != null) {
      _result.fontSize2 = fontSize2;
    }
    return _result;
  }
  factory Theme.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Theme.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Theme clone() => Theme()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Theme copyWith(void Function(Theme) updates) => super.copyWith((message) => updates(message as Theme)) as Theme; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Theme create() => Theme._();
  Theme createEmptyInstance() => create();
  static $pb.PbList<Theme> createRepeated() => $pb.PbList<Theme>();
  @$core.pragma('dart2js:noInline')
  static Theme getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Theme>(create);
  static Theme? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get textColor => $_getSZ(0);
  @$pb.TagNumber(1)
  set textColor($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTextColor() => $_has(0);
  @$pb.TagNumber(1)
  void clearTextColor() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get primaryColor => $_getSZ(1);
  @$pb.TagNumber(2)
  set primaryColor($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPrimaryColor() => $_has(1);
  @$pb.TagNumber(2)
  void clearPrimaryColor() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get accentColor => $_getSZ(2);
  @$pb.TagNumber(3)
  set accentColor($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasAccentColor() => $_has(2);
  @$pb.TagNumber(3)
  void clearAccentColor() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get fontSize1 => $_getIZ(3);
  @$pb.TagNumber(4)
  set fontSize1($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasFontSize1() => $_has(3);
  @$pb.TagNumber(4)
  void clearFontSize1() => clearField(4);

  @$pb.TagNumber(5)
  $core.int get fontSize2 => $_getIZ(4);
  @$pb.TagNumber(5)
  set fontSize2($core.int v) { $_setSignedInt32(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasFontSize2() => $_has(4);
  @$pb.TagNumber(5)
  void clearFontSize2() => clearField(5);
}

class FormField extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'FormField', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'fieldId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'label')
    ..aOB(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'enabled')
    ..aOM<FieldStyle>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'style', subBuilder: FieldStyle.create)
    ..pc<Validator>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'validators', $pb.PbFieldType.PM, subBuilder: Validator.create)
    ..aOM<FieldControl>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'control', subBuilder: FieldControl.create)
    ..aOM<Text>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'title', subBuilder: Text.create)
    ..aOB(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'shouldRequestFocus')
    ..hasRequiredFields = false
  ;

  FormField._() : super();
  factory FormField({
    $core.String? fieldId,
    $core.String? label,
    $core.bool? enabled,
    FieldStyle? style,
    $core.Iterable<Validator>? validators,
    FieldControl? control,
    Text? title,
    $core.bool? shouldRequestFocus,
  }) {
    final _result = create();
    if (fieldId != null) {
      _result.fieldId = fieldId;
    }
    if (label != null) {
      _result.label = label;
    }
    if (enabled != null) {
      _result.enabled = enabled;
    }
    if (style != null) {
      _result.style = style;
    }
    if (validators != null) {
      _result.validators.addAll(validators);
    }
    if (control != null) {
      _result.control = control;
    }
    if (title != null) {
      _result.title = title;
    }
    if (shouldRequestFocus != null) {
      _result.shouldRequestFocus = shouldRequestFocus;
    }
    return _result;
  }
  factory FormField.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory FormField.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  FormField clone() => FormField()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  FormField copyWith(void Function(FormField) updates) => super.copyWith((message) => updates(message as FormField)) as FormField; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static FormField create() => FormField._();
  FormField createEmptyInstance() => create();
  static $pb.PbList<FormField> createRepeated() => $pb.PbList<FormField>();
  @$core.pragma('dart2js:noInline')
  static FormField getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<FormField>(create);
  static FormField? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get fieldId => $_getSZ(0);
  @$pb.TagNumber(1)
  set fieldId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFieldId() => $_has(0);
  @$pb.TagNumber(1)
  void clearFieldId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get label => $_getSZ(1);
  @$pb.TagNumber(2)
  set label($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLabel() => $_has(1);
  @$pb.TagNumber(2)
  void clearLabel() => clearField(2);

  @$pb.TagNumber(3)
  $core.bool get enabled => $_getBF(2);
  @$pb.TagNumber(3)
  set enabled($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasEnabled() => $_has(2);
  @$pb.TagNumber(3)
  void clearEnabled() => clearField(3);

  @$pb.TagNumber(4)
  FieldStyle get style => $_getN(3);
  @$pb.TagNumber(4)
  set style(FieldStyle v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasStyle() => $_has(3);
  @$pb.TagNumber(4)
  void clearStyle() => clearField(4);
  @$pb.TagNumber(4)
  FieldStyle ensureStyle() => $_ensure(3);

  @$pb.TagNumber(5)
  $core.List<Validator> get validators => $_getList(4);

  @$pb.TagNumber(6)
  FieldControl get control => $_getN(5);
  @$pb.TagNumber(6)
  set control(FieldControl v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasControl() => $_has(5);
  @$pb.TagNumber(6)
  void clearControl() => clearField(6);
  @$pb.TagNumber(6)
  FieldControl ensureControl() => $_ensure(5);

  @$pb.TagNumber(7)
  Text get title => $_getN(6);
  @$pb.TagNumber(7)
  set title(Text v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasTitle() => $_has(6);
  @$pb.TagNumber(7)
  void clearTitle() => clearField(7);
  @$pb.TagNumber(7)
  Text ensureTitle() => $_ensure(6);

  @$pb.TagNumber(8)
  $core.bool get shouldRequestFocus => $_getBF(7);
  @$pb.TagNumber(8)
  set shouldRequestFocus($core.bool v) { $_setBool(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasShouldRequestFocus() => $_has(7);
  @$pb.TagNumber(8)
  void clearShouldRequestFocus() => clearField(8);
}

class FieldStyle extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'FieldStyle', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'accentedLabel')
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'fontSize', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  FieldStyle._() : super();
  factory FieldStyle({
    $core.bool? accentedLabel,
    $core.int? fontSize,
  }) {
    final _result = create();
    if (accentedLabel != null) {
      _result.accentedLabel = accentedLabel;
    }
    if (fontSize != null) {
      _result.fontSize = fontSize;
    }
    return _result;
  }
  factory FieldStyle.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory FieldStyle.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  FieldStyle clone() => FieldStyle()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  FieldStyle copyWith(void Function(FieldStyle) updates) => super.copyWith((message) => updates(message as FieldStyle)) as FieldStyle; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static FieldStyle create() => FieldStyle._();
  FieldStyle createEmptyInstance() => create();
  static $pb.PbList<FieldStyle> createRepeated() => $pb.PbList<FieldStyle>();
  @$core.pragma('dart2js:noInline')
  static FieldStyle getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<FieldStyle>(create);
  static FieldStyle? _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get accentedLabel => $_getBF(0);
  @$pb.TagNumber(1)
  set accentedLabel($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAccentedLabel() => $_has(0);
  @$pb.TagNumber(1)
  void clearAccentedLabel() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get fontSize => $_getIZ(1);
  @$pb.TagNumber(2)
  set fontSize($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasFontSize() => $_has(1);
  @$pb.TagNumber(2)
  void clearFontSize() => clearField(2);
}

enum Container_Type {
  column, 
  row, 
  formField, 
  notSet
}

class Container extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Container_Type> _Container_TypeByTag = {
    1 : Container_Type.column,
    2 : Container_Type.row,
    3 : Container_Type.formField,
    0 : Container_Type.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Container', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..oo(0, [1, 2, 3])
    ..aOM<FormColumn>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'column', subBuilder: FormColumn.create)
    ..aOM<FormRow>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'row', subBuilder: FormRow.create)
    ..aOM<FormField>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'formField', subBuilder: FormField.create)
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'columns', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  Container._() : super();
  factory Container({
    FormColumn? column,
    FormRow? row,
    FormField? formField,
    $core.int? columns,
  }) {
    final _result = create();
    if (column != null) {
      _result.column = column;
    }
    if (row != null) {
      _result.row = row;
    }
    if (formField != null) {
      _result.formField = formField;
    }
    if (columns != null) {
      _result.columns = columns;
    }
    return _result;
  }
  factory Container.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Container.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Container clone() => Container()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Container copyWith(void Function(Container) updates) => super.copyWith((message) => updates(message as Container)) as Container; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Container create() => Container._();
  Container createEmptyInstance() => create();
  static $pb.PbList<Container> createRepeated() => $pb.PbList<Container>();
  @$core.pragma('dart2js:noInline')
  static Container getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Container>(create);
  static Container? _defaultInstance;

  Container_Type whichType() => _Container_TypeByTag[$_whichOneof(0)]!;
  void clearType() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  FormColumn get column => $_getN(0);
  @$pb.TagNumber(1)
  set column(FormColumn v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasColumn() => $_has(0);
  @$pb.TagNumber(1)
  void clearColumn() => clearField(1);
  @$pb.TagNumber(1)
  FormColumn ensureColumn() => $_ensure(0);

  @$pb.TagNumber(2)
  FormRow get row => $_getN(1);
  @$pb.TagNumber(2)
  set row(FormRow v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasRow() => $_has(1);
  @$pb.TagNumber(2)
  void clearRow() => clearField(2);
  @$pb.TagNumber(2)
  FormRow ensureRow() => $_ensure(1);

  @$pb.TagNumber(3)
  FormField get formField => $_getN(2);
  @$pb.TagNumber(3)
  set formField(FormField v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasFormField() => $_has(2);
  @$pb.TagNumber(3)
  void clearFormField() => clearField(3);
  @$pb.TagNumber(3)
  FormField ensureFormField() => $_ensure(2);

  @$pb.TagNumber(4)
  $core.int get columns => $_getIZ(3);
  @$pb.TagNumber(4)
  set columns($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasColumns() => $_has(3);
  @$pb.TagNumber(4)
  void clearColumns() => clearField(4);
}

class Layout extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Layout', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Forms'), createEmptyInstance: create)
    ..pc<Container>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'containers', $pb.PbFieldType.PM, subBuilder: Container.create)
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'gap', $pb.PbFieldType.OD)
    ..aOM<Theme>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'theme', subBuilder: Theme.create)
    ..hasRequiredFields = false
  ;

  Layout._() : super();
  factory Layout({
    $core.Iterable<Container>? containers,
    $core.double? gap,
    Theme? theme,
  }) {
    final _result = create();
    if (containers != null) {
      _result.containers.addAll(containers);
    }
    if (gap != null) {
      _result.gap = gap;
    }
    if (theme != null) {
      _result.theme = theme;
    }
    return _result;
  }
  factory Layout.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Layout.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Layout clone() => Layout()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Layout copyWith(void Function(Layout) updates) => super.copyWith((message) => updates(message as Layout)) as Layout; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Layout create() => Layout._();
  Layout createEmptyInstance() => create();
  static $pb.PbList<Layout> createRepeated() => $pb.PbList<Layout>();
  @$core.pragma('dart2js:noInline')
  static Layout getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Layout>(create);
  static Layout? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Container> get containers => $_getList(0);

  @$pb.TagNumber(2)
  $core.double get gap => $_getN(1);
  @$pb.TagNumber(2)
  set gap($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasGap() => $_has(1);
  @$pb.TagNumber(2)
  void clearGap() => clearField(2);

  @$pb.TagNumber(3)
  Theme get theme => $_getN(2);
  @$pb.TagNumber(3)
  set theme(Theme v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasTheme() => $_has(2);
  @$pb.TagNumber(3)
  void clearTheme() => clearField(3);
  @$pb.TagNumber(3)
  Theme ensureTheme() => $_ensure(2);
}

