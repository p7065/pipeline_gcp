///
//  Generated code. Do not modify.
//  source: workflow.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'workflow.pb.dart' as $0;
export 'workflow.pb.dart';

class WorkflowClient extends $grpc.Client {
  static final _$listAvailableWorkflows =
      $grpc.ClientMethod<$0.WorkflowListRequest, $0.WorkflowListResponse>(
          '/Workflow.Workflow/ListAvailableWorkflows',
          ($0.WorkflowListRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.WorkflowListResponse.fromBuffer(value));
  static final _$initiate =
      $grpc.ClientMethod<$0.WorkflowRequest, $0.WorkflowDescriptor>(
          '/Workflow.Workflow/Initiate',
          ($0.WorkflowRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.WorkflowDescriptor.fromBuffer(value));

  WorkflowClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.WorkflowListResponse> listAvailableWorkflows(
      $0.WorkflowListRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$listAvailableWorkflows, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.WorkflowDescriptor> initiate(
      $0.WorkflowRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$initiate, request, options: options);
  }
}

abstract class WorkflowServiceBase extends $grpc.Service {
  $core.String get $name => 'Workflow.Workflow';

  WorkflowServiceBase() {
    $addMethod(
        $grpc.ServiceMethod<$0.WorkflowListRequest, $0.WorkflowListResponse>(
            'ListAvailableWorkflows',
            listAvailableWorkflows_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.WorkflowListRequest.fromBuffer(value),
            ($0.WorkflowListResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.WorkflowRequest, $0.WorkflowDescriptor>(
        'Initiate',
        initiate_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.WorkflowRequest.fromBuffer(value),
        ($0.WorkflowDescriptor value) => value.writeToBuffer()));
  }

  $async.Future<$0.WorkflowListResponse> listAvailableWorkflows_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.WorkflowListRequest> request) async {
    return listAvailableWorkflows(call, await request);
  }

  $async.Future<$0.WorkflowDescriptor> initiate_Pre(
      $grpc.ServiceCall call, $async.Future<$0.WorkflowRequest> request) async {
    return initiate(call, await request);
  }

  $async.Future<$0.WorkflowListResponse> listAvailableWorkflows(
      $grpc.ServiceCall call, $0.WorkflowListRequest request);
  $async.Future<$0.WorkflowDescriptor> initiate(
      $grpc.ServiceCall call, $0.WorkflowRequest request);
}
