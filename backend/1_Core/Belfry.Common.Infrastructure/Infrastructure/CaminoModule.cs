﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Belfry.Infrastructure
{
    public abstract class CaminoModule : Module, ICaminoModule
    {
        public string Key { get; }

        public CaminoModule()
        {
            Key = System.Reflection.Assembly.GetEntryAssembly()?.GetName().Name ?? "";
        }

        protected sealed override void Load(ContainerBuilder builder)
        {
            builder.RegisterBuildCallback(scope => InitializeComponent(scope));
            builder.RegisterInstance(this).As<ICaminoModule>();

            RegisterComponents(builder);
        }

        protected abstract void RegisterComponents(ContainerBuilder builder);

        protected virtual IEnumerable<Type> GetKnownTypes()
        {
            return Enumerable.Empty<Type>();
        }

        public virtual void InitializeComponent(ILifetimeScope container)
        {

        }
    }
}
