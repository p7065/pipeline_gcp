﻿using Autofac;

namespace Belfry.Infrastructure
{
    public interface IResolverHolder
    {
        ILifetimeScope Resolver { set; }

        protected internal void OnResolverSet() { }
    }
}
