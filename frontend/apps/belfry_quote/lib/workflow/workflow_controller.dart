import 'package:belfry_ui_generator/generated/workflow.pb.dart';
import 'package:belfry_quote/workflow/workflow_service.dart';
import 'package:get/get.dart';

class WorkflowController extends GetxController {
  final service = Get.put(FlowService());
  late final Iterable<WorkflowItem> _workflows;

  Iterable<WorkflowItem> get workflows => _workflows;

  @override
  void onInit() {
    try {
      getWorkflowListResponse().then((value) {
        _workflows = value.items;
      });
    } finally {
      super.onInit();
    }
  }

  Future<WorkflowListResponse> getWorkflowListResponse() async {
    return await Future.delayed(
      1.seconds,
      (() => service.listAvailableWorkflows()),
    );
    // return await service.listAvailableWorkflows();
  }

  Future<WorkflowDescriptor?> initWorkflow(WorkflowItem item) async {
    return item.hasType()
        ? service.init(WorkflowRequest(create_1: item.type))
        : null;
    // descriptor
  }
}
