import 'package:belfry_ui_generator/belfry_ui_generator.dart' as $uigenerator;
import 'package:flutter/material.dart';

class FormContent extends StatelessWidget {
  final $uigenerator.Layout layout;
  final $uigenerator.Schema schema;
  final $uigenerator.Item item;
  final void Function()? onSaved;
  late final _form = $uigenerator.FormGenerator(
    layout: layout,
    schema: schema,
    item: item,
  );
  FormContent(this.layout, this.schema, this.item, {this.onSaved, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          'Ajánlatkérés felöltése',
          style: Theme.of(context).textTheme.headline3,
        ),
        SingleChildScrollView(
          child: Card(
            child: Padding(
              padding: const EdgeInsets.only(
                top: 50,
                left: 100,
                right: 100,
                bottom: 50,
              ),
              child: _form,
            ),
          ),
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: MaterialButton(
                color: Theme.of(context).colorScheme.secondary,
                child: const Text(
                  "Submit",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  _form.saveForm();
                  onSaved?.call();
                },
              ),
            ),
            const SizedBox(width: 20),
            Expanded(
              child: MaterialButton(
                color: Theme.of(context).colorScheme.secondary,
                child: const Text(
                  "Reset",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () => _form.resetForm(),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
