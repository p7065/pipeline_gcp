﻿using Autofac;
using Autofac.Core;
using Autofac.Core.Registration;
using Autofac.Core.Resolving.Pipeline;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Belfry.Infrastructure
{
    public abstract class CaminoAutoInjectionModule : CaminoModule, IAutoInjector
    {
        public CaminoAutoInjectionModule()
        {
            Middleware = new AutoInjectionMiddleware(this);
        }

        internal AutoInjectionMiddleware Middleware { get; }

        protected override void AttachToComponentRegistration(IComponentRegistryBuilder componentRegistry, IComponentRegistration registration)
        {
            registration.PipelineBuilding += (sender, pipeline) =>
            {
                pipeline.Use(Middleware);
            };
        }

        protected virtual void OnRegistrationActivated(ResolveRequestContext context) { }

        void OnInjectCoreInterfaces(ResolveRequestContext context)
        {
            if (context.Instance is IResolverHolder holder)
            {
                holder.Resolver = context.Resolve<ILifetimeScope>();
                holder.OnResolverSet();
            }

            if (context.Instance is ILoggable loggable)
            {
                var factory = context.Resolve<ILoggerFactory>();

                loggable.Logger = factory.CreateLogger(context.Service.Description);
            }

            if (context.Instance is IInitializable initializable)
            {
                initializable.Initialize();
            }
        }

        void IAutoInjector.OnRegistrationActivated(ResolveRequestContext context)
        {
            OnInjectCoreInterfaces(context);
            OnRegistrationActivated(context);
        }
    }
}
