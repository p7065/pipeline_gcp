﻿using Microsoft.Extensions.Logging;

namespace Belfry.Infrastructure
{
    public interface ILoggable
    {
        ILogger Logger { set; }
    }
}
