# Belfry Industrial Automatization System Implementation Guides

## Project structure

In general, there are two types of assemblies:
- **Abstractions**: interface definitions, and structural data type definitions mainly. Should be eligible to deal with the feature it specifies without using implementation classes.
- **Implementers**: feature specific implementation assemblies for the abstract types. Technology and scenario dependent implementation. Use the one you need in a certain case.

### Folders and projects

- _1_Core_: the folder includes the infrastructural or common logic that is used in most features.
- _2_Communication_: any type of communication related material. Inter-service or user, emailing, texting, message streams you name it.
- _3_Workflow_: workflow related codebase. Workflow editing and controlling, base activities and so on.
- _4_Logic_: every domain specific feature that is not framework like. If there is a framework like feature that cannot be put into other folders, a new folder should be created.
- _5_Hosting_: frontend services and worker services hosted in K8s, or other type of executables for the system.


#### Abstractions

**Belfry.Common.Abstractions**: common internal infrastructure such as context handling, logging, configuration and environment, hosting, initialization pipeline, dependency injection etc.

**Belfry.Communication.Abstractions**: communication interfaces and message structures for templating and automatic sending.

**Belfry.Template.Abstractions**: automatic form and table generator interfaces and configuration structures

**Belfry.Features.Abstractions**: interfaces and definitions for business logic specific behavior

**Belfry.Storage.Abstractions**: interfaces for storage technologies such as EFCore, MongoDB etc.

**Belfry.Workflow.Abstractions**: interfaces and message structures for workflow management, configuration, editing, controlling, and tracing


#### Implementers

**Belfry.Features.Importing**: generic data importer feature implementations for different data sources


#### Hosts

**Belfry.Dashboard.Service**: backend service for the initial user interface. This is a (hopefully the only one) project specific frontend and backend that functions as a starting point for users to interact with the system.

**Belfry.Template.Service**: configuration editor and provider service for automatic user interface templates. DOES NOT provide data pipeline for requesting and storing data; only helps other services to know about the structure of the data, handles versioning and provides a backend for the template editor feature.


## Best practises

### Namespaces

1. On project creation the first should be to configure default namespace to project namespace (Belfry in this case). It has huge benefits. But firstly it decouples the namespace structure from the project structure - which is by default for a barely comprehensible reason in VS.
2. For extensions methods, the extension class should be in the same namespace that it extends. Benefits: whenever the extended class or interface is used, the extension will be visible as well. Other solution to put the extension into System or other widely used namespace, or add it to the global namespaces (C# 9.0 an above)


