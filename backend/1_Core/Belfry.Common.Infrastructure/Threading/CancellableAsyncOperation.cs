﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Belfry.Threading
{
    public class CancellableAsyncOperation<T> : IDisposable
    {
        private readonly CancellationTokenSource _cancellation;
        private readonly TaskCompletionSource<T> _task;
        private readonly CancellationTokenRegistration _registration;
        private bool _disposed = false;

        public Task<T> Task => _task.Task;
        public CancellationToken Cancellation => _cancellation.Token;

        public CancellableAsyncOperation()
        {
            _cancellation = new CancellationTokenSource();
            _task = new TaskCompletionSource<T>();
            _registration = _cancellation.Token.Register(OnCanceled, useSynchronizationContext: false);
        }

        public CancellableAsyncOperation(CancellationToken cancellation)
        {
            cancellation.Register(OnCanceled, useSynchronizationContext: false);
            _cancellation = CancellationTokenSource.CreateLinkedTokenSource(cancellation);
            _task = new TaskCompletionSource<T>();
            _registration = _cancellation.Token.Register(OnCanceled, useSynchronizationContext: false);
        }

        private void OnCanceled()
        {
            if (_task.TrySetCanceled(Cancellation))
            {
                _registration.Dispose();

                Trace.WriteLine($"The operation has been canceled");
            }
            else
            {
                Trace.WriteLine($"The operation could not be canceled");
            }
        }

        public void Cancel()
        {
            _cancellation.Cancel();
        }

        public bool Finish(T result)
        {
            if (_task.TrySetResult(result))
            {
                _registration.Dispose();

                return true;
            }
            else
            {
                Trace.WriteLine($"The operation could not be finished");

                return false;
            }
        }

        public void Dispose()
        {
            if (_disposed == false)
            {
                _registration.Dispose();
                _cancellation.Dispose();
                _disposed = true;
            }
        }
    }
}
