///
//  Generated code. Do not modify.
//  source: workflow.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use workflowContextDescriptor instead')
const WorkflowContext$json = const {
  '1': 'WorkflowContext',
};

/// Descriptor for `WorkflowContext`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List workflowContextDescriptor = $convert.base64Decode('Cg9Xb3JrZmxvd0NvbnRleHQ=');
@$core.Deprecated('Use workflowDescriptorDescriptor instead')
const WorkflowDescriptor$json = const {
  '1': 'WorkflowDescriptor',
  '2': const [
    const {'1': 'correlation_id', '3': 1, '4': 1, '5': 9, '10': 'correlationId'},
  ],
};

/// Descriptor for `WorkflowDescriptor`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List workflowDescriptorDescriptor = $convert.base64Decode('ChJXb3JrZmxvd0Rlc2NyaXB0b3ISJQoOY29ycmVsYXRpb25faWQYASABKAlSDWNvcnJlbGF0aW9uSWQ=');
@$core.Deprecated('Use workflowTypeDescriptor instead')
const WorkflowType$json = const {
  '1': 'WorkflowType',
  '2': const [
    const {'1': 'workflow_id', '3': 1, '4': 1, '5': 9, '10': 'workflowId'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
  ],
};

/// Descriptor for `WorkflowType`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List workflowTypeDescriptor = $convert.base64Decode('CgxXb3JrZmxvd1R5cGUSHwoLd29ya2Zsb3dfaWQYASABKAlSCndvcmtmbG93SWQSEgoEbmFtZRgCIAEoCVIEbmFtZQ==');
@$core.Deprecated('Use workflowListRequestDescriptor instead')
const WorkflowListRequest$json = const {
  '1': 'WorkflowListRequest',
};

/// Descriptor for `WorkflowListRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List workflowListRequestDescriptor = $convert.base64Decode('ChNXb3JrZmxvd0xpc3RSZXF1ZXN0');
@$core.Deprecated('Use workflowListResponseDescriptor instead')
const WorkflowListResponse$json = const {
  '1': 'WorkflowListResponse',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.Workflow.WorkflowItem', '10': 'items'},
  ],
};

/// Descriptor for `WorkflowListResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List workflowListResponseDescriptor = $convert.base64Decode('ChRXb3JrZmxvd0xpc3RSZXNwb25zZRIsCgVpdGVtcxgBIAMoCzIWLldvcmtmbG93LldvcmtmbG93SXRlbVIFaXRlbXM=');
@$core.Deprecated('Use workflowItemDescriptor instead')
const WorkflowItem$json = const {
  '1': 'WorkflowItem',
  '2': const [
    const {'1': 'type', '3': 1, '4': 1, '5': 11, '6': '.Workflow.WorkflowType', '10': 'type'},
    const {'1': 'instances', '3': 2, '4': 3, '5': 11, '6': '.Workflow.WorkflowInstance', '10': 'instances'},
  ],
};

/// Descriptor for `WorkflowItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List workflowItemDescriptor = $convert.base64Decode('CgxXb3JrZmxvd0l0ZW0SKgoEdHlwZRgBIAEoCzIWLldvcmtmbG93LldvcmtmbG93VHlwZVIEdHlwZRI4CglpbnN0YW5jZXMYAiADKAsyGi5Xb3JrZmxvdy5Xb3JrZmxvd0luc3RhbmNlUglpbnN0YW5jZXM=');
@$core.Deprecated('Use workflowInstanceDescriptor instead')
const WorkflowInstance$json = const {
  '1': 'WorkflowInstance',
  '2': const [
    const {'1': 'descriptor', '3': 1, '4': 1, '5': 11, '6': '.Workflow.WorkflowDescriptor', '10': 'descriptor'},
  ],
};

/// Descriptor for `WorkflowInstance`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List workflowInstanceDescriptor = $convert.base64Decode('ChBXb3JrZmxvd0luc3RhbmNlEjwKCmRlc2NyaXB0b3IYASABKAsyHC5Xb3JrZmxvdy5Xb3JrZmxvd0Rlc2NyaXB0b3JSCmRlc2NyaXB0b3I=');
@$core.Deprecated('Use workflowRequestDescriptor instead')
const WorkflowRequest$json = const {
  '1': 'WorkflowRequest',
  '2': const [
    const {'1': 'create', '3': 1, '4': 1, '5': 11, '6': '.Workflow.WorkflowType', '9': 0, '10': 'create'},
    const {'1': 'continue', '3': 2, '4': 1, '5': 11, '6': '.Workflow.WorkflowInstance', '9': 0, '10': 'continue'},
  ],
  '8': const [
    const {'1': 'type'},
  ],
};

/// Descriptor for `WorkflowRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List workflowRequestDescriptor = $convert.base64Decode('Cg9Xb3JrZmxvd1JlcXVlc3QSMAoGY3JlYXRlGAEgASgLMhYuV29ya2Zsb3cuV29ya2Zsb3dUeXBlSABSBmNyZWF0ZRI4Cghjb250aW51ZRgCIAEoCzIaLldvcmtmbG93LldvcmtmbG93SW5zdGFuY2VIAFIIY29udGludWVCBgoEdHlwZQ==');
