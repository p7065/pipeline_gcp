﻿using Autofac.Core.Resolving.Pipeline;

namespace Belfry.Infrastructure
{
    public interface IAutoInjector
    {
        string Key { get; }
        void OnRegistrationActivated(ResolveRequestContext context);
    }
}
