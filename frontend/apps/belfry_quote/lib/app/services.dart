import 'package:belfry_quote/app/environment.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:grpc/grpc.dart';
import 'package:grpc/grpc_web.dart';
import 'package:grpc/service_api.dart' as $grpc;

abstract class WorkflowServiceBase extends GetxService {
  static String _accessToken = "";

  CallOptions get authorization =>
      CallOptions(metadata: {'Authorization': 'Bearer $_accessToken'});
}

abstract class WorkflowService<TClient extends $grpc.Client>
    extends WorkflowServiceBase {
  $grpc.ClientChannel? _channel;

  $grpc.ClientChannel createChannel();

  TClient get client {
    _channel ??= createChannel();

    return createClient(_channel!);
  }

  @protected
  TClient createClient($grpc.ClientChannel channel);
}

abstract class WebGrpcService {
  $grpc.ClientChannel createChannel() => GrpcWebClientChannel.xhr(
        Uri(
            scheme: Environment.serviceSchema,
            host: Environment.serviceHost,
            port: Environment.servicePort),
      );
}

abstract class GrpcService {
  $grpc.ClientChannel createChannel() {
    var credentials = const ChannelCredentials.secure();

    if (Environment.insecure) {
      credentials = const ChannelCredentials.insecure();
    }

    return ClientChannel(
      Environment.serviceHost,
      port: Environment.servicePort,
      options: ChannelOptions(credentials: credentials),
    );
  }
}

abstract class TokenSetter {
  void setToken(String accessToken) {
    WorkflowServiceBase._accessToken = accessToken;
  }
}
